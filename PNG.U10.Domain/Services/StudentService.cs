using System.Collections.Generic;
using AutoMapper;
using PNG.U10.Domain.Abstract;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Models;
using PNG.U10.Domain.Repositories.Abstract;
using PNG.U10.Domain.Repositories.Filters;
using PNG.U10.Domain.Services.Abstract;

namespace PNG.U10.Domain.Services {
    /// <summary>
    /// Student service
    /// </summary>
    public class StudentService : BaseService<StudentDTO, Student>, IStudentService {
        private IStudentRepository repository;

        /// <summary>
        /// Constructor
        /// </summary>
        public StudentService (IMapper mapper, IStudentRepository repository) : base (mapper) => this.repository = repository;

        public IEnumerable<StudentDTO> GetAll (int pageNumber = 0, int pageSize = 100, StudentFilter filter = null) => Map (repository.Get (pageNumber, pageSize, filter));
        public StudentDTO GetOne (StudentFilter filter = null) => Map (repository.GetOne (filter));
        public StudentDTO UpdateOne (StudentFilter filter, StudentDTO student) => Map (repository.UpdateOne (filter, Map (student)));

        public StudentDTO CreateOne (StudentDTO student) => Map (repository.CreateOne (Map (student)));
    }
}