using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Models;

namespace PNG.U10.Domain.Services.Abstract {

    public interface IInstitutionService : IBaseService<InstitutionDTO, Institution> {
        InstitutionDTO GetInstitution (int id);
        IEnumerable<InstitutionDTO> GetInstitutions (int pageNumber = 0, int pageSize = 100);
        IEnumerable<InstitutionDTO> GetInstitutions (int[] institutions, int pageNumber = 0, int pageSize = 100);
    }
}