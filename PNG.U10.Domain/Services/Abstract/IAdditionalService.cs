using System.Collections.Generic;
using PNG.U10.Domain.Models;

namespace PNG.U10.Domain.Services.Abstract {
    public interface IAdditionalService {
        StudentAdditionalDTO GetStudentAdditional (int studentId, int typeId);
        IEnumerable<StudentAdditionalDTO> GetStudentAdditionals (int studentId, int pageNumber = 0, int pageSize = 100);
        MembershipAdditionalDTO GetMembershipAdditional (int membershipId, int typeId);
        IEnumerable<MembershipAdditionalDTO> GetMembershipAdditionals (int membershipId, int pageNumber = 0, int pageSize = 100);
    }
}