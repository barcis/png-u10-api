using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Models;

namespace PNG.U10.Domain.Services.Abstract {
    public interface IOtherStatusService : IBaseService<OtherStatusDictionaryDTO, OtherStatusDictionary> {

        OtherStatusDictionaryDTO GetOtherStatus (int id);
        IEnumerable<OtherStatusDictionaryDTO> GetOtherStatuses (int pageNumber = 0, int pageSize = 100);
    }
}