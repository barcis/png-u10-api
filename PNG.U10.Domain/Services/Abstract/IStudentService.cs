using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Models;
using PNG.U10.Domain.Repositories.Filters;

namespace PNG.U10.Domain.Services.Abstract {
    public interface IStudentService : IBaseService<StudentDTO, Student> {
        IEnumerable<StudentDTO> GetAll (int pageNumber = 0, int pageSize = 100, StudentFilter filter = null);
        StudentDTO GetOne (StudentFilter filter = null);
        StudentDTO UpdateOne (StudentFilter filter, StudentDTO student);
        StudentDTO CreateOne (StudentDTO student);
    }
}