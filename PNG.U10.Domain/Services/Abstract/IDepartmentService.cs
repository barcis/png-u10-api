using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Models;

namespace PNG.U10.Domain.Services.Abstract {
    public interface IDepartmentService : IBaseService<DepartmentDTO, Department> {
        DepartmentDTO GetDepartment (int id);
        IEnumerable<DepartmentDTO> GetDepartments (int pageNumber = 0, int pageSize = 100);

        IEnumerable<DepartmentDTO> GetDepartments (int[] institutions, int pageNumber = 0, int pageSize = 100);
    }
}