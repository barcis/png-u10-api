namespace PNG.U10.Domain.Services.Abstract {
    public interface IPathService : IInstitutionService, IDepartmentService, IProgrammeService, ISemesterService { }
}