using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Models;

namespace PNG.U10.Domain.Services.Abstract {
    public interface ISemesterService : IBaseService<MembershipSemesterDTO, MembershipSemester> {
        MembershipSemesterDTO GetSemester (int id);
        IEnumerable<MembershipSemesterDTO> GetSemesters (int[] institutions, int[] departments, int[] programmes, int pageNumber = 0, int pageSize = 100);
    }
}