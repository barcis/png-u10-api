using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Models;

namespace PNG.U10.Domain.Services.Abstract {
    public interface IProgrammeService : IBaseService<ProgrammeDTO, Programme> {
        ProgrammeDTO GetProgramme (int id);
        IEnumerable<ProgrammeDTO> GetProgrammes (int pageNumber = 0, int pageSize = 100);
        IEnumerable<ProgrammeDTO> GetProgrammes (int[] institutions, int[] departments, int pageNumber = 0, int pageSize = 100);
    }
}