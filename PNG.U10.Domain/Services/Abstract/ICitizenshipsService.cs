using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Models;

namespace PNG.U10.Domain.Services.Abstract {
    public interface ICitizenshipsService : IBaseService<CitizenshipDictionaryDTO, CitizenshipDictionary> {
        CitizenshipDictionaryDTO GetCitizenship (int id);
        IEnumerable<CitizenshipDictionaryDTO> GetCitizenships (int pageNumber = 0, int pageSize = 100);
    }
}