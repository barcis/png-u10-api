using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Models;

namespace PNG.U10.Domain.Services.Abstract {
    public interface IEnrollmentStatusService : IBaseService<EnrollmentStatusDictionaryDTO, EnrollmentStatusDictionary> {
        EnrollmentStatusDictionaryDTO GetEnrollmentStatus (int id);
        IEnumerable<EnrollmentStatusDictionaryDTO> GetEnrollmentStatuses (int pageNumber = 0, int pageSize = 100);
    }
}