using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Models;

namespace PNG.U10.Domain.Services.Abstract {
    public interface IScholarshipStatusService : IBaseService<ScholarshipStatusDictionaryDTO, ScholarshipStatusDictionary> {
        ScholarshipStatusDictionaryDTO GetScholarshipStatus (int id);
        IEnumerable<ScholarshipStatusDictionaryDTO> GetScholarshipStatuses (int pageNumber = 0, int pageSize = 100);
    }
}