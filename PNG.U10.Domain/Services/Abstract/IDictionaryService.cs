namespace PNG.U10.Domain.Services.Abstract {
    public interface IDictionaryService : IPortOfTravelService, IEnrollmentStatusService, IResidentialStatusService, IScholarshipStatusService, IOtherStatusService, ICitizenshipsService { }
}