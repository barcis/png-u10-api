using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Models;

namespace PNG.U10.Domain.Services.Abstract {
    public interface IUserService : IBaseService<UserDTO, User> {
        IEnumerable<UserDTO> GetAll (int pageNumber = 0, int pageSize = 100);
        IEnumerable<UserDTO> GetAllWithRole (string role, int pageNumber = 0, int pageSize = 100);
        UserDTO GetOne (int id);

        UserDTO GetOneByPersonId (int personId);
        UserDTO GetOneBySubId (string subId);
    }
}