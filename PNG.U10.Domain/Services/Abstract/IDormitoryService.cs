using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Models;

namespace PNG.U10.Domain.Services.Abstract {
    public interface IDormitoryService : IBaseService<DormitoryDTO, Dormitory> {
        DormitoryDTO GetOne (int id);
        IEnumerable<DormitoryDTO> GetAll (int pageNumber = 0, int pageSize = 100);
        IEnumerable<DormitoryRoomDTO> GetOneDormitoryRooms (int id);
        IEnumerable<DormitoryRoomDTO> GetAllDormitoriesRooms ();
        DormitoryRoomDTO GetDormitoryRoom (int id);
    }

    public interface IDormitoryRoomService : IBaseService<DormitoryRoomDTO, DormitoryRoom> {
        IEnumerable<DormitoryRoomDTO> GetOneDormitoryRooms (int id);
        IEnumerable<DormitoryRoomDTO> GetAllDormitoriesRooms ();
        DormitoryRoomDTO GetDormitoryRoom (int id);
    }
}