using System.Collections.Generic;
using AutoMapper;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Models;
using PNG.U10.Domain.Repositories.Abstract;
using PNG.U10.Domain.Services.Abstract;

namespace PNG.U10.Domain.Services {
    public class AdditionalService : IAdditionalService {

        private IStudentAdditionalRepository student;
        private IMembershipAdditionalRepository membership;
        private IMapper Mapper;

        public AdditionalService (IMapper mapper, IStudentAdditionalRepository student, IMembershipAdditionalRepository membership) {
            this.student = student;
            this.membership = membership;
            Mapper = mapper;
        }

        public MembershipAdditionalDTO GetMembershipAdditional (int membershipId, int typeId) => Mapper.Map<MembershipAdditionalDTO> (membership.GetOne (membershipId, typeId));

        public IEnumerable<MembershipAdditionalDTO> GetMembershipAdditionals (int membershipId, int pageNumber = 0, int pageSize = 100) => Mapper.Map<IEnumerable<MembershipAdditionalDTO>> (membership.Get (membershipId, pageNumber, pageSize));

        public StudentAdditionalDTO GetStudentAdditional (int studentId, int typeId) => Mapper.Map<StudentAdditionalDTO> (student.GetOne (studentId, typeId));

        public IEnumerable<StudentAdditionalDTO> GetStudentAdditionals (int studentId, int pageNumber = 0, int pageSize = 100) => Mapper.Map<IEnumerable<StudentAdditionalDTO>> (student.Get (studentId, pageNumber, pageSize));
    }
}