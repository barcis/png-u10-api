using System.Collections.Generic;
using AutoMapper;
using PNG.U10.Domain.Abstract;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Models;
using PNG.U10.Domain.Repositories.Abstract;
using PNG.U10.Domain.Services.Abstract;

namespace PNG.U10.Domain.Services {
    public class ProgrammeService : BaseService<ProgrammeDTO, Programme>, IProgrammeService {
        private IProgrammeRepository repository;

        public ProgrammeService (IMapper mapper, IProgrammeRepository repository) : base (mapper) => this.repository = repository;
        public ProgrammeDTO GetProgramme (int id) => Map (repository.GetOne (id));

        public IEnumerable<ProgrammeDTO> GetProgrammes (int pageNumber = 0, int pageSize = 100) => Map (repository.Get (pageNumber, pageSize));
        public IEnumerable<ProgrammeDTO> GetProgrammes (int[] institutions, int[] departments, int pageNumber = 0, int pageSize = 100) => Map (repository.Get (institutions, departments, pageNumber, pageSize));
    }
}