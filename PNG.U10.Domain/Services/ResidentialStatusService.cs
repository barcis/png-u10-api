using System.Collections.Generic;
using AutoMapper;
using PNG.U10.Domain.Abstract;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Models;
using PNG.U10.Domain.Repositories.Abstract;
using PNG.U10.Domain.Services.Abstract;

namespace PNG.U10.Domain.Services {
    public class ResidentialStatusService : BaseService<ResidentialStatusDictionaryDTO, ResidentialStatusDictionary>, IResidentialStatusService {
        private IResidentialStatusDictionaryRepository repository;

        public ResidentialStatusService (IMapper mapper, IResidentialStatusDictionaryRepository repository) : base (mapper) => this.repository = repository;

        public ResidentialStatusDictionaryDTO GetResidentialStatus (int id) => Map (repository.GetOne (id));
        public IEnumerable<ResidentialStatusDictionaryDTO> GetResidentialStatuses (int pageNumber = 0, int pageSize = 100) => Map (repository.Get (pageNumber, pageSize));
    }
}