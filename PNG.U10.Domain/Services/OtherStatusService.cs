using System.Collections.Generic;
using AutoMapper;
using PNG.U10.Domain.Abstract;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Models;
using PNG.U10.Domain.Repositories.Abstract;
using PNG.U10.Domain.Services.Abstract;

namespace PNG.U10.Domain.Services {
    public class OtherStatusService : BaseService<OtherStatusDictionaryDTO, OtherStatusDictionary>, IOtherStatusService {
        private IOtherStatusDictionaryRepository repository;

        public OtherStatusService (IMapper mapper, IOtherStatusDictionaryRepository repository) : base (mapper) => this.repository = repository;

        public OtherStatusDictionaryDTO GetOtherStatus (int id) => Map (repository.GetOne (id));
        public IEnumerable<OtherStatusDictionaryDTO> GetOtherStatuses (int pageNumber = 0, int pageSize = 100) => Map (repository.Get (pageNumber, pageSize));
    }
}