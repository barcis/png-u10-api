using System.Collections.Generic;
using AutoMapper;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Models;
using PNG.U10.Domain.Services.Abstract;

namespace PNG.U10.Domain.Services {
    /// <summary>
    /// Dictionary service
    /// </summary>
    public class DictionaryService : IDictionaryService {
        private IMapper Mapper;
        private IEnrollmentStatusService EnrollemntStatus;
        private IOtherStatusService OtherStatus;
        private IPortOfTravelService PortOfTravel;
        private IResidentialStatusService ResidentialStatus;
        private IScholarshipStatusService ScholarshipStatus;
        private ICitizenshipsService Citizenships;

        /// <summary>
        /// Constructor
        /// </summary>
        public DictionaryService (
            IMapper mapper,
            IEnrollmentStatusService enrollemntStatus,
            IOtherStatusService otherStatus,
            IPortOfTravelService portOfTravel,
            IResidentialStatusService residentialStatus,
            IScholarshipStatusService scholarshipStatus,
            ICitizenshipsService citizenships
        ) {
            Mapper = mapper;
            EnrollemntStatus = enrollemntStatus;
            OtherStatus = otherStatus;
            PortOfTravel = portOfTravel;
            ResidentialStatus = residentialStatus;
            ScholarshipStatus = scholarshipStatus;
            Citizenships = citizenships;
        }

        #region Other Status Dictionary
        public OtherStatusDictionaryDTO GetOtherStatus (int id) => OtherStatus.GetOtherStatus (id);
        public IEnumerable<OtherStatusDictionaryDTO> GetOtherStatuses (int pageNumber = 0, int pageSize = 100) => OtherStatus.GetOtherStatuses (pageNumber, pageSize);

        public OtherStatusDictionaryDTO Map (OtherStatusDictionary item) => Mapper.Map<OtherStatusDictionaryDTO> (item);
        public OtherStatusDictionary Map (OtherStatusDictionaryDTO item) => Mapper.Map<OtherStatusDictionary> (item);
        public IEnumerable<OtherStatusDictionaryDTO> Map (IEnumerable<OtherStatusDictionary> item) => Mapper.Map<IEnumerable<OtherStatusDictionaryDTO>> (item);
        public IEnumerable<OtherStatusDictionary> Map (IEnumerable<OtherStatusDictionaryDTO> item) => Mapper.Map<IEnumerable<OtherStatusDictionary>> (item);
        #endregion

        #region Port Of Travel Dictionary 
        public PortOfTravelDictionaryDTO GetPortOfTravel (int id) => PortOfTravel.GetPortOfTravel (id);
        public IEnumerable<PortOfTravelDictionaryDTO> GetPortsOfTravel (int pageNumber = 0, int pageSize = 100) => PortOfTravel.GetPortsOfTravel (pageNumber, pageSize);

        public PortOfTravelDictionaryDTO Map (PortOfTravelDictionary item) => Mapper.Map<PortOfTravelDictionaryDTO> (item);
        public PortOfTravelDictionary Map (PortOfTravelDictionaryDTO item) => Mapper.Map<PortOfTravelDictionary> (item);
        public IEnumerable<PortOfTravelDictionaryDTO> Map (IEnumerable<PortOfTravelDictionary> item) => Mapper.Map<IEnumerable<PortOfTravelDictionaryDTO>> (item);
        public IEnumerable<PortOfTravelDictionary> Map (IEnumerable<PortOfTravelDictionaryDTO> item) => Mapper.Map<IEnumerable<PortOfTravelDictionary>> (item);
        #endregion

        #region Residential Status Dictionary 
        public ResidentialStatusDictionaryDTO GetResidentialStatus (int id) => ResidentialStatus.GetResidentialStatus (id);
        public IEnumerable<ResidentialStatusDictionaryDTO> GetResidentialStatuses (int pageNumber = 0, int pageSize = 100) => ResidentialStatus.GetResidentialStatuses (pageNumber, pageSize);

        public ResidentialStatusDictionaryDTO Map (ResidentialStatusDictionary item) => Mapper.Map<ResidentialStatusDictionaryDTO> (item);
        public ResidentialStatusDictionary Map (ResidentialStatusDictionaryDTO item) => Mapper.Map<ResidentialStatusDictionary> (item);
        public IEnumerable<ResidentialStatusDictionaryDTO> Map (IEnumerable<ResidentialStatusDictionary> item) => Mapper.Map<IEnumerable<ResidentialStatusDictionaryDTO>> (item);
        public IEnumerable<ResidentialStatusDictionary> Map (IEnumerable<ResidentialStatusDictionaryDTO> item) => Mapper.Map<IEnumerable<ResidentialStatusDictionary>> (item);
        #endregion

        #region Scholarship Status Dictionary 
        public ScholarshipStatusDictionaryDTO GetScholarshipStatus (int id) => ScholarshipStatus.GetScholarshipStatus (id);
        public IEnumerable<ScholarshipStatusDictionaryDTO> GetScholarshipStatuses (int pageNumber = 0, int pageSize = 100) => ScholarshipStatus.GetScholarshipStatuses (pageNumber, pageSize);

        public ScholarshipStatusDictionaryDTO Map (ScholarshipStatusDictionary item) => Mapper.Map<ScholarshipStatusDictionaryDTO> (item);
        public ScholarshipStatusDictionary Map (ScholarshipStatusDictionaryDTO item) => Mapper.Map<ScholarshipStatusDictionary> (item);
        public IEnumerable<ScholarshipStatusDictionaryDTO> Map (IEnumerable<ScholarshipStatusDictionary> item) => Mapper.Map<IEnumerable<ScholarshipStatusDictionaryDTO>> (item);
        public IEnumerable<ScholarshipStatusDictionary> Map (IEnumerable<ScholarshipStatusDictionaryDTO> item) => Mapper.Map<IEnumerable<ScholarshipStatusDictionary>> (item);
        #endregion

        #region Enrollment Status
        public EnrollmentStatusDictionaryDTO GetEnrollmentStatus (int id) => EnrollemntStatus.GetEnrollmentStatus (id);
        public IEnumerable<EnrollmentStatusDictionaryDTO> GetEnrollmentStatuses (int pageNumber = 0, int pageSize = 100) => EnrollemntStatus.GetEnrollmentStatuses (pageNumber, pageSize);

        public EnrollmentStatusDictionaryDTO Map (EnrollmentStatusDictionary item) => Mapper.Map<EnrollmentStatusDictionaryDTO> (item);
        public EnrollmentStatusDictionary Map (EnrollmentStatusDictionaryDTO item) => Mapper.Map<EnrollmentStatusDictionary> (item);
        public IEnumerable<EnrollmentStatusDictionaryDTO> Map (IEnumerable<EnrollmentStatusDictionary> item) => Mapper.Map<IEnumerable<EnrollmentStatusDictionaryDTO>> (item);
        public IEnumerable<EnrollmentStatusDictionary> Map (IEnumerable<EnrollmentStatusDictionaryDTO> item) => Mapper.Map<IEnumerable<EnrollmentStatusDictionary>> (item);
        #endregion

        #region Enrollment Status
        public CitizenshipDictionaryDTO GetCitizenship (int id) => Citizenships.GetCitizenship (id);
        public IEnumerable<CitizenshipDictionaryDTO> GetCitizenships (int pageNumber = 0, int pageSize = 100) => Citizenships.GetCitizenships (pageNumber, pageSize);

        public CitizenshipDictionaryDTO Map (CitizenshipDictionary item) => Mapper.Map<CitizenshipDictionaryDTO> (item);
        public CitizenshipDictionary Map (CitizenshipDictionaryDTO item) => Mapper.Map<CitizenshipDictionary> (item);
        public IEnumerable<CitizenshipDictionaryDTO> Map (IEnumerable<CitizenshipDictionary> item) => Mapper.Map<IEnumerable<CitizenshipDictionaryDTO>> (item);
        public IEnumerable<CitizenshipDictionary> Map (IEnumerable<CitizenshipDictionaryDTO> item) => Mapper.Map<IEnumerable<CitizenshipDictionary>> (item);
        #endregion
    }
}