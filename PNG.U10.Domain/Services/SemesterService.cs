using System.Collections.Generic;
using AutoMapper;
using PNG.U10.Domain.Abstract;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Models;
using PNG.U10.Domain.Repositories.Abstract;
using PNG.U10.Domain.Services.Abstract;

namespace PNG.U10.Domain.Services {
    public class SemesterService : BaseService<MembershipSemesterDTO, MembershipSemester>, ISemesterService {

        private ISemesterRepository repository;

        public SemesterService (IMapper mapper, ISemesterRepository repository) : base (mapper) => this.repository = repository;
        public MembershipSemesterDTO GetSemester (int id) => Map (repository.GetOne (id));
        public IEnumerable<MembershipSemesterDTO> GetSemesters (int pageNumber = 0, int pageSize = 100) => Map (repository.Get (pageNumber, pageSize));
        public IEnumerable<MembershipSemesterDTO> GetSemesters (int[] institutions, int[] departments, int[] programmes, int pageNumber = 0, int pageSize = 100) => Map (repository.Get (institutions, departments, programmes, pageNumber, pageSize));
    }
}