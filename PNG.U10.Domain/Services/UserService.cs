using System.Collections.Generic;
using AutoMapper;
using PNG.U10.Domain.Abstract;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Models;
using PNG.U10.Domain.Repositories.Abstract;
using PNG.U10.Domain.Services.Abstract;

namespace PNG.U10.Domain.Services {

    /// <summary>
    /// User service
    /// </summary>
    public class UserService : BaseService<UserDTO, User>, IUserService {
        private IUserRepository repository;

        public UserService (IMapper mapper, IUserRepository repository) : base (mapper) => this.repository = repository;

        /// <summary>
        /// Get all users
        /// </summary>
        public IEnumerable<UserDTO> GetAll (int pageNumber = 0, int pageSize = 100) => Map (repository.Get (pageNumber, pageSize));

        /// <summary>
        /// Get all users with role
        /// </summary>
        public IEnumerable<UserDTO> GetAllWithRole (string role, int pageNumber = 0, int pageSize = 100) => Map (repository.GetByRole (role, pageNumber, pageSize));

        /// <summary>
        /// Get one user
        /// </summary>
        public UserDTO GetOne (int id) => Map (repository.GetOne (id));

        public UserDTO GetOneByPersonId (int personId) => Map (repository.GetByPersonId (personId));

        public UserDTO GetOneBySubId (string subId) => Map (repository.GetBySubId (subId));
    }
}