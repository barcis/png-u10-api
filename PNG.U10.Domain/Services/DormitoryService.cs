using System.Collections.Generic;
using AutoMapper;
using PNG.U10.Domain.Abstract;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Models;
using PNG.U10.Domain.Repositories.Abstract;
using PNG.U10.Domain.Services.Abstract;

namespace PNG.U10.Domain.Services {
    /// <summary>
    /// Dormitory Service
    /// </summary>
    public class DormitoryService : BaseService<DormitoryDTO, Dormitory>, IDormitoryService, IDormitoryRoomService {
        private IDormitoryRepository repository;

        /// <summary>
        /// Constructor
        /// </summary>
        public DormitoryService (IMapper mapper, IDormitoryRepository repository) : base (mapper) => this.repository = repository;

        /// <summary>
        /// Get all dormitories
        /// </summary>
        public IEnumerable<DormitoryDTO> GetAll (int pageNumber = 0, int pageSize = 100) => Map (repository.Get (pageNumber, pageSize));

        public DormitoryDTO GetOne (int id) => Map (repository.GetOne (id));

        public IEnumerable<DormitoryRoomDTO> GetAllDormitoriesRooms () => Map (repository.GetRooms ());

        public DormitoryRoomDTO GetDormitoryRoom (int id) => Map (repository.GetRoom (id));

        public IEnumerable<DormitoryRoomDTO> GetOneDormitoryRooms (int id) => Map (repository.GetRooms (id));

        public DormitoryRoom Map (DormitoryRoomDTO item) => Mapper.Map<DormitoryRoom> (item);
        public DormitoryRoomDTO Map (DormitoryRoom item) => Mapper.Map<DormitoryRoomDTO> (item);
        public IEnumerable<DormitoryRoom> Map (IEnumerable<DormitoryRoomDTO> items) => Mapper.Map<IEnumerable<DormitoryRoom>> (items);
        public IEnumerable<DormitoryRoomDTO> Map (IEnumerable<DormitoryRoom> items) => Mapper.Map<IEnumerable<DormitoryRoomDTO>> (items);
    }
}