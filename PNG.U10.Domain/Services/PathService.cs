using System.Collections.Generic;
using AutoMapper;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Models;
using PNG.U10.Domain.Services.Abstract;

namespace PNG.U10.Domain.Services {
    /// <summary>
    /// Path service
    /// </summary>
    public class PathService : IPathService {
        private IMapper Mapper;
        private IInstitutionService Institution;
        private IDepartmentService Department;
        private IProgrammeService Programme;
        private ISemesterService Semester;

        /// <summary>
        /// Constructor
        /// </summary>
        public PathService (
            IMapper mapper,
            IInstitutionService institution,
            IDepartmentService department,
            IProgrammeService programme,
            ISemesterService semester) {
            Mapper = mapper;
            Institution = institution;
            Department = department;
            Programme = programme;
            Semester = semester;
        }

        #region Institution
        public InstitutionDTO GetInstitution (int id) => Institution.GetInstitution (id);
        public IEnumerable<InstitutionDTO> GetInstitutions (int pageNumber = 0, int pageSize = 100) => Institution.GetInstitutions (pageNumber, pageSize);
        public IEnumerable<InstitutionDTO> GetInstitutions (int[] institutions, int pageNumber = 0, int pageSize = 100) => Institution.GetInstitutions (institutions, pageNumber, pageSize);

        public InstitutionDTO Map (Institution item) => Mapper.Map<InstitutionDTO> (item);
        public Institution Map (InstitutionDTO item) => Mapper.Map<Institution> (item);
        public IEnumerable<InstitutionDTO> Map (IEnumerable<Institution> item) => Mapper.Map<IEnumerable<InstitutionDTO>> (item);
        public IEnumerable<Institution> Map (IEnumerable<InstitutionDTO> item) => Mapper.Map<IEnumerable<Institution>> (item);
        #endregion

        #region Department
        public DepartmentDTO GetDepartment (int id) => Department.GetDepartment (id);
        public IEnumerable<DepartmentDTO> GetDepartments (int pageNumber = 0, int pageSize = 100) => Department.GetDepartments (pageNumber, pageSize);
        public IEnumerable<DepartmentDTO> GetDepartments (int[] institutions, int pageNumber = 0, int pageSize = 100) => Department.GetDepartments (institutions, pageNumber, pageSize);

        public DepartmentDTO Map (Department item) => Mapper.Map<DepartmentDTO> (item);
        public Department Map (DepartmentDTO item) => Mapper.Map<Department> (item);
        public IEnumerable<DepartmentDTO> Map (IEnumerable<Department> item) => Mapper.Map<IEnumerable<DepartmentDTO>> (item);
        public IEnumerable<Department> Map (IEnumerable<DepartmentDTO> item) => Mapper.Map<IEnumerable<Department>> (item);
        #endregion

        #region Programme
        public ProgrammeDTO GetProgramme (int id) => Programme.GetProgramme (id);
        public IEnumerable<ProgrammeDTO> GetProgrammes (int pageNumber = 0, int pageSize = 100) => Programme.GetProgrammes (pageNumber, pageSize);
        public IEnumerable<ProgrammeDTO> GetProgrammes (int[] institutions, int[] departments, int pageNumber = 0, int pageSize = 100) => Programme.GetProgrammes (institutions, departments, pageNumber, pageSize);

        public ProgrammeDTO Map (Programme item) => Mapper.Map<ProgrammeDTO> (item);
        public Programme Map (ProgrammeDTO item) => Mapper.Map<Programme> (item);
        public IEnumerable<ProgrammeDTO> Map (IEnumerable<Programme> item) => Mapper.Map<IEnumerable<ProgrammeDTO>> (item);
        public IEnumerable<Programme> Map (IEnumerable<ProgrammeDTO> item) => Mapper.Map<IEnumerable<Programme>> (item);
        #endregion

        #region Semester
        public MembershipSemesterDTO GetSemester (int id) => Semester.GetSemester (id);
        public IEnumerable<MembershipSemesterDTO> GetSemesters (int[] institutions, int[] departments, int[] programmes, int pageNumber = 0, int pageSize = 100) => Semester.GetSemesters (institutions, departments, programmes, pageNumber, pageSize);

        public MembershipSemesterDTO Map (MembershipSemester item) => Mapper.Map<MembershipSemesterDTO> (item);
        public MembershipSemester Map (MembershipSemesterDTO item) => Mapper.Map<MembershipSemester> (item);
        public IEnumerable<MembershipSemesterDTO> Map (IEnumerable<MembershipSemester> item) => Mapper.Map<IEnumerable<MembershipSemesterDTO>> (item);
        public IEnumerable<MembershipSemester> Map (IEnumerable<MembershipSemesterDTO> item) => Mapper.Map<IEnumerable<MembershipSemester>> (item);
        #endregion
    }
}