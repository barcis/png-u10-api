using System.Collections.Generic;
using AutoMapper;
using PNG.U10.Domain.Abstract;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Models;
using PNG.U10.Domain.Repositories.Abstract;
using PNG.U10.Domain.Services.Abstract;

namespace PNG.U10.Domain.Services {
    public class EnrollmentStatusService : BaseService<EnrollmentStatusDictionaryDTO, EnrollmentStatusDictionary>, IEnrollmentStatusService {
        private IEnrollmentStatusDictionaryRepository repository;

        public EnrollmentStatusService (IMapper mapper, IEnrollmentStatusDictionaryRepository repository) : base (mapper) => this.repository = repository;

        public EnrollmentStatusDictionaryDTO GetEnrollmentStatus (int id) => Map (repository.GetOne (id));
        public IEnumerable<EnrollmentStatusDictionaryDTO> GetEnrollmentStatuses (int pageNumber = 0, int pageSize = 100) => Map (repository.Get (pageNumber, pageSize));

    }
}