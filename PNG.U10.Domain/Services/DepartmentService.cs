using System.Collections.Generic;
using AutoMapper;
using PNG.U10.Domain.Abstract;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Models;
using PNG.U10.Domain.Repositories.Abstract;
using PNG.U10.Domain.Services.Abstract;

namespace PNG.U10.Domain.Services {
    /// <summary>
    /// Department service
    /// </summary>
    public class DepartmentService : BaseService<DepartmentDTO, DepartmentFullDTO, Department>, IDepartmentService {
        private IDepartmentRepository repository;

        public DepartmentService (IMapper mapper, IDepartmentRepository repository) : base (mapper) => this.repository = repository;

        public DepartmentDTO GetDepartment (int id) => MapFull (repository.GetOne (id));

        public IEnumerable<DepartmentDTO> GetDepartments (int pageNumber = 0, int pageSize = 100) => Map (repository.Get (pageNumber, pageSize));

        public IEnumerable<DepartmentDTO> GetDepartments (int[] institutions, int pageNumber = 0, int pageSize = 100) => Map (repository.Get (institutions, pageNumber, pageSize));
    }
}