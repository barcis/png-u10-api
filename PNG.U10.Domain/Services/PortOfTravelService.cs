using System.Collections.Generic;
using AutoMapper;
using PNG.U10.Domain.Abstract;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Models;
using PNG.U10.Domain.Repositories.Abstract;
using PNG.U10.Domain.Services.Abstract;

namespace PNG.U10.Domain.Services {
    public class PortOfTravelService : BaseService<PortOfTravelDictionaryDTO, PortOfTravelDictionary>, IPortOfTravelService {
        private IPortOfTravelDictionaryRepository repository;

        public PortOfTravelService (IMapper mapper, IPortOfTravelDictionaryRepository repository) : base (mapper) => this.repository = repository;

        public PortOfTravelDictionaryDTO GetPortOfTravel (int id) => Map (repository.GetOne (id));
        public IEnumerable<PortOfTravelDictionaryDTO> GetPortsOfTravel (int pageNumber = 0, int pageSize = 100) => Map (repository.Get (pageNumber, pageSize));

    }
}