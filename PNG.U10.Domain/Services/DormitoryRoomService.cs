using System.Collections.Generic;
using AutoMapper;
using PNG.U10.Domain.Abstract;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Models;
using PNG.U10.Domain.Repositories.Abstract;
using PNG.U10.Domain.Services.Abstract;

namespace PNG.U10.Domain.Services {
    /// <summary>
    /// DormitoryRoom Service
    /// </summary>
    public class DormitoryRoomService : BaseService<DormitoryRoomDTO, DormitoryRoom>, IDormitoryRoomService {
        private IDormitoryRepository repository;
        public DormitoryRoomService (IMapper mapper, IDormitoryRepository repository) : base (mapper) => this.repository = repository;

        public IEnumerable<DormitoryRoomDTO> GetAllDormitoriesRooms () => Map (repository.GetRooms ());

        public DormitoryRoomDTO GetDormitoryRoom (int id) => Map (repository.GetRoom (id));

        public IEnumerable<DormitoryRoomDTO> GetOneDormitoryRooms (int id) => Map (repository.GetRooms (id));

    }
}