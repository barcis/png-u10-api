using System.Collections.Generic;
using AutoMapper;
using PNG.U10.Domain.Abstract;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Models;
using PNG.U10.Domain.Repositories.Abstract;
using PNG.U10.Domain.Services.Abstract;

namespace PNG.U10.Domain.Services {
    public class CitizenshipsService : BaseService<CitizenshipDictionaryDTO, CitizenshipDictionary>, ICitizenshipsService {
        private ICitizenshipDictionaryRepository repository;

        public CitizenshipsService (IMapper mapper, ICitizenshipDictionaryRepository repository) : base (mapper) => this.repository = repository;

        public CitizenshipDictionaryDTO GetCitizenship (int id) => Map (repository.GetOne (id));

        public IEnumerable<CitizenshipDictionaryDTO> GetCitizenships (int pageNumber = 0, int pageSize = 100) => Map (repository.Get (pageNumber, pageSize));
    }
}