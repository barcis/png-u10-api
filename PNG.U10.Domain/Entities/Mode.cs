using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Entities {
    /// <summary>
    /// Mode entity
    /// Mode of studies
    /// </summary>
    public class Mode : BaseEntity {
        /// <summary>
        /// Mode name
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Mode short
        /// </summary>
        public virtual string Short { get; set; }

        /// <summary>
        /// Mode is Part-time or full-time studies
        /// </summary>
        public virtual bool Fulltime { get; set; }

        /// <summary>
        /// Mode is Active
        /// </summary>
        public virtual bool Active { get; set; }
    }
}