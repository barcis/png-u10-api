using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Helpers;

namespace PNG.U10.Domain.Entities {
    /// <summary>
    /// Dictionary entity
    /// </summary>
    public class DictionaryEntity : BaseEntity {
        [AutocopyDisabled]
        public virtual string Name { get; set; }

        [AutocopyDisabled]
        public virtual bool Active { get; set; }

        public override int GetHashCode () {
            return Name.GetHashCode () + Id.GetHashCode ();
        }

        public override bool Equals (object obj) {
            DictionaryEntity item = obj as DictionaryEntity;

            if (item == null) {
                return false;
            }

            return item.Id == this.Id && item.Name == this.Name && item.Active == this.Active;
        }
    }

}