using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Entities {
    /// <summary>
    /// Role entity
    /// </summary>
    public class Role : BaseEntity {
        /// <summary>
        /// Role name
        /// </summary>
        public virtual string Name { get; set; }
    }
}