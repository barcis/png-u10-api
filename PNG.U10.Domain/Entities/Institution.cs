using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Entities {
    /// <summary>
    /// Institution entity
    /// </summary>
    public class Institution : BaseEntity {
        /// <summary>
        /// Institution name
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Institution short
        /// </summary>
        public virtual string Short { get; set; }

        /// <summary>
        /// Institution code
        /// </summary>
        public virtual string Code { get; set; }

        /// <summary>
        /// Institution is Active
        /// </summary>
        public virtual bool Active { get; set; }

        /// <summary>
        /// Institution id in Selection system
        /// </summary>
        public virtual int InstitutionId { get; set; }

        /// <summary>
        /// Departments related to Institution
        /// </summary>
        public virtual IList<Department> Departments { get; set; }
    }
}