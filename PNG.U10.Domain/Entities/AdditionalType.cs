using System;
using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Entities {
    /// <summary>
    /// AdditionalType entity
    /// </summary>
    public class AdditionalType : BaseEntity {
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual int Type { get; set; }
        public virtual int Edit { get; set; }
        public virtual bool Active { get; set; }

        public virtual IList<AdditionalDictionary> Options { get; set; }
    }
}