namespace PNG.U10.Domain.Entities {
    /// <summary>
    /// StudentResidentialStatus entity
    /// </summary>
    public class StudentResidentialStatus : StudentAdditionalEntity<string> { }
}