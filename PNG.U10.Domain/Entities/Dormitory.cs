using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Entities {
    /// <summary>
    /// Dormitory entity
    /// </summary>
    public class Dormitory : BaseEntity {
        /// <summary>
        /// Dormitory Name
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Dormitory ShortName
        /// </summary>
        public virtual string ShortName { get; set; }

        /// <summary>
        /// Dormitory Number
        /// </summary>
        public virtual int Number { get; set; }

        /// <summary>
        /// Dormitory Floors
        /// </summary>
        public virtual int Floors { get; set; }

        /// <summary>
        /// Dormitory Rooms Number
        /// </summary>
        public virtual int RoomsNumber { get; set; }

        /// <summary>
        /// Dormitory is Active
        /// </summary>
        public virtual bool Active { get; set; }

        /// <summary>
        /// Dormitory Rooms
        /// </summary>
        public virtual IEnumerable<DormitoryRoom> Rooms { get; set; }
    }
}