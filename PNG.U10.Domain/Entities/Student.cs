using System;
using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Helpers;

namespace PNG.U10.Domain.Entities {
    /// <summary>
    /// Student entity
    /// </summary>
    public class Student : BaseEntity {

        /// <summary>
        /// Student Id from U10 system
        /// </summary>
        [AutocopyDisabled]
        public virtual string Uni10Id { get; set; }

        /// <summary>
        /// Person related with Student
        /// </summary>
        [Autocopy]
        public virtual Person Person { get; set; }

        /// <summary>
        /// Allocator related with Student
        /// </summary>
        [AutocopyDisabled]
        public virtual StudentAllocator Allocator { get; set; }

        /// <summary>
        /// Memberships related with Student
        /// </summary>
        [Autocopy]
        public virtual IList<Membership> Memberships { get; set; }

        /// <summary>
        /// Port related with Student
        /// </summary>
        [AutocopyStudentAdditional (dictionaryType = typeof (PortOfTravelDictionary))]
        public virtual StudentPortOfTravel Port { get; set; }

        /// <summary>
        /// Comment related with Student
        /// </summary>
        [AutocopyStudentAdditional (additionalTypeName = AdditionalTypes.Comment)]
        public virtual StudentComment Comment { get; set; }

        /// <summary>
        /// ResidentialStatus related with Student
        /// </summary>
        [AutocopyStudentAdditional (dictionaryType = typeof (ResidentialStatusDictionary))]
        public virtual StudentResidentialStatus ResidentialStatus { get; set; }

        /// <summary>
        /// OtherStatus related with Student
        /// </summary>
        [AutocopyStudentAdditional (dictionaryType = typeof (OtherStatusDictionary))]
        public virtual StudentOtherStatus OtherStatus { get; set; }

        /// <summary>
        /// ScholarshipStatus related with Student
        /// </summary>
        [AutocopyStudentAdditional (dictionaryType = typeof (ScholarshipStatusDictionary))]
        public virtual StudentScholarshipStatus ScholarshipStatus { get; set; }
    }
}