using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Entities {
    /// <summary>
    /// Module entity
    /// </summary>
    public class Module : BaseEntity {

        /// <summary>
        /// Membership related to Module
        /// </summary>
        public virtual Membership Membership { get; set; }

        /// <summary>
        /// MembershipSemester related to Module
        /// </summary>
        public virtual MembershipSemester MembershipSemester { get; set; }

        /// <summary>
        /// Specialization related to Module
        /// </summary>
        public virtual Specialization Specialization { get; set; }

        /// <summary>
        /// Specialty related to Module
        /// </summary>
        public virtual Specialty Specialty { get; set; }

        /// <summary>
        /// Semester related to Module
        /// </summary>
        public virtual Semester Semester { get; set; }
    }
}