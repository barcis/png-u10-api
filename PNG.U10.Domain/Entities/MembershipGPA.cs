using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Entities {
    /// <summary>
    /// MembershipGPA entity
    /// </summary>
    public class MembershipGPA : BaseEntity {
        /// <summary>
        /// Membership related to GPA
        /// </summary>
        public virtual Membership Membership { get; set; }
        public virtual int TypeId { get; set; }
        public virtual double Value { get; set; }
    }
}