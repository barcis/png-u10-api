namespace PNG.U10.Domain.Entities {
    /// <summary>
    /// StudentScholarshipStatus entity
    /// </summary>
    public class StudentScholarshipStatus : StudentAdditionalEntity<string> { }
}