using PNG.U10.Domain.Helpers;

namespace PNG.U10.Domain.Entities {
    /// <summary>
    /// AdditionalDictionary entity
    /// </summary>
    public class AdditionalDictionaryEntity : DictionaryEntity {
        [AutocopyDisabled]
        public virtual int TypeId { get; set; }
    }

}