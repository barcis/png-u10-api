using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Entities {
    /// <summary>
    /// Specialization entity
    /// </summary>
    public class Specialization : BaseEntity {
        /// <summary>
        /// Specialization name
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Specialization short
        /// </summary>
        public virtual string Short { get; set; }

        /// <summary>
        /// Specialization code
        /// </summary>
        public virtual string Code { get; set; }

        /// <summary>
        /// Specialization is Active
        /// </summary>
        public virtual bool Active { get; set; }

        /// <summary>
        /// Specialty related to Specialization
        /// </summary>
        public virtual Specialty Specialty { get; set; }

    }
}