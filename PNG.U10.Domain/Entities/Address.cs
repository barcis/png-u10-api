using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Entities {
    /// <summary>
    /// Address entity
    /// </summary>
    public class Address : BaseEntity {

        /// <summary>
        /// Person related to Address
        /// </summary>
        public virtual Person Person { get; set; }

        public virtual string Street { get; set; }
        public virtual string HomeNumber { get; set; }
        public virtual string FlatNumber { get; set; }
        public virtual string PostOffice { get; set; }
        public virtual string PostalCode { get; set; }
        public virtual string Town { get; set; }
        public virtual string Province { get; set; }
        public virtual string District { get; set; }
        public virtual string LocalGovernment { get; set; }

        public virtual string CorrespondenceStreet { get; set; }
        public virtual string CorrespondenceHomeNumber { get; set; }
        public virtual string CorrespondenceFlatNumber { get; set; }
        public virtual string CorrespondencePostOffice { get; set; }
        public virtual string CorrespondencePostalCode { get; set; }
        public virtual string CorrespondenceTown { get; set; }
        public virtual string CorrespondenceProvince { get; set; }
        public virtual string CorrespondenceDistrict { get; set; }
        public virtual string CorrespondenceLocalGovernment { get; set; }

    }
}