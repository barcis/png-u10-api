using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Entities {
    public class DormitoryEquipment : BaseEntity {
        /// <summary>
        /// Dormitory equipment Name
        /// </summary>
        public virtual string Name { get; set; }
        /// <summary>
        /// Dormitory equipment ordinal number
        /// </summary>
        public virtual int OrdinalNumber { get; set; }

        /// <summary>
        /// Dormitory equipment is Active
        /// </summary>
        public virtual bool Active { get; set; }

    }
}