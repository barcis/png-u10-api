using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Entities {
    /// <summary>
    /// Specialty entity
    /// </summary>
    public class Specialty : BaseEntity {

        /// <summary>
        /// Specialty name
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Specialty short
        /// </summary>
        public virtual string Short { get; set; }

        /// <summary>
        /// Specialty code
        /// </summary>
        public virtual string Code { get; set; }

        /// <summary>
        /// Specialty is Active
        /// </summary>
        public virtual bool Active { get; set; }

        /// <summary>
        /// Kind related to Specialty
        /// </summary>
        public virtual Kind Kind { get; set; }

        /// <summary>
        /// Specializations related to Specialty
        /// </summary>
        public virtual IList<Specialization> Specialization { get; set; }
    }
}