using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Helpers;

namespace PNG.U10.Domain.Entities {
    /// <summary>
    /// Student Enrollment Status entity
    /// </summary>
    public class StudentEnrollmentStatus : BaseEntity {

        /// <summary>
        /// Semester related to StudentEnrollmentStatus
        /// </summary>
        [AutocopyDisabled]
        public virtual MembershipSemester Semester { get; set; }

        /// <summary>
        /// Status related to StudentEnrollmentStatus
        /// </summary>
        [AutocopyDictionary]
        public virtual EnrollmentStatusDictionary Value { get; set; }

        /// <summary>
        /// StudentEnrollmentStatus is Active
        /// </summary>
        [AutocopyDisabled]
        public virtual bool Active { get; set; }
    }
}