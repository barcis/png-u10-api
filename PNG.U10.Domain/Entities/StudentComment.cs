namespace PNG.U10.Domain.Entities {
    /// <summary>
    /// StudentComment entity
    /// </summary>
    public class StudentComment : StudentAdditionalEntity<string> { }
}