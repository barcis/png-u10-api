using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Entities {
    /// <summary>
    /// User entity
    /// </summary>
    public class User : BaseEntity {
        /// <summary>
        /// User login
        /// </summary>
        public virtual string Login { get; set; }

        /// <summary>
        /// User LDAP Domain
        /// </summary>
        public virtual string LDAPDomain { get; set; }

        /// <summary>
        /// Person related to User
        /// </summary>
        public virtual Person Person { get; set; }
    }
}