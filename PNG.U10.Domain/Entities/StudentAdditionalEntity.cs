using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Helpers;

namespace PNG.U10.Domain.Entities {
    public class StudentAdditionalEntity<V> : BaseEntity {
        [AutocopyDisabled]
        public virtual Student Student { get; set; }

        [AutocopyDisabled]
        public virtual int TypeId { get; set; }
        public virtual V Value { get; set; }
    }
}