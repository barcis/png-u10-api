using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Entities {
    /// <summary>
    /// StudentAllocator entity
    /// </summary>
    public class StudentAllocator : StudentAdditionalEntity<string> { }
}