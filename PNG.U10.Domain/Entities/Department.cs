using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Entities {
    /// <summary>
    /// Department entity
    /// </summary>
    public class Department : BaseEntity {
        /// <summary>
        /// Department name
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Department short
        /// </summary>
        public virtual string Short { get; set; }

        /// <summary>
        /// Department code
        /// </summary>
        public virtual string Code { get; set; }

        /// <summary>
        /// Department is Active
        /// </summary>
        public virtual bool Active { get; set; }

        /// <summary>
        /// Department granding scale Id
        /// TODO: change to object?
        /// </summary>
        public virtual int GradingScaleId { get; set; }

        /// <summary>
        /// Institution related to Department
        /// </summary>
        public virtual Institution Institution { get; set; }

        /// <summary>
        /// Programmes related to Department
        /// </summary>
        public virtual IEnumerable<Programme> Programmes { get; set; }
    }
}