using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Entities {
    /// <summary>
    /// Person entity
    /// </summary>
    public class PersonCitizenship : BaseEntity {
        public virtual int PersonId { get; set; }
        public virtual int CitizenshipId { get; set; }
    }

}