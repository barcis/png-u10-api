using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Entities;

namespace PNG.U10.Domain.Entities {
    /// <summary>
    /// DormitoryRoom entity
    /// </summary>
    public class DormitoryRoom : BaseEntity {
        /// <summary>
        /// Dormitory
        /// </summary>
        public virtual Dormitory Dormitory { get; set; }

        /// <summary>
        /// Dormitory room number
        /// </summary>
        public virtual string Number { get; set; }

        /// <summary>
        /// Dormitory room floor
        /// </summary>
        public virtual int Floor { get; set; }

        /// <summary>
        /// Dormitory room is active
        /// </summary>
        public virtual bool Active { get; set; }

        /// <summary>
        /// Dormitory room equipment list
        /// </summary>
        public virtual IEnumerable<DormitoryRoomEquipment> Equipments { get; set; }
    }
}