using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Entities {
    public class DormitoryRoomEquipment : BaseEntity {
        /// <summary>
        /// Dormitory room
        /// </summary>
        public virtual DormitoryRoom Room { get; set; }

        /// <summary>
        /// Dormitory room equipment
        /// </summary>
        public virtual DormitoryEquipment Equipment { get; set; }

        /// <summary>
        /// Dormitory room equipment number of items
        /// </summary>
        public virtual int NumberOfItems { get; set; }

        /// <summary>
        /// Dormitory room equipment name
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Dormitory room equipment description
        /// </summary>
        public virtual string Description { get; set; }
    }
}