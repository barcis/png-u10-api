using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Entities {
    /// <summary>
    /// Kind entity
    /// Kind of studies
    /// </summary>
    public class Kind : BaseEntity {
        /// <summary>
        /// Kind code
        /// </summary>
        public virtual string Code { get; set; }

        /// <summary>
        /// Kind name
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Kind short
        /// </summary>
        public virtual string Short { get; set; }

        /// <summary>
        /// Kind is Active
        /// </summary>
        public virtual bool Active { get; set; }

        /// <summary>
        /// Mode related to Kind
        /// </summary>
        public virtual Mode Mode { get; set; }

        /// <summary>
        /// Programme related to Kind
        /// </summary>
        public virtual Programme Programme { get; set; }

        /// <summary>
        /// Specialty related to Kind
        /// </summary>
        public virtual IList<Specialty> Specialties { get; set; }
    }
}