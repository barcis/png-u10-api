using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Entities {
    /// <summary>
    /// EnrollmentStatusDictionary entity
    /// </summary>
    public class EnrollmentStatusDictionary : DictionaryEntity { }
}