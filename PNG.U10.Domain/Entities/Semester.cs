using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Entities {
    /// <summary>
    /// Semester entity
    /// </summary>
    public class Semester : BaseEntity {
        /// <summary>
        /// Semester name
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Semester number
        /// </summary>
        public virtual int Number {
            get {
                return Id.GetValueOrDefault ();
            }
        }
    }
}