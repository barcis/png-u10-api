using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Entities {
    /// <summary>
    /// Programme entity
    /// </summary>
    public class Programme : BaseEntity {
        /// <summary>
        /// Programme name
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Programme short
        /// </summary>
        public virtual string Short { get; set; }

        /// <summary>
        /// Programme code
        /// </summary>
        public virtual string Code { get; set; }

        /// <summary>
        /// Programme is Active
        /// </summary>
        public virtual bool Active { get; set; }

        /// <summary>
        /// Department related to Programme
        /// </summary>
        public virtual Department Department { get; set; }

        /// <summary>
        /// Kinds related to Programme
        /// </summary>
        public virtual IList<Kind> Kinds { get; set; }
    }
}