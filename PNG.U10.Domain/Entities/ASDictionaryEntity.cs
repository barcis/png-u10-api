using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Helpers;

namespace PNG.U10.Domain.Entities {
    /// <summary>
    /// ASDictionary entity
    /// </summary>
    public class ASDictionaryEntity : BaseEntity {
        [AutocopyDisabled]
        public virtual int OrdinalNumber { get; set; }

        [AutocopyDisabled]
        public virtual string Value { get; set; }

        [AutocopyDisabled]
        public virtual string Short { get; set; }

        [AutocopyDisabled]
        public virtual int TypeId { get; set; }

        [AutocopyDisabled]
        public virtual bool Active { get; set; }
    }

}