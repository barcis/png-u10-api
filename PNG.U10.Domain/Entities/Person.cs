using System;
using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Helpers;

namespace PNG.U10.Domain.Entities {
    /// <summary>
    /// Person entity
    /// </summary>
    public class Person : BaseEntity {
        /// <summary>
        /// Person Surname
        /// </summary>
        [Autocopy]
        public virtual string Surname { get; set; }
        /// <summary>
        /// Person name
        /// </summary>

        [Autocopy]
        public virtual string Name { get; set; }
        /// <summary>
        /// Person gender
        /// </summary>
        [Autocopy]
        public virtual int Gender { get; set; }

        /// <summary>
        /// Person subject id (IdP id)
        /// </summary>
        [Autocopy]
        public virtual string SubId { get; set; }

        /// <summary>
        /// Person email
        /// </summary>
        [Autocopy]
        public virtual string Email { get; set; }

        /// <summary>
        /// Person email2
        /// </summary>
        [Autocopy]
        public virtual string Email2 { get; set; }

        /// <summary>
        /// Person email to work
        /// </summary>
        [Autocopy]
        public virtual string EmailWork { get; set; }

        /// <summary>
        /// Person cell phone number
        /// </summary>
        [Autocopy]
        public virtual string PhoneNumber { get; set; }

        /// <summary>
        /// Person cell phone number
        /// </summary>
        [Autocopy]
        public virtual string PhoneNumberDormitory { get; set; }

        /// <summary>
        /// Person cell phone number
        /// </summary>
        [Autocopy]
        public virtual string PhoneNumberHome { get; set; }

        /// <summary>
        /// Person cell phone number
        /// </summary>
        [Autocopy]
        public virtual string PhoneNumberHome2 { get; set; }

        /// <summary>
        /// Person cell phone number
        /// </summary>
        [Autocopy]
        public virtual string PhoneNumberWork { get; set; }

        /// <summary>
        /// Person cell phone number
        /// </summary>
        [Autocopy]
        public virtual string PhoneNumberWork2 { get; set; }

        /// <summary>
        /// Person cell phone number
        /// </summary>
        [Autocopy]
        public virtual string PhoneNumberOther { get; set; }

        /// <summary>
        /// Person Date Of Birth
        /// </summary>
        [Autocopy]
        public virtual DateTime? DateOfBirth { get; set; }

        /// <summary>
        /// Person Place Of Birth
        /// </summary>
        [Autocopy]
        public virtual PlaceDictionary PlaceOfBirth { get; set; }

        /// <summary>
        /// Person citizenships list
        /// </summary>
        [Autocopy]
        public virtual IList<CitizenshipDictionary> Citizenships { get; set; }

        /// <summary>
        /// Person nationality
        /// </summary>
        [Autocopy]
        public virtual string Nationality { get; set; }

        /// <summary>
        /// Person nrigin
        /// </summary>
        [Autocopy]
        public virtual string Origin { get; set; }

        /// <summary>
        /// Person company
        /// </summary>
        [Autocopy]
        public virtual string Company { get; set; }

        /// <summary>
        /// Person marital status
        /// </summary>
        [Autocopy]
        public virtual int MaritalStatus { get; set; }

        /// <summary>
        /// Roles related to Person
        /// </summary>
        [AutocopyDisabled]
        public virtual IList<Role> Roles { get; set; }

    }

}