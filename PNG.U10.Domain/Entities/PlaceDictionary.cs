using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Entities {
    /// <summary>
    /// PlaceDictionary entity
    /// </summary>
    public class PlaceDictionary : BaseEntity {
        /// <summary>
        /// Place name
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Place is Active
        /// </summary>
        public virtual bool Active { get; set; }
    }
}