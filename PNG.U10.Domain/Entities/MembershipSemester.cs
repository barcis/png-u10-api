using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Helpers;

namespace PNG.U10.Domain.Entities {
    /// <summary>
    /// MembershipSemester entity
    /// </summary>
    public class MembershipSemester : BaseEntity {
        /// <summary>
        /// MembershipSemester Year
        /// </summary>
        [AutocopyDisabled]
        public virtual int Year { get; set; }
        /// <summary>
        /// MembershipSemester Its ??
        /// </summary>
        [AutocopyDisabled]
        public virtual int Its { get; set; }

        /// <summary>
        /// MembershipSemester manual average value
        /// </summary>
        public virtual double ManualAverage { get; set; }

        /// <summary>
        /// MembershipSemester is Active
        /// </summary>
        [AutocopyDisabled]
        public virtual bool Active { get; set; }

        /// <summary>
        /// Membership related to MembershipSemester
        /// </summary>
        [AutocopyDisabled]
        public virtual Membership Membership { get; set; }

        /// <summary>
        /// Semester related to  MembershipSemester
        /// </summary>
        [AutocopyDisabled]
        public virtual Semester Semester { get; set; }

        /// <summary>
        /// Statuses related to MembershipSemester
        /// </summary>
        [Autocopy]
        public virtual IList<StudentEnrollmentStatus> Statuses { get; set; }
    }
}