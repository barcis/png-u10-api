using System;
using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Helpers;

namespace PNG.U10.Domain.Entities {
    /// <summary>
    /// Student Additional base entity
    /// </summary>
    public class StudentAdditional : BaseEntity {
        [AutocopyDisabled]
        public virtual Student Student { get; set; }

        [AutocopyDisabled]
        public virtual AdditionalType Type { get; set; }
        public virtual string StringValue { get; set; }
        public virtual int? IntValue { get; set; }
        public virtual double? DoubleValue { get; set; }
        public virtual DateTime? DateValue { get; set; }

        public virtual object Value {
            get {
                switch (Type.Type) {
                    case 0:
                        return this.StringValue;
                    case 1:
                        return this.IntValue;
                    case 2:
                        return this.DoubleValue;
                    case 3:
                        return this.DateValue;
                    default:
                        return this.StringValue;
                }
            }
            set {
                switch (Type.Type) {
                    case 0:
                        this.StringValue = (value as string);
                        break;
                    case 1:
                        this.IntValue = (value as int?);
                        break;
                    case 2:
                        this.DoubleValue = (value as double?);
                        break;
                    case 3:
                        this.DateValue = (value as DateTime?);
                        break;
                    default:
                        this.StringValue = (value as string);
                        break;
                }
            }
        }
    }
}