using System;
using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Helpers;

namespace PNG.U10.Domain.Entities {
    /// <summary>
    /// Membership entity
    /// </summary>
    public class Membership : BaseEntity {
        /// <summary>
        /// Membership base (0 - current, 1 - archived, etc...)
        /// </summary>
        public virtual int Base { get; set; }

        /// <summary>
        /// Membership is Active
        /// </summary>
        public virtual bool Active { get; set; }

        /// <summary>
        /// Membership Album Number
        /// </summary>
        public virtual string AlbumNumber { get; set; }

        /// <summary>
        /// Membership Start Date
        /// </summary>
        public virtual DateTime? StartDate { get; set; }

        /// <summary>
        /// Membership Start Date on University
        /// </summary>
        public virtual DateTime? UniversityStartDate { get; set; }

        /// <summary>
        /// MembershipGPA related to Membership
        /// </summary>
        [AutocopyDisabled]
        public virtual MembershipGPA GPA { get; set; }

        /// <summary>
        /// Student related to Membership
        /// </summary>
        [AutocopyDisabled]
        public virtual Student Student { get; set; }

        /// <summary>
        /// Institution related to Membership
        /// </summary>
        [AutocopyDisabled]
        public virtual Institution Institution { get; set; }

        /// <summary>
        /// Department related to Membership
        /// </summary>
        [AutocopyDisabled]
        public virtual Department Department { get; set; }

        /// <summary>
        /// Programme related to Membership
        /// </summary>
        [AutocopyDisabled]
        public virtual Programme Programme { get; set; }

        /// <summary>
        /// Kind related to Membership
        /// </summary>
        [AutocopyDisabled]
        public virtual Kind Kind { get; set; }

        /// <summary>
        /// List of Modules related to Membership
        /// </summary>
        [Autocopy]
        public virtual IList<Module> Modules { get; set; }

        /// <summary>
        /// List of Semesters related to Membership
        /// </summary>
        [Autocopy]
        public virtual IList<MembershipSemester> MembershipSemesters { get; set; }
    }
}