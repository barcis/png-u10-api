using FluentNHibernate.Mapping;
using PNG.U10.Domain.Entities;

namespace PNG.U10.Domain.Mappings {
    /// <summary>
    /// Mode ClassMap
    /// </summary>
    public class ModeMap : ClassMap<Mode> {
        public ModeMap () {
            Table ("AS_SRodzaj");

            Id (x => x.Id, "ID_SRODZAJ");
            Map (x => x.Name, "ASSR_NAZWA");
            Map (x => x.Short, "ASSR_SKROT");
            Map (x => x.Fulltime, "ASSR_STACJONARNE");
            Map (x => x.Active, "ASSR_ACTIVE");

            Map (x => x.CreateFirm, "CREATE_FIRMA");
            Map (x => x.CreateUser, "CREATE_USER");
            Map (x => x.UpdateUser, "UPDATE_USER");
            Map (x => x.CreateTime, "CREATE_TIME");
            Map (x => x.UpdateTime, "UPDATE_TIME");
        }
    }
}