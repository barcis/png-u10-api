using FluentNHibernate.Mapping;
using PNG.U10.Domain.Entities;

namespace PNG.U10.Domain.Mappings {
    /// <summary>
    /// Dormitory ClassMap
    /// </summary>
    public class DormitoryMap : ClassMap<Dormitory> {
        /// <summary>
        /// Constructor
        /// </summary>
        public DormitoryMap () {
            Table ("AS_Akademik");

            Id (x => x.Id, "ID_AKADEMIK");

            Map (x => x.Name, "ASAK_NAZWA");
            Map (x => x.ShortName, "ASAK_SKROT");
            Map (x => x.Floors, "ASAK_LICZBA_PIETER");
            Map (x => x.Active, "ASAK_ACTIVE");
            Map (x => x.Number, "ASAK_NUMER_AKADEMIKA");

            HasMany (x => x.Rooms).KeyColumn ("ASAP_ID_AKADEMIK")
                .Inverse ()
                .Cascade.None ();

            Map (x => x.CreateFirm, "CREATE_FIRMA");
            Map (x => x.CreateUser, "CREATE_USER");
            Map (x => x.UpdateUser, "UPDATE_USER");
            Map (x => x.CreateTime, "CREATE_TIME");
            Map (x => x.UpdateTime, "UPDATE_TIME");

        }
    }
}