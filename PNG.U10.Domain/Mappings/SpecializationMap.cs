using FluentNHibernate.Mapping;
using PNG.U10.Domain.Entities;

namespace PNG.U10.Domain.Mappings {
    /// <summary>
    /// Specialization ClassMap
    /// </summary>
    public class SpecializationMap : ClassMap<Specialization> {
        public SpecializationMap () {
            Table ("Std_Specjalizacja");

            Id (x => x.Id, "ID_SPECJALIZACJA");
            Map (x => x.Name, "SPJ_NAZWA");
            Map (x => x.Short, "SPJ_SKROT");
            Map (x => x.Code, "SPJ_RS_KOD");
            Map (x => x.Active, "SPJ_AKTYWNA");

            References (x => x.Specialty, "SPJ_ID_SPECJALNOSC")
                .Cascade.None ();

            Map (x => x.CreateFirm, "CREATE_FIRMA");
            Map (x => x.CreateUser, "CREATE_USER");
            Map (x => x.UpdateUser, "UPDATE_USER");
            Map (x => x.CreateTime, "CREATE_TIME");
            Map (x => x.UpdateTime, "UPDATE_TIME");
        }
    }
}