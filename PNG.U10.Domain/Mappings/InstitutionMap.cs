using FluentNHibernate.Mapping;
using PNG.U10.Domain.Entities;

namespace PNG.U10.Domain.Mappings {
    /// <summary>
    /// Institution ClassMap
    /// </summary>
    public class InstitutionMap : ClassMap<Institution> {
        public InstitutionMap () {
            Table ("Std_Wydzial");
            Id (x => x.Id, "ID_WYDZIAL");
            Map (x => x.Name, "C_NAZWA");
            Map (x => x.Short, "C_SKROT");
            Map (x => x.Code, "C_RS_KOD");
            Map (x => x.Active, "C_AKTYWNY");
            Map (x => x.InstitutionId, "C_POLON_UID");

            HasMany (x => x.Departments)
                .KeyColumn ("K_ID_WYDZIAL")
                .Where ("K_AKTYWNY = 1")
                .Inverse ()
                .Cascade.None ();

            Map (x => x.CreateFirm, "CREATE_FIRMA");
            Map (x => x.CreateUser, "CREATE_USER");
            Map (x => x.UpdateUser, "UPDATE_USER");
            Map (x => x.CreateTime, "CREATE_TIME");
            Map (x => x.UpdateTime, "UPDATE_TIME");
        }
    }
}