using FluentNHibernate.Mapping;
using PNG.U10.Domain.Entities;

namespace PNG.U10.Domain.Mappings {
    /// <summary>
    /// EnrollmentStatusDictionary ClassMap
    /// </summary>
    public class EnrollmentStatusDictionaryMap : ClassMap<EnrollmentStatusDictionary> {
        public EnrollmentStatusDictionaryMap () {
            Table ("AS_Status");

            Where ("M_ACTIVE = 1");

            Id (x => x.Id, "ID_STATUS");
            Map (x => x.Name, "M_NAZWA");
            Map (x => x.Active, "M_ACTIVE");

            Map (x => x.CreateFirm, "CREATE_FIRMA");
            Map (x => x.CreateUser, "CREATE_USER");
            Map (x => x.UpdateUser, "UPDATE_USER");
            Map (x => x.CreateTime, "CREATE_TIME");
            Map (x => x.UpdateTime, "UPDATE_TIME");
        }
    }
}