using System;
using FluentNHibernate.Mapping;
using FluentNHibernate.MappingModel;
using PNG.U10.Domain.Entities;

namespace PNG.U10.Domain.Mappings {
    /// <summary>
    /// Person ClassMap
    /// </summary>
    public class PersonMap : ClassMap<Person> {
        public PersonMap () {
            Table ("Osoba");
            Id (x => x.Id, "ID_OSOBA").GeneratedBy.Native ();

            Map (x => x.Gender, "OS_PLEC");
            Map (x => x.Name, "OS_IMIE_I");
            Map (x => x.Surname, "OS_NAZWISKO");
            Map (x => x.SubId, "OS_SUBJECT_ID");
            Map (x => x.MaritalStatus, "OS_STAN_CYWILNY");
            Map (x => x.Email, "OS_EMAIL");
            Map (x => x.Email2, "OS_EMAIL2");
            Map (x => x.EmailWork, "OS_EMAIL_FIRMA");

            Map (x => x.PhoneNumber, "OS_TELEFON_KOM");
            Map (x => x.PhoneNumberDormitory, "OS_TELEFON_AKADEMIK");
            Map (x => x.PhoneNumberHome, "OS_TELEFON_DOM");
            Map (x => x.PhoneNumberHome2, "OS_TELEFON_DOM2");
            Map (x => x.PhoneNumberWork, "OS_TELEFON_FIRMA");
            Map (x => x.PhoneNumberWork2, "OS_TELEFON_FIRMA2");
            Map (x => x.PhoneNumberOther, "OS_TELEFON_INNY");
            Map (x => x.DateOfBirth, "OS_DATA_URODZENIA");
            Map (x => x.Nationality, "OS_NARODOWOSC");
            Map (x => x.Origin, "OS_POCHODZENIE");
            Map (x => x.Company, "OS_FIRMA");

            References (x => x.PlaceOfBirth, "OS_MIEJSCOWOSC_ID")
                .LazyLoad ()
                .Cascade.None ();

            HasManyToMany (x => x.Roles)
                .LazyLoad ()
                .Table ("Osoba_Roles")
                .ParentKeyColumn ("URO_OSOBA_ID")
                .ChildKeyColumn ("URO_ROLE_ID");

            HasManyToMany (x => x.Citizenships)
                .LazyLoad ()
                .Table ("O_Obywatelstwo")
                .ParentKeyColumn ("OO_ID_OSOBA")
                .ChildKeyColumn ("OO_ID_OBYW")
                .Inverse ()
                .ReadOnly ()
                .Cascade.None ();

            Map (x => x.CreateFirm, "CREATE_FIRMA");
            Map (x => x.CreateUser, "CREATE_USER");
            Map (x => x.UpdateUser, "UPDATE_USER");
            Map (x => x.CreateTime, "CREATE_TIME");
            Map (x => x.UpdateTime, "UPDATE_TIME");
        }
    }
}