using FluentNHibernate.Mapping;
using PNG.U10.Domain.Entities;

namespace PNG.U10.Domain.Mappings {
    /// <summary>
    /// Role ClassMap
    /// </summary>
    public class RoleMap : ClassMap<Role> {
        public RoleMap () {
            Table ("Roles");
            Id (x => x.Id, "ROL_ID_ROLE");
            Map (x => x.Name, "ROL_ROLE_NAME");

            Map (x => x.CreateFirm, "CREATE_FIRMA");
            Map (x => x.CreateUser, "CREATE_USER");
            Map (x => x.UpdateUser, "UPDATE_USER");
            Map (x => x.CreateTime, "CREATE_TIME");
            Map (x => x.UpdateTime, "UPDATE_TIME");
        }
    }
}