using FluentNHibernate.Mapping;
using PNG.U10.Domain.Entities;

namespace PNG.U10.Domain.Mappings {
    /// <summary>
    /// Department ClassMap
    /// </summary>
    public class DepartmentMap : ClassMap<Department> {
        public DepartmentMap () {
            Table ("Std_Kierunek");
            Id (x => x.Id, "ID_KIERUNEK");

            Map (x => x.Name, "K_NAZWA");
            Map (x => x.Short, "K_SKROT");
            Map (x => x.Code, "K_RS_KOD");
            Map (x => x.Active, "K_AKTYWNY");
            Map (x => x.GradingScaleId, "K_ID_SKALA");

            References (x => x.Institution, "K_ID_WYDZIAL")
                .Cascade.None ();

            HasMany (x => x.Programmes)
                .KeyColumn ("T_ID_KIERUNEK")
                .Where ("T_AKTYWNY = 1")
                .Inverse ()
                .Cascade.None ();

            Map (x => x.CreateFirm, "CREATE_FIRMA");
            Map (x => x.CreateUser, "CREATE_USER");
            Map (x => x.UpdateUser, "UPDATE_USER");
            Map (x => x.CreateTime, "CREATE_TIME");
            Map (x => x.UpdateTime, "UPDATE_TIME");
        }
    }
}