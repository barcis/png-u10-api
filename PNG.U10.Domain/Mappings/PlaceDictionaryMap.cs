using FluentNHibernate.Mapping;
using PNG.U10.Domain.Entities;

namespace PNG.U10.Domain.Mappings {
    /// <summary>
    /// PlaceDictionary ClassMap
    /// </summary>
    public class PlaceDictionaryMap : ClassMap<PlaceDictionary> {
        public PlaceDictionaryMap () {
            Table ("AS_Miejscowosc");
            Id (x => x.Id, "ID_MIEJSCOWOSC");

            Map (x => x.Name, "M_WARTOSC");
            Map (x => x.Active, "M_AKTYWNY");

            Map (x => x.CreateFirm, "CREATE_FIRMA");
            Map (x => x.CreateUser, "CREATE_USER");
            Map (x => x.UpdateUser, "UPDATE_USER");
            Map (x => x.CreateTime, "CREATE_TIME");
            Map (x => x.UpdateTime, "UPDATE_TIME");
        }
    }
}