using FluentNHibernate.Mapping;
using PNG.U10.Domain.Entities;

namespace PNG.U10.Domain.Mappings {
    /// <summary>
    /// Department ClassMap
    /// </summary>
    public class StudentCommentMap : ClassMap<StudentComment> {
        public StudentCommentMap () {
            Table ("S_DodDane");

            Where ("SDW_ID_DD_TYP = (SELECT AS_DD_Typ.ID_DD_TYP FROM AS_DD_Typ WHERE(AS_DD_Typ.DDS_NAZWA = 'COMMENT') AND (AS_DD_Typ.DDS_ACTIVE = 1))");

            Id (x => x.Id, "ID_SD_WARTOSC").GeneratedBy.Native ();
            Map (x => x.Value, "SDW_WARTOSC");
            Map (x => x.TypeId, "SDW_ID_DD_TYP");
            References (x => x.Student, "SDW_ID_STUDENT")
                .Cascade.None ();

            Map (x => x.CreateFirm, "CREATE_FIRMA");
            Map (x => x.CreateUser, "CREATE_USER");
            Map (x => x.UpdateUser, "UPDATE_USER");
            Map (x => x.CreateTime, "CREATE_TIME");
            Map (x => x.UpdateTime, "UPDATE_TIME");
        }
    }
}