using FluentNHibernate.Mapping;
using PNG.U10.Domain.Entities;

namespace PNG.U10.Domain.Mappings {
    /// <summary>
    /// Specialty ClassMap
    /// </summary>
    public class SpecialtyMap : ClassMap<Specialty> {
        public SpecialtyMap () {
            Table ("Std_Specjalnosc");

            Id (x => x.Id, "ID_SPECJALNOSC");

            Map (x => x.Name, "SP_NAZWA");
            Map (x => x.Short, "SP_SKROT");
            Map (x => x.Code, "SP_RS_KOD");
            Map (x => x.Active, "SP_AKTYWNA");

            References (x => x.Kind, "SP_ID_RODZAJ")
                .Cascade.None ();

            HasMany (x => x.Specialization)
                .KeyColumn ("SPJ_ID_SPECJALNOSC")
                .Where ("SPJ_AKTYWNA = 1")
                .Inverse ()
                .Cascade.None ();

            Map (x => x.CreateFirm, "CREATE_FIRMA");
            Map (x => x.CreateUser, "CREATE_USER");
            Map (x => x.UpdateUser, "UPDATE_USER");
            Map (x => x.CreateTime, "CREATE_TIME");
            Map (x => x.UpdateTime, "UPDATE_TIME");
        }
    }
}