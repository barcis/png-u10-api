using FluentNHibernate.Mapping;
using PNG.U10.Domain.Entities;

namespace PNG.U10.Domain.Mappings {
    /// <summary>
    /// User ClassMap
    /// </summary>
    public class UserMap : ClassMap<User> {
        public UserMap () {
            Table ("Osoba_Logins");
            Id (x => x.Id, "OL_ID_OSOBA_LOGIN");
            Map (x => x.Login, "OL_LOGIN");
            Map (x => x.LDAPDomain, "OL_LDAP_DOMAIN");

            References (x => x.Person, "OL_OSOBA_ID")
                .Cascade.None ();

            Map (x => x.CreateFirm, "CREATE_FIRMA");
            Map (x => x.CreateUser, "CREATE_USER");
            Map (x => x.UpdateUser, "UPDATE_USER");
            Map (x => x.CreateTime, "CREATE_TIME");
            Map (x => x.UpdateTime, "UPDATE_TIME");
        }
    }
}