using FluentNHibernate.Mapping;
using PNG.U10.Domain.Entities;

namespace PNG.U10.Domain.Mappings {
    /// <summary>
    /// StudentAdditional ClassMap
    /// </summary>
    public class StudentAdditionalMap : ClassMap<StudentAdditional> {
        public StudentAdditionalMap () {
            Table ("S_DodDane");

            Id (x => x.Id, "ID_SD_WARTOSC");

            Map (x => x.StringValue, "SDW_WARTOSC");
            Map (x => x.DoubleValue, "SDW_WARTOSC_FLOAT");
            Map (x => x.IntValue, "SDW_WARTOSC_INT");
            Map (x => x.DateValue, "SDW_WARTOSC_DATE");

            References (x => x.Type, "SDW_ID_DD_TYP");
            References (x => x.Student, "SDW_ID_STUDENT");

            Map (x => x.CreateFirm, "CREATE_FIRMA");
            Map (x => x.CreateUser, "CREATE_USER");
            Map (x => x.UpdateUser, "UPDATE_USER");
            Map (x => x.CreateTime, "CREATE_TIME");
            Map (x => x.UpdateTime, "UPDATE_TIME");
        }
    }
}