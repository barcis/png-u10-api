using FluentNHibernate.Mapping;
using PNG.U10.Domain.Entities;

namespace PNG.U10.Domain.Mappings {
    /// <summary>
    /// Membership ClassMap
    /// </summary>
    public class MembershipMap : ClassMap<Membership> {
        public MembershipMap () {
            Table ("Przynaleznosc");

            Where ("G_BAZA == 2");

            Id (x => x.Id, "ID_PRZYNALEZNOSC");
            Map (x => x.Base, "G_BAZA");
            Map (x => x.Active, "G_AKTYWNY");
            Map (x => x.AlbumNumber, "G_NUMER_ALBUMU");
            Map (x => x.StartDate, "G_DATA_ROZP");
            Map (x => x.UniversityStartDate, "G_DATA_ROZP_NA_UCZELNI");

            HasMany (x => x.MembershipSemesters)
                .KeyColumn ("L_ID_PRZYNALEZNOSC")
                .Where ("L_AKTYWNY = 1")
                .Inverse ()
                .Cascade.None ();

            HasMany (x => x.Modules)
                .KeyColumn ("MD_ID_PRZYNALEZNOSC")
                .Inverse ()
                .Cascade.None ();

            HasOne (x => x.GPA)
                .PropertyRef (x => x.Membership);

            References (x => x.Student, "G_ID_STUDENT")
                .Cascade.None ();

            References (x => x.Institution, "G_ID_WYDZIAL")
                .Cascade.None ();

            References (x => x.Department, "G_ID_KIERUNEK")
                .Cascade.None ();

            References (x => x.Programme, "G_ID_TYP")
                .Cascade.None ();

            References (x => x.Kind, "G_ID_RODZAJ")
                .Cascade.None ();

            Map (x => x.CreateFirm, "CREATE_FIRMA");
            Map (x => x.CreateUser, "CREATE_USER");
            Map (x => x.UpdateUser, "UPDATE_USER");
            Map (x => x.CreateTime, "CREATE_TIME");
            Map (x => x.UpdateTime, "UPDATE_TIME");
        }
    }
}