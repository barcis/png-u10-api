using FluentNHibernate.Mapping;
using PNG.U10.Domain.Entities;

namespace PNG.U10.Domain.Mappings {
    /// <summary>
    /// PersonCitizenship ClassMap
    /// </summary>
    public class PersonCitizenshipMap : ClassMap<PersonCitizenship> {
        public PersonCitizenshipMap () {
            Table ("O_Obywatelstwo");
            Id (x => x.Id, "ID_O_OBYW");
            Map (x => x.PersonId, "OO_ID_OSOBA");
            Map (x => x.CitizenshipId, "OO_ID_OBYW");

            Map (x => x.CreateFirm, "CREATE_FIRMA");
            Map (x => x.CreateUser, "CREATE_USER");
            Map (x => x.UpdateUser, "UPDATE_USER");
            Map (x => x.CreateTime, "CREATE_TIME");
            Map (x => x.UpdateTime, "UPDATE_TIME");
        }
    }
}