using FluentNHibernate.Mapping;
using PNG.U10.Domain.Entities;
/// <summary>
/// Modul ClassMap
/// </summary>
public class ModulMap : ClassMap<Module> {
    public ModulMap () {
        Table ("Modul");

        Id (x => x.Id, "ID_MODUL");

        Map (x => x.CreateFirm, "CREATE_FIRMA");
        Map (x => x.CreateUser, "CREATE_USER");
        Map (x => x.CreateTime, "CREATE_TIME");

        References (x => x.Membership, "MD_ID_PRZYNALEZNOSC")
            .Cascade.None ();

        References (x => x.MembershipSemester, "MD_ID_SEMESTR")
            .Cascade.None ();

        References (x => x.Specialization, "MD_ID_SPECJALIZACJA")
            .Cascade.None ();

        References (x => x.Specialty, "MD_ID_SPECJALNOSC")
            .Cascade.None ();

        References (x => x.Semester, "MD_NR_SEMESTRU")
            .Cascade.None ();
    }
}