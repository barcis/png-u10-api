using FluentNHibernate.Mapping;
using FluentNHibernate.MappingModel;
using PNG.U10.Domain.Entities;

namespace PNG.U10.Domain.Mappings {
    /// <summary>
    /// Student ClassMap
    /// </summary>
    public class StudentMap : ClassMap<Student> {
        public StudentMap () {
            Table ("Student");
            Id (x => x.Id, "ID_STUDENT").GeneratedBy.Native ();

            Map (x => x.Uni10Id, "T_NR_KLIENTA");

            References (x => x.Person, "T_ID_OSOBA")
                .LazyLoad ()
                .Cascade.None ();
            // HasOne (x => x.Person)
            //     .PropertyRef (x => x.Student)
            //     .Cascade.None ();

            HasOne (x => x.Allocator)
                .PropertyRef (x => x.Student)
                .LazyLoad ()
                .Cascade.None ();

            HasOne (x => x.Port)
                .PropertyRef (x => x.Student)
                .LazyLoad ()
                .Cascade.None ();

            HasOne (x => x.Comment)
                .PropertyRef (x => x.Student)
                .LazyLoad ()
                .Cascade.None ();

            HasOne (x => x.ResidentialStatus)
                .LazyLoad ()
                .PropertyRef (x => x.Student)
                .Cascade.None ();

            HasOne (x => x.OtherStatus)
                .LazyLoad ()
                .PropertyRef (x => x.Student)
                .Cascade.None ();

            HasOne (x => x.ScholarshipStatus)
                .LazyLoad ()
                .PropertyRef (x => x.Student)
                .Cascade.None ();

            HasMany (x => x.Memberships)
                .LazyLoad ()
                .KeyColumn ("G_ID_STUDENT")
                .Inverse ()
                .Cascade.None ();

            Map (x => x.CreateFirm, "CREATE_FIRMA");
            Map (x => x.CreateUser, "CREATE_USER");
            Map (x => x.UpdateUser, "UPDATE_USER");
            Map (x => x.CreateTime, "CREATE_TIME");
            Map (x => x.UpdateTime, "UPDATE_TIME");
        }
    }

}