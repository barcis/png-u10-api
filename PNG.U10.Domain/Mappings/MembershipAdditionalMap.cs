using FluentNHibernate.Mapping;
using PNG.U10.Domain.Entities;

namespace PNG.U10.Domain.Mappings {
    /// <summary>
    /// MembershipAdditional ClassMap
    /// </summary>
    public class MembershipAdditionalMap : ClassMap<MembershipAdditional> {
        public MembershipAdditionalMap () {
            Table ("P_DodDane");

            Id (x => x.Id, "ID_PD_WARTOSC");

            Map (x => x.StringValue, "PDD_WARTOSC");
            Map (x => x.DoubleValue, "PDD_WARTOSC_FLOAT");
            Map (x => x.IntValue, "PDD_WARTOSC_INT");
            Map (x => x.DateValue, "PDD_WARTOSC_DATE");

            References (x => x.Type, "PDD_ID_DD_TYP");
            References (x => x.Membership, "PDD_ID_PRZYNALEZNOSC");

            Map (x => x.CreateFirm, "CREATE_FIRMA");
            Map (x => x.CreateUser, "CREATE_USER");
            Map (x => x.UpdateUser, "UPDATE_USER");
            Map (x => x.CreateTime, "CREATE_TIME");
            Map (x => x.UpdateTime, "UPDATE_TIME");
        }
    }
}