using FluentNHibernate.Mapping;
using PNG.U10.Domain.Entities;

namespace PNG.U10.Domain.Mappings {
    /// <summary>
    /// Kind ClassMap
    /// </summary>
    public class KindMap : ClassMap<Kind> {
        public KindMap () {
            Table ("Std_Rodzaj");
            Id (x => x.Id, "ID_RODZAJ");
            Map (x => x.Code, "R_KOD");
            Map (x => x.Name, "R_NAZWA");
            Map (x => x.Short, "R_SKROT");
            Map (x => x.Active, "R_AKTYWNA");

            References (x => x.Mode, "R_ID_SRODZAJ")
                .Cascade.None ();

            References (x => x.Programme, "R_ID_TYP")
                .Cascade.None ();

            HasMany (x => x.Specialties)
                .KeyColumn ("SP_ID_RODZAJ")
                .Where ("SP_AKTYWNA = 1")
                .Inverse ()
                .Cascade.None ();

            Map (x => x.CreateFirm, "CREATE_FIRMA");
            Map (x => x.CreateUser, "CREATE_USER");
            Map (x => x.UpdateUser, "UPDATE_USER");
            Map (x => x.CreateTime, "CREATE_TIME");
            Map (x => x.UpdateTime, "UPDATE_TIME");
        }
    }
}