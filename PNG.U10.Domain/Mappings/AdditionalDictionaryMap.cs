using FluentNHibernate.Mapping;
using PNG.U10.Domain.Entities;

namespace PNG.U10.Domain.Mappings {
    /// <summary>
    /// AdditionalDictionary ClassMap
    /// </summary>
    public class AdditionalDictionaryMap : ClassMap<AdditionalDictionary> {
        public AdditionalDictionaryMap () {
            Table ("AS_DD_Slownik");

            Where ("DSL_ACTIVE = 1");

            Id (x => x.Id, "ID_DD_SLOWNIK");
            Map (x => x.Value, "DSL_WARTOSC");
            Map (x => x.Active, "DSL_ACTIVE");
            Map (x => x.TypeId, "DSL_ID_DD_TYP");

            Map (x => x.CreateFirm, "CREATE_FIRMA");
            Map (x => x.CreateUser, "CREATE_USER");
            Map (x => x.UpdateUser, "UPDATE_USER");
            Map (x => x.CreateTime, "CREATE_TIME");
            Map (x => x.UpdateTime, "UPDATE_TIME");

        }
    }
}