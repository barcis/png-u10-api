using System;
using FluentNHibernate.Mapping;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Helpers;

namespace PNG.U10.Domain.Mappings {
    /// <summary>
    /// NationalityDictionary ClassMap
    /// </summary>
    public class NationalityDictionaryMap : ClassMap<NationalityDictionary> {
        public NationalityDictionaryMap () {
            Table ("AS_Slownik");

            Where (String.Format ("ASS_AKTYWNY = 1 AND ASS_TYP = {0}", OtherDictionaries.Nationality));

            Id (x => x.Id, "ID_SLOWNIK").GeneratedBy.Native ();

            Map (x => x.TypeId, "ASS_TYP");
            Map (x => x.OrdinalNumber, "ASS_LP");
            Map (x => x.Value, "ASS_WARTOSC");
            Map (x => x.Active, "ASS_AKTYWNY");
            Map (x => x.Short, "ASS_SKROT");

            Map (x => x.CreateFirm, "CREATE_FIRMA");
            Map (x => x.CreateUser, "CREATE_USER");
            Map (x => x.UpdateUser, "UPDATE_USER");
            Map (x => x.CreateTime, "CREATE_TIME");
            Map (x => x.UpdateTime, "UPDATE_TIME");
        }
    }
}