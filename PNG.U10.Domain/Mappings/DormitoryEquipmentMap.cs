using FluentNHibernate.Mapping;
using PNG.U10.Domain.Entities;

namespace PNG.U10.Domain.Mappings {
    /// <summary>
    /// DormitoryEquipment ClassMap
    /// </summary>
    public class DormitoryEquipmentMap : ClassMap<DormitoryEquipment> {
        public DormitoryEquipmentMap () {
            Table ("AS_AkademikWyposazenie");
            Id (x => x.Id, "ID_WYPOSAZENIE");

            Map (x => x.Name, "AKDW_PRZEDMIOT");
            Map (x => x.OrdinalNumber, "AKDW_LP");
            Map (x => x.Active, "AKDW_ACTIVE");

            Map (x => x.CreateFirm, "CREATE_FIRMA");
            Map (x => x.CreateUser, "CREATE_USER");
            Map (x => x.UpdateUser, "UPDATE_USER");
            Map (x => x.CreateTime, "CREATE_TIME");
            Map (x => x.UpdateTime, "UPDATE_TIME");
        }
    }

    /// <summary>
    /// DormitoryRoomEquipment ClassMap
    /// </summary>
    public class DormitoryRoomEquipmentMap : ClassMap<DormitoryRoomEquipment> {
        public DormitoryRoomEquipmentMap () {
            Table ("AS_AkademikPokojWyposazenie");
            Id (x => x.Id, "ID_POKOJ_WYPOSAZENIE");

            Map (x => x.Name, "AKPW_PELNA_NAZWA");
            Map (x => x.Description, "AKPW_OPIS");
            Map (x => x.NumberOfItems, "AKPW_LICZBA_SZTUK");

            References (x => x.Room, "AKPW_ID_POKOJ")
                .Cascade.None ();

            References (x => x.Equipment, "AKPW_ID_WYPOSAZENIE")
                .Cascade.None ();

            Map (x => x.CreateFirm, "CREATE_FIRMA");
            Map (x => x.CreateUser, "CREATE_USER");
            Map (x => x.UpdateUser, "UPDATE_USER");
            Map (x => x.CreateTime, "CREATE_TIME");
            Map (x => x.UpdateTime, "UPDATE_TIME");
        }
    }
}