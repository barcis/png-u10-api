using FluentNHibernate.Mapping;
using PNG.U10.Domain.Entities;

namespace PNG.U10.Domain.Mappings {
    /// <summary>
    /// OtherStatusDictionary ClassMap
    /// </summary>
    public class OtherStatusDictionaryMap : ClassMap<OtherStatusDictionary> {
        public OtherStatusDictionaryMap () {
            Table ("AS_DD_Slownik");

            Where ("DSL_ACTIVE = 1 AND DSL_ID_DD_TYP = (SELECT AS_DD_Typ.ID_DD_TYP FROM AS_DD_Typ WHERE(AS_DD_Typ.DDS_NAZWA = 'SCHOARSHIP original') AND (AS_DD_Typ.DDS_ACTIVE = 1))");

            Id (x => x.Id, "ID_DD_SLOWNIK");
            Map (x => x.Name, "DSL_WARTOSC");
            Map (x => x.Active, "DSL_ACTIVE");
            Map (x => x.TypeId, "DSL_ID_DD_TYP");

            Map (x => x.CreateFirm, "CREATE_FIRMA");
            Map (x => x.CreateUser, "CREATE_USER");
            Map (x => x.UpdateUser, "UPDATE_USER");
            Map (x => x.CreateTime, "CREATE_TIME");
            Map (x => x.UpdateTime, "UPDATE_TIME");

        }
    }
}