using FluentNHibernate.Mapping;
using PNG.U10.Domain.Entities;

namespace PNG.U10.Domain.Mappings {
    /// <summary>
    /// MembershipSemester ClassMap
    /// </summary>
    public class MembershipSemesterMap : ClassMap<MembershipSemester> {
        public MembershipSemesterMap () {
            Table ("Lista_Semestrow");

            Where ("L_AKTYWNY = 1");

            Id (x => x.Id, "ID_LISTA_SEMESTROW");
            Map (x => x.Year, "L_ROK");
            Map (x => x.Its, "L_ITS");
            Map (x => x.ManualAverage, "L_SREDNIA_RECZNIE");
            Map (x => x.Active, "L_AKTYWNY");

            References (x => x.Membership, "L_ID_PRZYNALEZNOSC")
                .Cascade.None ();

            References (x => x.Semester, "L_NR_SEMESTRU")
                .Cascade.None ();

            HasMany (x => x.Statuses)
                .KeyColumn ("LS_ID_LISTA_SEMESTROW")
                .Where ("LS_AKTYWNY = 1")
                .Inverse ()
                .Cascade.None ();

            Map (x => x.CreateFirm, "CREATE_FIRMA");
            Map (x => x.CreateUser, "CREATE_USER");
            Map (x => x.UpdateUser, "UPDATE_USER");
            Map (x => x.CreateTime, "CREATE_TIME");
            Map (x => x.UpdateTime, "UPDATE_TIME");

        }
    }
}