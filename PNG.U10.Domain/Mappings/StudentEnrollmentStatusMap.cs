using FluentNHibernate.Mapping;
using PNG.U10.Domain.Entities;

namespace PNG.U10.Domain.Mappings {
    /// <summary>
    /// StudentEnrollmentStatus ClassMap
    /// </summary>
    public class StudentEnrollmentStatusMap : ClassMap<StudentEnrollmentStatus> {
        public StudentEnrollmentStatusMap () {
            Table ("LStatus");

            Where ("LS_AKTYWNY = 1");

            Id (x => x.Id, "ID_LSTATUS").GeneratedBy.Native ();
            Map (x => x.Active, "LS_AKTYWNY");

            References (x => x.Semester, "LS_ID_LISTA_SEMESTROW")
                .Cascade.None ();

            References (x => x.Value, "LS_ID_STATUS")
                .Cascade.None ();

            Map (x => x.CreateFirm, "CREATE_FIRMA");
            Map (x => x.CreateUser, "CREATE_USER");
            Map (x => x.UpdateUser, "UPDATE_USER");
            Map (x => x.CreateTime, "CREATE_TIME");
            Map (x => x.UpdateTime, "UPDATE_TIME");
        }
    }
}