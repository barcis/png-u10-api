using FluentNHibernate.Mapping;
using PNG.U10.Domain.Entities;

namespace PNG.U10.Domain.Mappings {
    /// <summary>
    /// Programme ClassMap
    /// </summary>
    public class ProgrammeMap : ClassMap<Programme> {
        public ProgrammeMap () {
            Table ("Std_Typ");
            Id (x => x.Id, "ID_TYP");
            Map (x => x.Name, "T_NAZWA");
            Map (x => x.Short, "T_SKROT");
            Map (x => x.Code, "T_RS_KOD");
            Map (x => x.Active, "T_AKTYWNY");

            References (x => x.Department, "T_ID_KIERUNEK")
                .Cascade.None ();

            HasMany (x => x.Kinds)
                .KeyColumn ("R_ID_TYP")
                .Where ("R_AKTYWNA = 1")
                .Inverse ()
                .Cascade.None ();

            Map (x => x.CreateFirm, "CREATE_FIRMA");
            Map (x => x.CreateUser, "CREATE_USER");
            Map (x => x.UpdateUser, "UPDATE_USER");
            Map (x => x.CreateTime, "CREATE_TIME");
            Map (x => x.UpdateTime, "UPDATE_TIME");
        }
    }
}