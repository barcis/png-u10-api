using FluentNHibernate.Mapping;
using PNG.U10.Domain.Entities;

namespace PNG.U10.Domain.Mappings {
    /// <summary>
    /// Semester ClassMap
    /// </summary>
    public class SemesterMap : ClassMap<Semester> {
        public SemesterMap () {
            Table ("Std_Semestr");

            Id (x => x.Id, "NR_SEMESTRU");
            Map (x => x.Name, "SE_NAZWA");

            Map (x => x.CreateFirm, "CREATE_FIRMA");
            Map (x => x.CreateUser, "CREATE_USER");
            Map (x => x.UpdateUser, "UPDATE_USER");
            Map (x => x.CreateTime, "CREATE_TIME");
            Map (x => x.UpdateTime, "UPDATE_TIME");

        }
    }
}