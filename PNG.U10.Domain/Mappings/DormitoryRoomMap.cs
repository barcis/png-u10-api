using FluentNHibernate.Mapping;
using PNG.U10.Domain.Entities;

namespace PNG.U10.Domain.Mappings {
    /// <summary>
    /// DormitoryRoom ClassMap
    /// </summary>
    public class DormitoryRoomMap : ClassMap<DormitoryRoom> {
        /// <summary>
        /// Constructor
        /// </summary>
        public DormitoryRoomMap () {
            Table ("AS_AkademikPokoj");

            Id (x => x.Id, "ID_POKOJ");

            Map (x => x.Floor, "ASAP_PIETRO");
            Map (x => x.Active, "ASAP_ACTIVE");
            Map (x => x.Number, "ASAP_NUMER");

            References (x => x.Dormitory, "ASAP_ID_AKADEMIK")
                .Cascade.None ();

            HasMany (x => x.Equipments).KeyColumn ("AKPW_ID_POKOJ")
                .Inverse ()
                .Cascade.None ();

            Map (x => x.CreateFirm, "CREATE_FIRMA");
            Map (x => x.CreateUser, "CREATE_USER");
            Map (x => x.UpdateUser, "UPDATE_USER");
            Map (x => x.CreateTime, "CREATE_TIME");
            Map (x => x.UpdateTime, "UPDATE_TIME");
        }
    }
}