using FluentNHibernate.Mapping;
using PNG.U10.Domain.Entities;

namespace PNG.U10.Domain.Mappings {
    /// <summary>
    /// AdditionalType ClassMap
    /// </summary>
    public class AdditionalTypeMap : ClassMap<AdditionalType> {
        public AdditionalTypeMap () {
            Table ("AS_DD_Typ");
            Where ("DDS_ACTIVE = 1");

            Id (x => x.Id, "ID_DD_TYP");
            Map (x => x.Name, "DDS_NAZWA");
            Map (x => x.Description, "DDS_OPIS");
            Map (x => x.Type, "DDS_TYP_DANYCH");
            Map (x => x.Edit, "DDS_EDIT");
            Map (x => x.Active, "DDS_ACTIVE");

            Map (x => x.CreateFirm, "CREATE_FIRMA");
            Map (x => x.CreateUser, "CREATE_USER");
            Map (x => x.UpdateUser, "UPDATE_USER");
            Map (x => x.CreateTime, "CREATE_TIME");
            Map (x => x.UpdateTime, "UPDATE_TIME");
        }
    }
}