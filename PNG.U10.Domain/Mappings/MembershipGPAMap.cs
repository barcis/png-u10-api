using FluentNHibernate.Mapping;
using PNG.U10.Domain.Entities;

namespace PNG.U10.Domain.Mappings {
    /// <summary>
    /// MembershipGPA ClassMap
    /// </summary>
    public class MembershipGPAMap : ClassMap<MembershipGPA> {
        public MembershipGPAMap () {
            Table ("P_DodDane");

            Where ("PDD_ID_DD_TYP = (SELECT AS_DD_Typ.ID_DD_TYP FROM AS_DD_Typ WHERE(AS_DD_Typ.DDS_NAZWA = 'GPA_HEI') AND (AS_DD_Typ.DDS_ACTIVE = 1))");

            Id (x => x.Id, "ID_PD_WARTOSC").GeneratedBy.Native ();

            Map (x => x.TypeId, "PDD_ID_DD_TYP");
            Map (x => x.Value, "PDD_WARTOSC_FLOAT");

            References (x => x.Membership, "PDD_ID_PRZYNALEZNOSC")
                .Cascade.None ();

            Map (x => x.CreateFirm, "CREATE_FIRMA");
            Map (x => x.CreateUser, "CREATE_USER");
            Map (x => x.UpdateUser, "UPDATE_USER");
            Map (x => x.CreateTime, "CREATE_TIME");
            Map (x => x.UpdateTime, "UPDATE_TIME");
        }
    }
}