using FluentNHibernate.Mapping;
using PNG.U10.Domain.Entities;

namespace PNG.U10.Domain.Mappings {
    /// <summary>
    /// Address ClassMap
    /// </summary>
    public class AddressMap : ClassMap<Address> {
        public AddressMap () {
            Table ("O_ADRES");
            Id (x => x.Id, "OADR_ID_OSOBA");

            Map (x => x.Street, "OADR_ZAM_ULICA");
            Map (x => x.HomeNumber, "OADR_ZAM_NR_DOMU");
            Map (x => x.FlatNumber, "OADR_ZAM_NR_MIESZKANIA");
            Map (x => x.PostOffice, "OADR_ZAM_POCZTA");
            Map (x => x.PostalCode, "OADR_ZAM_KOD_POCZTOWY");

            Map (x => x.CorrespondenceStreet, "OADR_KOR_ULICA");
            Map (x => x.CorrespondenceHomeNumber, "OADR_KOR_NR_DOMU");
            Map (x => x.CorrespondenceFlatNumber, "OADR_KOR_NR_MIESZKANIA");
            Map (x => x.CorrespondencePostOffice, "OADR_KOR_POCZTA");
            Map (x => x.CorrespondencePostalCode, "OADR_KOR_KOD_POCZTOWY");

            // Map (x => x.Town, "OADR_ZAM_MIEJSCOWOSC_ID");
            // Map (x => x.Province, "");
            // Map (x => x.District, "");
            // Map (x => x.LocalGovernment, "");

            // Map (x => x.CorrespondenceTown, "OADR_ZAM_MIEJSCOWOSC_ID");
            // Map (x => x.CorrespondenceProvince, "");
            // Map (x => x.CorrespondenceDistrict, "");
            // Map (x => x.CorrespondenceLocalGovernment, "");

            References (x => x.Person, "OADR_ID_OSOBA")
                .Cascade.None ();

            Map (x => x.CreateFirm, "CREATE_FIRMA");
            Map (x => x.CreateUser, "CREATE_USER");
            Map (x => x.UpdateUser, "UPDATE_USER");
            Map (x => x.CreateTime, "CREATE_TIME");
            Map (x => x.UpdateTime, "UPDATE_TIME");
        }
    }
}