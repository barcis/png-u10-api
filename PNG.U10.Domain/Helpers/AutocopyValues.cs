using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Entities;

namespace PNG.U10.Domain.Helpers {
    public interface IAutocopyValues {
        IList<string> Copy (BaseEntity from, BaseEntity to);
        IAutocopyValues ContextStudent (Student student);
    }

    public class AutocopyValues : IAutocopyValues {

        private Student contextStudent;
        private NHibernate.ISession Session;

        public AutocopyValues (NHibernate.ISession session) {
            Session = session;
        }

        public IAutocopyValues ContextStudent (Student student) {
            contextStudent = student;
            return this;
        }

        public IList<string> Copy (BaseEntity from, BaseEntity to) {
            return Copy (from, to, true);
        }

        private IList<string> Copy (BaseEntity from, BaseEntity to, bool clearContext) {
            var properties = to.GetType ().GetProperties ();
            IList<string> changes = new List<string> ();
            foreach (var property in properties) {
                var attributes = property.GetCustomAttributes (false);

                bool isAutocopy = attributes
                    .Where (x => x is AutocopyAttribute)
                    .Any ();

                if (!isAutocopy) {
                    continue;
                }

                bool isDisabled = attributes
                    .Where (x => x is AutocopyDisabledAttribute)
                    .Any ();

                if (isDisabled) {
                    continue;
                }

                bool isDictionary = attributes
                    .Where (a => a.GetType () == typeof (AutocopyDictionaryAttribute))
                    .Any ();

                object fromValue = property.GetValue (from);
                object toValue = property.GetValue (to);
                if (!object.Equals (fromValue, toValue)) {

                    if (fromValue is StudentAdditionalEntity<string>) {
                        AutocopyStudentAdditionalAttribute attribute = (AutocopyStudentAdditionalAttribute) attributes
                            .Where (a => a.GetType () == typeof (AutocopyStudentAdditionalAttribute)).First ();

                        if (attribute == null) {

                        } else if (contextStudent == null) {
                            if (from is Student) {
                                contextStudent = from as Student;
                            } else {
                                throw new System.Exception ("AutocopyStudentAdditionalAttribute was used without providing the student context");
                            }
                        }
                        var fromAdditional = (fromValue as StudentAdditionalEntity<string>);
                        var toAdditional = (toValue as StudentAdditionalEntity<string>);

                        if (fromAdditional.Value != null && toAdditional.Value == null) {
                            //insert;
                            if (attribute.dictionaryType != null) {
                                var dictionaryType = attribute.dictionaryType;

                                var generic = this.GetType ().GetMethod ("GetDictionaryItemByValue", BindingFlags.NonPublic | BindingFlags.Instance);
                                var method = generic.MakeGenericMethod (dictionaryType);

                                AdditionalDictionaryEntity itemDict = method.Invoke (this, new object[] { fromAdditional.Value }) as AdditionalDictionaryEntity;
                                toAdditional.TypeId = itemDict.TypeId;

                            } else if (attribute.additionalTypeName != null) {
                                toAdditional.TypeId = GetAdditionalTypeId (attribute.additionalTypeName);
                            } else {
                                throw new System.Exception ("AutocopyStudentAdditionalAttribute was used without set 'dictionaryType' or 'additionalTypeName'");
                            }

                            if (toAdditional.TypeId > 0) {
                                toAdditional.Value = fromAdditional.Value;
                                toAdditional.Student = contextStudent;
                                Session.Save (toValue);
                            } else {
                                throw new System.Exception ("AutocopyStudentAdditionalAttribute bad value");
                            }

                        } else if (fromAdditional.Value == null && toAdditional.Value != null) {
                            // delete
                            Session.Delete (toAdditional);
                        } else if (fromAdditional.Value != toAdditional.Value) {
                            // update
                            toAdditional.Student = contextStudent;
                            toAdditional.Value = fromAdditional.Value;
                            Session.Update (toAdditional);
                        }

                    } else if (fromValue is IEnumerable<BaseEntity>) {
                        IEnumerable<BaseEntity> fromItems = (fromValue as IList).Cast<BaseEntity> ();
                        IEnumerable<BaseEntity> toItems = (toValue as IList).Cast<BaseEntity> ();

                        //edycja
                        foreach (BaseEntity fromItem in fromItems) {
                            BaseEntity toItem = toItems.FirstOrDefault (i => i.Id == fromItem.Id);
                            if (toItem != null) {
                                var rchange = Copy (fromItem, toItem, false);
                                if (rchange.Any ()) {
                                    foreach (var c in rchange) {
                                        changes.Add (c);
                                    }
                                    Session.SaveOrUpdate (toItem);
                                }
                            }
                        }

                        //usuwanie 
                        int[] ids = fromItems.Select (x => x.Id.GetValueOrDefault ()).ToArray<int> ();

                        IEnumerable<BaseEntity> toRemove;
                        if (ids.Any ()) {
                            toRemove = toItems.Where (x => !ids.Contains (x.Id.GetValueOrDefault ())).ToList ();
                        } else {
                            toRemove = toItems.ToList ();
                        }

                        foreach (var item in toRemove) {
                            (toItems as IList).Remove (item);
                            changes.Add (property.Name + " Remove: " + item.Id.ToString ());
                        }
                    } else if (isDictionary && fromValue is DictionaryEntity) {
                        var fromDictionary = (fromValue as DictionaryEntity);
                        var toDictionary = (toValue as DictionaryEntity);

                        var generic = this.GetType ().GetMethod ("GetDictionaryItemById", BindingFlags.NonPublic | BindingFlags.Instance);
                        var method = generic.MakeGenericMethod (property.PropertyType);

                        DictionaryEntity itemDict = method.Invoke (this, new object[] { fromDictionary.Id }) as DictionaryEntity;
                        if (!object.Equals (itemDict, toValue)) {
                            property.SetValue (to, itemDict);
                            changes.Add (property.Name);
                        }
                    } else if (fromValue is BaseEntity) {
                        var rchange = Copy ((BaseEntity) fromValue, (BaseEntity) toValue, false);
                        if (rchange.Any ()) {
                            foreach (var c in rchange) {
                                changes.Add (c);
                            }
                            Session.SaveOrUpdate (toValue);
                        }
                    } else {
                        property.SetValue (to, fromValue);
                        changes.Add (property.Name);
                    }
                }
            }
            if (clearContext) {
                contextStudent = null;
            }
            return changes;
        }

        private TAdditional CreateAdditionalTypeByName<TAdditional, TAdditionalValue> (string AdditionalTypeName) where TAdditional : StudentAdditionalEntity<TAdditionalValue>, new () {
            var AdditionalTypeId = GetAdditionalTypeId (AdditionalTypeName);
            return new TAdditional () {
                Student = contextStudent,
                    TypeId = AdditionalTypeId
            };
        }

        private TAdditionalDictionary GetDictionaryItemByValue<TAdditionalDictionary> (string value) where TAdditionalDictionary : AdditionalDictionaryEntity {
            var item = Session
                .QueryOver<TAdditionalDictionary> ()
                .Where (p => p.Name == value)
                .SingleOrDefault<TAdditionalDictionary> ();

            return item;
        }

        private TDictionary GetDictionaryItemById<TDictionary> (int value) where TDictionary : DictionaryEntity {
            var item = Session
                .QueryOver<TDictionary> ()
                .Where (p => p.Id == value)
                .SingleOrDefault<TDictionary> ();

            return item;
        }

        private int GetAdditionalTypeId (string name) {
            return Session
                .QueryOver<AdditionalType> ()
                .Where (at => at.Name == name)
                .Select (at => at.Id)
                .SingleOrDefault<int> ();
        }
    }

}