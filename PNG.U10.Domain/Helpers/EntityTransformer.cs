using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NHibernate.Transform;
using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Helpers {
    public class EntityTransformer<TEntity> : IResultTransformer where TEntity : IBaseEntity, new () {
        List<string> listPaths = new List<string> ();
        public EntityTransformer () { }

        public IList TransformList (IList collection) {

            var results = new List<IBaseEntity> ();

            foreach (var item in collection) {
                var newRow = (item as IBaseEntity);
                var row = results.Where (x => x.Id == newRow.Id).FirstOrDefault ();
                if (row != null) {

                    foreach (var parts in listPaths) {
                        object toValue = row;
                        object fromValue = newRow;

                        foreach (var part in parts.Split ('/')) {

                            if (toValue is IList) {
                                fromValue = (fromValue as IList).Cast<IBaseEntity> ().FirstOrDefault ();
                                toValue = (toValue as IList).Cast<IBaseEntity> ().Where (x => x.Id == (fromValue as IBaseEntity).Id).FirstOrDefault ();
                            }

                            var oType = toValue.GetType ();
                            var property = oType.GetProperty (part);
                            toValue = property.GetValue (toValue);
                            fromValue = property.GetValue (fromValue);
                        }

                        //tutaj już listTo i list From powinny być listami;
                        IList<IBaseEntity> toList = (toValue as IList).Cast<IBaseEntity> ().ToList ();
                        IList<IBaseEntity> fromList = (fromValue as IList).Cast<IBaseEntity> ().ToList ();
                        foreach (var _item in fromList) {
                            if (!toList.Where (x => x.Id == _item.Id).Any ()) {
                                (toValue as IList).Add (_item);
                            }
                        }
                    }

                } else {
                    results.Add (newRow);
                }
            }

            return results;
        }

        public object TransformTuple (object[] tuple, string[] aliases) {
            TEntity root = new TEntity ();
            var listOfAliases = aliases.Select ((path, index) => {
                var value = tuple[index];
                return (path, value);
            });

            foreach (var alias in listOfAliases) {
                var o = (object) root;
                var parts = alias.path.TrimStart ('/').Split ('/');
                var last = parts.Length - 1;
                var current = 0;

                foreach (var part in parts) {
                    var oType = o.GetType ();

                    if (oType.IsGenericType && oType.GetGenericTypeDefinition () == typeof (List<>)) {
                        var list = (o as IList);

                        object v;

                        if (list.Count == 0) {
                            v = Activator.CreateInstance (oType.GenericTypeArguments.First ());
                            var path = String.Join ("/", parts.Take (parts.Count () - 1).ToArray ());
                            if (!listPaths.Contains (path)) {
                                listPaths.Add (path);
                            }
                            list.Add (v);
                        } else {
                            v = list[0];
                        }
                        o = v;
                        oType = o.GetType ();
                    }

                    var property = oType.GetProperty (part);
                    if (current != last) {
                        var v = property.GetValue (o);
                        if (v == null) {
                            if (typeof (BaseEntity).IsAssignableFrom (property.PropertyType)) {
                                v = Activator.CreateInstance (property.PropertyType);
                                property.SetValue (o, v);
                            } else if (property.PropertyType.IsGenericType && property.PropertyType.GetGenericTypeDefinition () == typeof (IList<>)) {
                                Type genericType = property.PropertyType.GenericTypeArguments.First ();
                                Type baseListType = typeof (List<>);
                                Type listType = baseListType.MakeGenericType (genericType);

                                v = Activator.CreateInstance (listType);
                                property.SetValue (o, v);
                            }
                        }
                        o = v;
                    } else {
                        property.SetValue (o, alias.value);
                    }
                    current++;
                }
            }

            return root;
        }
    }
}