namespace PNG.U10.Domain.Helpers {
    public class AdditionalTypes {
        /// <summary>
        /// OtherStatus
        /// </summary>
        public const string OtherStatus = "STATUS";

        /// <summary>
        /// ScholarshipStatus
        /// </summary>
        public const string ScholarshipStatus = "SCHOLARSHIP";

        /// <summary>
        /// ResidentialStatus
        /// </summary>
        public const string ResidentialStatus = "RESIDENTIAL";

        /// <summary>
        /// PortOfTravel
        /// </summary>
        public const string PortOfTravel = "PORT";

        /// <summary>
        /// Comment
        /// </summary>
        public const string Comment = "COMMENT";
    }

    public class OtherDictionaries {
        /// <summary>
        /// Obywatelstwo
        /// </summary>
        public const int Citizenship = 7;
        /// <summary>
        /// Narodowość
        /// </summary>
        public const int Nationality = 6;
    }
}