using System;
using PNG.U10.Domain.Entities;

namespace PNG.U10.Domain.Helpers {
    [AttributeUsage (AttributeTargets.Property)]
    public class AutocopyAttribute : Attribute {
        public AutocopyAttribute () { }
    }
    public class AutocopyDisabledAttribute : AutocopyAttribute {
        public AutocopyDisabledAttribute () { }
    }

    [AttributeUsage (AttributeTargets.Property)]
    public class AutocopyStudentAdditionalAttribute : AutocopyAttribute {
        public Type dictionaryType;
        public string additionalTypeName;
        public AutocopyStudentAdditionalAttribute () { }
    }

    [AttributeUsage (AttributeTargets.Property)]
    public class AutocopyDictionaryAttribute : AutocopyAttribute {
        public AutocopyDictionaryAttribute () { }
    }
}