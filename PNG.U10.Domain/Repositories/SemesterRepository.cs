using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Models;
using PNG.U10.Domain.Repositories.Abstract;

namespace PNG.U10.Domain.Repositories {

    /// <summary>
    /// Semester Repository
    /// </summary>
    public class SemesterRepository : BaseRepository, ISemesterRepository {
        public SemesterRepository (NHibernate.ISession session, IHttpContextAccessor httpContextAccessor) : base (session, httpContextAccessor) { }

        private IQueryOver<MembershipSemester, MembershipSemester> BuildBaseQuery () {
            MembershipSemester aMembershipSemester = null;
            Semester aSemester = null;
            Membership aMembership = null;
            Programme aProgramme = null;
            Department aDepartment = null;
            Institution aInstitution = null;

            return Session
                .QueryOver<MembershipSemester> (() => aMembershipSemester)
                .Left.JoinAlias (() => aMembershipSemester.Membership, () => aMembership)
                .Left.JoinAlias (() => aMembershipSemester.Semester, () => aSemester)
                .Left.JoinAlias (() => aMembership.Programme, () => aProgramme)
                .Left.JoinAlias (() => aMembership.Department, () => aDepartment)
                .Left.JoinAlias (() => aMembership.Institution, () => aInstitution)
                .Where (Restrictions.Where (() => aMembership.Base == 2))
                // .Select (
                //     Projections.ProjectionList ()
                //     .Add (Projections.Property (() => aSemester.Id).WithAlias (() => dSemester.Id))
                //     .Add (Projections.Property (() => aSemester.Number).WithAlias (() => dSemester.Number))
                //     .Add (Projections.Property (() => aSemester.Year).WithAlias (() => dSemester.Year))
                //     .Add (Projections.Property (() => aSemester.Active).WithAlias (() => dSemester.Active))
                //     .Add (Projections.Property (() => aMembership.Id).WithAlias (() => dSemester.MembershipId))

                // )
                .TransformUsing (Transformers.RootEntity);
        }

        public IEnumerable<MembershipSemester> Get (int[] institutions, int[] departments, int[] programmes, int pageNumber = 0, int pageSize = 100) {
            Department aDepartment = null;
            Institution aInstitution = null;
            Programme aProgramme = null;
            Semester aSemester = null;

            var query = BuildBaseQuery ();

            if (institutions.Length > 0) {
                query = query.Where (() => aInstitution.Id.IsIn (institutions));
            }
            if (departments.Length > 0) {
                query = query.Where (() => aDepartment.Id.IsIn (departments));
            }
            if (programmes.Length > 0) {
                query = query.Where (() => aProgramme.Id.IsIn (programmes));
            }

            return query
                .OrderBy (() => aSemester.Number).Asc
                .Skip (pageSize * pageNumber).Take (pageSize).List<MembershipSemester> ();
        }

        public IEnumerable<MembershipSemester> Get (int pageNumber = 0, int pageSize = 100) {
            List<int> institutions = new List<int> ();
            List<int> departments = new List<int> ();
            List<int> programmes = new List<int> ();

            return Get (institutions.ToArray (), departments.ToArray (), programmes.ToArray (), pageNumber, pageSize);
        }

        public MembershipSemester GetOne (int id) {
            MembershipSemester aMembershipSemester = null;

            return BuildBaseQuery ()
                .Where (() => aMembershipSemester.Id == id)
                .SingleOrDefault<MembershipSemester> ();
        }
    }
}