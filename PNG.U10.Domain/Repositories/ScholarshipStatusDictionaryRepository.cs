using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Models;
using PNG.U10.Domain.Repositories.Abstract;

namespace PNG.U10.Domain.Repositories {
    /// <summary>
    /// ScholarshipStatusDictionary Repository
    /// </summary>
    public class ScholarshipStatusDictionaryRepository : BaseRepository, IScholarshipStatusDictionaryRepository {
        public ScholarshipStatusDictionaryRepository (NHibernate.ISession session, IHttpContextAccessor httpContextAccessor) : base (session, httpContextAccessor) { }

        private IQueryOver<ScholarshipStatusDictionary, ScholarshipStatusDictionary> BuildBaseQuery () {
            ScholarshipStatusDictionary aStudentScholarshipStatusDictionary = null;
            // ScholarshipStatusDictionaryDTO dStudentScholarshipStatusDictionary = null;

            return Session
                .QueryOver<ScholarshipStatusDictionary> (() => aStudentScholarshipStatusDictionary)
                .Where (() => aStudentScholarshipStatusDictionary.Active == true)
                // .Select (
                //     Projections.ProjectionList ()
                //     .Add (Projections.Property (() => aStudentScholarshipStatusDictionary.Id).WithAlias (() => dStudentScholarshipStatusDictionary.Id))
                //     .Add (Projections.Property (() => aStudentScholarshipStatusDictionary.Name).WithAlias (() => dStudentScholarshipStatusDictionary.Name))
                // )
                .TransformUsing (Transformers.RootEntity);
        }

        public IEnumerable<ScholarshipStatusDictionary> Get (int pageNumber = 0, int pageSize = 100) {
            return BuildBaseQuery ()
                .OrderBy (p => p.Name).Asc
                .Skip (pageSize * pageNumber).Take (pageSize).List<ScholarshipStatusDictionary> ();
        }

        public ScholarshipStatusDictionary GetOne (int id) {
            ScholarshipStatusDictionary aStudentOtherStatusDictionary = null;

            return BuildBaseQuery ()
                .Where (() => aStudentOtherStatusDictionary.Id == id)
                .SingleOrDefault<ScholarshipStatusDictionary> ();
        }
    }
}