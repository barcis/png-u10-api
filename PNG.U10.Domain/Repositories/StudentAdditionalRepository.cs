using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using NHibernate;
using NHibernate.Transform;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Repositories.Abstract;

namespace PNG.U10.Domain.Repositories {
    /// <summary>
    /// StudentAdditional Repository
    /// </summary>
    public class StudentAdditionalRepository : BaseRepository, IStudentAdditionalRepository {
        public StudentAdditionalRepository (NHibernate.ISession session, IHttpContextAccessor httpContextAccessor) : base (session, httpContextAccessor) { }

        private IQueryOver<StudentAdditional, StudentAdditional> BuildBaseQuery () {
            StudentAdditional aStudentAdditional = null;
            return Session
                .QueryOver<StudentAdditional> (() => aStudentAdditional)
                .TransformUsing (Transformers.RootEntity);
        }

        public IEnumerable<StudentAdditional> Get (int pageNumber = 0, int pageSize = 100) {
            return BuildBaseQuery ()
                .Skip (pageSize * pageNumber).Take (pageSize).List<StudentAdditional> ();
        }

        public IEnumerable<StudentAdditional> Get (int studentId, int pageNumber, int pageSize) {
            StudentAdditional aStudentAdditional = null;
            return BuildBaseQuery ()
                .Where (() => aStudentAdditional.Student.Id == studentId)
                .Skip (pageSize * pageNumber).Take (pageSize).List<StudentAdditional> ();
        }

        public StudentAdditional GetOne (int id) {
            StudentAdditional aStudentAdditional = null;

            return BuildBaseQuery ()
                .Where (() => aStudentAdditional.Id == id)
                .SingleOrDefault<StudentAdditional> ();
        }

        public StudentAdditional GetOne (int studentId, int typeId) {
            StudentAdditional aStudentAdditional = null;

            var result = BuildBaseQuery ()
                .Where (() => aStudentAdditional.Student.Id == studentId)
                .Where (() => aStudentAdditional.Type.Id == typeId)
                .SingleOrDefault<StudentAdditional> ();

            if (result.Type.Edit == 2) {
                AdditionalDictionary additionalDictionary = null;
                result.Type.Options = Session.QueryOver<AdditionalDictionary> (() => additionalDictionary)
                    .Where (() => additionalDictionary.TypeId == typeId)
                    .List<AdditionalDictionary> ();
            }

            return result;
        }

    }
}