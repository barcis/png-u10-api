using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Repositories.Abstract;

namespace PNG.U10.Domain.Repositories {
    /// <summary>
    /// PortOfTravelDictionary Repository
    /// </summary>
    public class PortOfTravelDictionaryRepository : BaseRepository, IPortOfTravelDictionaryRepository {
        public PortOfTravelDictionaryRepository (NHibernate.ISession session, IHttpContextAccessor httpContextAccessor) : base (session, httpContextAccessor) { }

        private IQueryOver<PortOfTravelDictionary, PortOfTravelDictionary> BuildBaseQuery () {
            PortOfTravelDictionary aStudentPortOfTravelDictionary = null;
            // PortOfTravelDictionaryDTO dStudentPortOfTravelDictionary = null;

            return Session
                .QueryOver<PortOfTravelDictionary> (() => aStudentPortOfTravelDictionary)
                .Where (() => aStudentPortOfTravelDictionary.Active == true)
                // .Select (
                //     Projections.ProjectionList ()
                //     .Add (Projections.Property (() => aStudentPortOfTravelDictionary.Id).WithAlias (() => dStudentPortOfTravelDictionary.Id))
                //     .Add (Projections.Property (() => aStudentPortOfTravelDictionary.Name).WithAlias (() => dStudentPortOfTravelDictionary.Name))
                // )
                .TransformUsing (Transformers.RootEntity);
        }

        public IEnumerable<PortOfTravelDictionary> Get (int pageNumber = 0, int pageSize = 100) {
            return BuildBaseQuery ()
                .OrderBy (p => p.Name).Asc
                .Skip (pageSize * pageNumber).Take (pageSize).List<PortOfTravelDictionary> ();
        }

        public PortOfTravelDictionary GetOne (int id) {
            PortOfTravelDictionary aStudentPortOfTravelDictionary = null;

            return BuildBaseQuery ()
                .Where (() => aStudentPortOfTravelDictionary.Id == id)
                .SingleOrDefault<PortOfTravelDictionary> ();
        }
    }
}