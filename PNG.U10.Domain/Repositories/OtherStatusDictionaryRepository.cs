using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using NHibernate;
using NHibernate.Transform;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Repositories.Abstract;

namespace PNG.U10.Domain.Repositories {
    /// <summary>
    /// OtherStatusDictionary Repository
    /// </summary>
    public class OtherStatusDictionaryRepository : BaseRepository, IOtherStatusDictionaryRepository {
        public OtherStatusDictionaryRepository (NHibernate.ISession session, IHttpContextAccessor httpContextAccessor) : base (session, httpContextAccessor) { }

        private IQueryOver<OtherStatusDictionary, OtherStatusDictionary> BuildBaseQuery () {
            OtherStatusDictionary aStudentOtherStatusDictionary = null;
            // OtherStatusDictionaryDTO dStudentOtherStatusDictionary = null;

            return Session
                .QueryOver<OtherStatusDictionary> (() => aStudentOtherStatusDictionary)
                .Where (() => aStudentOtherStatusDictionary.Active == true)
                // .Select (
                //     Projections.ProjectionList ()
                //     .Add (Projections.Property (() => aStudentOtherStatusDictionary.Id).WithAlias (() => dStudentOtherStatusDictionary.Id))
                //     .Add (Projections.Property (() => aStudentOtherStatusDictionary.Name).WithAlias (() => dStudentOtherStatusDictionary.Name))
                // )
                .TransformUsing (Transformers.RootEntity);
        }

        public IEnumerable<OtherStatusDictionary> Get (int pageNumber = 0, int pageSize = 100) {
            return BuildBaseQuery ()
                .OrderBy (p => p.Name).Asc
                .Skip (pageSize * pageNumber).Take (pageSize).List<OtherStatusDictionary> ();
        }

        public OtherStatusDictionary GetOne (int id) {
            OtherStatusDictionary aStudentOtherStatusDictionary = null;

            return BuildBaseQuery ()
                .Where (() => aStudentOtherStatusDictionary.Id == id)
                .SingleOrDefault<OtherStatusDictionary> ();
        }
    }
}