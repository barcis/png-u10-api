using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Entities;

namespace PNG.U10.Domain.Repositories.Abstract {
    public interface ISemesterRepository : IReadRepository<MembershipSemester> {

        /// <summary>
        /// Get semesters by institution, department and programme ids
        /// </summary>
        IEnumerable<MembershipSemester> Get (int[] institutions, int[] departments, int[] programmes, int pageNumber = 0, int pageSize = 100);
    }
}