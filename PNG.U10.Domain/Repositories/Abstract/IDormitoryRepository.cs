using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Entities;

namespace PNG.U10.Domain.Repositories.Abstract {
    /// <summary>
    /// Interface of Dormitory Repository
    /// </summary>
    public interface IDormitoryRepository : IReadRepository<Dormitory> {
        IEnumerable<DormitoryRoom> GetRooms ();
        IEnumerable<DormitoryRoom> GetRooms (int id);
        DormitoryRoom GetRoom (int id);

    }
}