using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Entities;

namespace PNG.U10.Domain.Repositories.Abstract {
    public interface IDepartmentRepository : IReadRepository<Department> {
        IEnumerable<Department> Get (int[] institutions, int pageNumber = 0, int pageSize = 100);
    }
}