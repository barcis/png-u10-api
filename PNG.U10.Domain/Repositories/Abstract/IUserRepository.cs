using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Entities;

namespace PNG.U10.Domain.Repositories.Abstract {

    /// <summary>
    /// Interface of User Repository
    /// </summary>
    public interface IUserRepository : IReadRepository<User> {
        IEnumerable<User> GetByRole (string role, int pageNumber = 0, int pageSize = 100);

        User GetByPersonId (int personId);
        User GetBySubId (string subId);
    }
}