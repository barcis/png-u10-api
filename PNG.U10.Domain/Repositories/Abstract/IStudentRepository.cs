using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Repositories.Filters;

namespace PNG.U10.Domain.Repositories.Abstract {
    public interface IStudentRepository : IReadRepository<Student>, IUpdateRepository<Student> {
        IEnumerable<Student> Get (int pageNumber = 0, int pageSize = 100, StudentFilter filters = null);
        Student GetOne (StudentFilter filters = null);
        Student UpdateOne (StudentFilter filters, Student student);
        Student CreateOne (Student student);
    }
}