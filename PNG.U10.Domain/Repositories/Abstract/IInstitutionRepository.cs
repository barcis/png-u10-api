using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Models;

namespace PNG.U10.Domain.Repositories.Abstract {
    /// <summary>
    /// Interface of User Repository
    /// </summary>
    public interface IInstitutionRepository : IReadRepository<Institution> {
        IEnumerable<Institution> Get (int[] institutions, int pageNumber = 0, int pageSize = 100);
    }
}