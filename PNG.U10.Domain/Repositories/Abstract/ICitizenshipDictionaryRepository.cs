using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Entities;

namespace PNG.U10.Domain.Repositories.Abstract {
    public interface ICitizenshipDictionaryRepository : IReadRepository<CitizenshipDictionary> { }
}