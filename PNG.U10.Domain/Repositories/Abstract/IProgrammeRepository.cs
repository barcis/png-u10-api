using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Entities;

namespace PNG.U10.Domain.Repositories.Abstract {
    public interface IProgrammeRepository : IReadRepository<Programme> {

        /// <summary>
        /// Get programes by institution and department ids
        /// </summary>
        IEnumerable<Programme> Get (int[] institutions, int[] departments, int pageNumber = 0, int pageSize = 100);
    }
}