using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Entities;

namespace PNG.U10.Domain.Repositories.Abstract {
    public interface IStudentAdditionalRepository : IReadRepository<StudentAdditional> {
        IEnumerable<StudentAdditional> Get (int studentId, int pageNumber, int pageSize);
        StudentAdditional GetOne (int studentId, int typeId);

    }
    public interface IMembershipAdditionalRepository : IReadRepository<MembershipAdditional> {
        IEnumerable<MembershipAdditional> Get (int membershipId, int pageNumber, int pageSize);
        MembershipAdditional GetOne (int membershipId, int typeId);

    }
}