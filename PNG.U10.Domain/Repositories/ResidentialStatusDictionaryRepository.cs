using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Models;
using PNG.U10.Domain.Repositories.Abstract;

namespace PNG.U10.Domain.Repositories {
    /// <summary>
    /// ResidentialStatusDictionary Repository
    /// </summary>
    public class ResidentialStatusDictionaryRepository : BaseRepository, IResidentialStatusDictionaryRepository {
        public ResidentialStatusDictionaryRepository (NHibernate.ISession session, IHttpContextAccessor httpContextAccessor) : base (session, httpContextAccessor) { }

        private IQueryOver<ResidentialStatusDictionary, ResidentialStatusDictionary> BuildBaseQuery () {
            ResidentialStatusDictionary aStudentResidentialStatusDictionary = null;
            // ResidentialStatusDictionaryDTO dStudentResidentialStatusDictionary = null;

            return Session
                .QueryOver<ResidentialStatusDictionary> (() => aStudentResidentialStatusDictionary)
                .Where (() => aStudentResidentialStatusDictionary.Active == true)
                // .Select (
                //     Projections.ProjectionList ()
                //     .Add (Projections.Property (() => aStudentResidentialStatusDictionary.Id).WithAlias (() => dStudentResidentialStatusDictionary.Id))
                //     .Add (Projections.Property (() => aStudentResidentialStatusDictionary.Name).WithAlias (() => dStudentResidentialStatusDictionary.Name))
                // )
                .TransformUsing (Transformers.RootEntity);
        }

        public IEnumerable<ResidentialStatusDictionary> Get (int pageNumber = 0, int pageSize = 100) {
            return BuildBaseQuery ()
                .OrderBy (p => p.Name).Asc
                .Skip (pageSize * pageNumber).Take (pageSize).List<ResidentialStatusDictionary> ();
        }

        public ResidentialStatusDictionary GetOne (int id) {
            ResidentialStatusDictionary aStudentResidentialStatusDictionary = null;

            return BuildBaseQuery ()
                .Where (() => aStudentResidentialStatusDictionary.Id == id)
                .SingleOrDefault<ResidentialStatusDictionary> ();
        }
    }
}