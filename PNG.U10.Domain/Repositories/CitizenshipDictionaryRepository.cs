using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using NHibernate;
using NHibernate.Transform;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Repositories.Abstract;

namespace PNG.U10.Domain.Repositories {
    /// <summary>
    /// CitizenshipDictionary Repository
    /// </summary>
    public class CitizenshipDictionaryRepository : BaseRepository, ICitizenshipDictionaryRepository {
        public CitizenshipDictionaryRepository (NHibernate.ISession session, IHttpContextAccessor httpContextAccessor) : base (session, httpContextAccessor) { }

        private IQueryOver<CitizenshipDictionary, CitizenshipDictionary> BuildBaseQuery () {
            CitizenshipDictionary aCitizenshipDictionary = null;
            return Session
                .QueryOver<CitizenshipDictionary> (() => aCitizenshipDictionary)
                .Where (() => aCitizenshipDictionary.Active == true)
                .TransformUsing (Transformers.RootEntity);
        }

        public IEnumerable<CitizenshipDictionary> Get (int pageNumber = 0, int pageSize = 100) {
            return BuildBaseQuery ()
                .OrderBy (p => p.OrdinalNumber).Asc
                .Skip (pageSize * pageNumber).Take (pageSize).List<CitizenshipDictionary> ();
        }

        public CitizenshipDictionary GetOne (int id) {
            CitizenshipDictionary aCitizenshipDictionary = null;

            return BuildBaseQuery ()
                .Where (() => aCitizenshipDictionary.Id == id)
                .SingleOrDefault<CitizenshipDictionary> ();
        }
    }
}