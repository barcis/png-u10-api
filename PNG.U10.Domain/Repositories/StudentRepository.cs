using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using NHibernate;
using NHibernate.Criterion;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Helpers;
using PNG.U10.Domain.Repositories.Abstract;
using PNG.U10.Domain.Repositories.Filters;

namespace PNG.U10.Domain.Repositories {

    /// <summary>
    /// Student Repository
    /// </summary>
    public class StudentRepository : BaseRepository, IStudentRepository {

        IAutocopyValues AutocopyValues;
        public StudentRepository (NHibernate.ISession session, IHttpContextAccessor httpContextAccessor, IAutocopyValues autocopyValues) : base (session, httpContextAccessor) => AutocopyValues = autocopyValues;

        private IQueryOver<Student, Student> BuildBaseQuery () {

            Student aStudent = null;
            StudentAllocator aAllocator = null;
            StudentComment aComment = null;
            StudentPortOfTravel aPort = null;
            StudentResidentialStatus aResidentialStatus = null;
            StudentScholarshipStatus aScholarshipStatus = null;
            StudentOtherStatus aOtherStatus = null;
            StudentEnrollmentStatus aStudentEnrollmentStatus = null;
            Membership aMembership = null;
            Institution aInstitution = null;
            Department aDepartment = null;
            Programme aProgramme = null;
            Module aModule = null;
            Specialty aSpecialty = null;
            Specialization aSpecialization = null;
            Kind aKind = null;
            Mode aMode = null;
            MembershipSemester aMembershipSemester = null;
            Semester aSemester = null;
            EnrollmentStatusDictionary aEnrollmentStatusDictionary = null;
            Person aPerson = null;
            PlaceDictionary aPlaceOfBirth = null;
            CitizenshipDictionary aCitizenship = null;

            var list = Projections.ProjectionList ();
            addProjections<Student> (list, "/", () => aStudent);
            addProjections<Person> (list, "/Person/", () => aPerson);
            addProjections<PlaceDictionary> (list, "/Person/PlaceOfBirth/", () => aPlaceOfBirth);
            addProjections<CitizenshipDictionary> (list, "/Person/Citizenships/", () => aCitizenship);
            addProjections<StudentAllocator> (list, "/Allocator/", () => aAllocator);
            addProjections<StudentComment> (list, "/Comment/", () => aComment);
            addProjections<StudentPortOfTravel> (list, "/Port/", () => aPort);
            addProjections<StudentResidentialStatus> (list, "/ResidentialStatus/", () => aResidentialStatus);
            addProjections<StudentScholarshipStatus> (list, "/ScholarshipStatus/", () => aScholarshipStatus);
            addProjections<StudentOtherStatus> (list, "/OtherStatus/", () => aOtherStatus);
            addProjections<Membership> (list, "/Memberships/", () => aMembership);

            addProjections<Institution> (list, "/Memberships/Institution/", () => aInstitution);
            addProjections<Department> (list, "/Memberships/Department/", () => aDepartment);
            addProjections<Programme> (list, "/Memberships/Programme/", () => aProgramme);
            addProjections<Kind> (list, "/Memberships/Kind/", () => aKind);
            addProjections<Module> (list, "/Memberships/Modules/", () => aModule);
            addProjections<Specialty> (list, "/Memberships/Modules/Specialty/", () => aSpecialty);
            addProjections<Specialization> (list, "/Memberships/Modules/Specialization/", () => aSpecialization);
            addProjections<Mode> (list, "/Memberships/Kind/Mode/", () => aMode);

            addProjections<MembershipSemester> (list, "/Memberships/MembershipSemesters/", () => aMembershipSemester);
            addProjections<Semester> (list, "/Memberships/MembershipSemesters/Semester/", () => aSemester);
            addProjections<StudentEnrollmentStatus> (list, "/Memberships/MembershipSemesters/Statuses/", () => aStudentEnrollmentStatus);
            addProjections<EnrollmentStatusDictionary> (list, "/Memberships/MembershipSemesters/Statuses/Value/", () => aEnrollmentStatusDictionary);

            var query = Session.QueryOver (() => aStudent)
                .JoinAlias (() => aStudent.Person, () => aPerson)
                .Left.JoinAlias (() => aPerson.PlaceOfBirth, () => aPlaceOfBirth)
                .Left.JoinAlias (() => aPerson.Citizenships, () => aCitizenship)
                .Left.JoinAlias (() => aStudent.Allocator, () => aAllocator)
                .Left.JoinAlias (() => aStudent.Comment, () => aComment)
                .Left.JoinAlias (() => aStudent.Port, () => aPort)
                .Left.JoinAlias (() => aStudent.ResidentialStatus, () => aResidentialStatus)
                .Left.JoinAlias (() => aStudent.ScholarshipStatus, () => aScholarshipStatus)
                .Left.JoinAlias (() => aStudent.OtherStatus, () => aOtherStatus)
                .Left.JoinAlias (() => aStudent.Memberships, () => aMembership)
                .Left.JoinAlias (() => aMembership.Institution, () => aInstitution)
                .Left.JoinAlias (() => aMembership.Department, () => aDepartment)
                .Left.JoinAlias (() => aMembership.Programme, () => aProgramme)
                .Left.JoinAlias (() => aMembership.Kind, () => aKind)
                .Left.JoinAlias (() => aMembership.Modules, () => aModule)
                .Left.JoinAlias (() => aModule.Specialty, () => aSpecialty)
                .Left.JoinAlias (() => aModule.Specialization, () => aSpecialization)
                .Left.JoinAlias (() => aKind.Mode, () => aMode)
                .Left.JoinAlias (() => aMembership.MembershipSemesters, () => aMembershipSemester)
                .Left.JoinAlias (() => aMembershipSemester.Statuses, () => aStudentEnrollmentStatus)
                .Left.JoinAlias (() => aMembershipSemester.Semester, () => aSemester)
                .Left.JoinAlias (() => aStudentEnrollmentStatus.Value, () => aEnrollmentStatusDictionary)
                .Select (list);

            return query;
        }

        /// <summary>
        /// Get one student by id
        /// </summary>
        /// <param name="id">Student id.</param>
        public Student GetOne (int id) {
            StudentFilter filter = new StudentFilter () {
                Id = id
            };

            return GetOne (filter);
        }

        /// <summary>
        /// Get one student by id
        /// </summary>
        /// <param name="id">Student id.</param>
        public Student GetOne (StudentFilter filter = null) {

            var query = BuildBaseQuery ();
            if (filter != null) {

                if (filter.Id.HasValue) {
                    Student aStudent = null;
                    query = query.Where (() => aStudent.Id == filter.Id.Value);
                }

                if (filter.PersonId.HasValue) {
                    Person aPerson = null;
                    query = query.Where (() => aPerson.Id == filter.PersonId.Value);
                }

                if (filter.PersonSubId != null) {
                    Person aPerson = null;
                    query = query.Where (() => aPerson.SubId == filter.PersonSubId);
                }

                if (filter.Uni10Id != null) {
                    Student aStudent = null;
                    query = query.Where (() => aStudent.Uni10Id == filter.Uni10Id);
                }

                if (filter.Allocator != null) {
                    StudentAllocator aStudentAllocator = null;
                    query = query.Where (() => aStudentAllocator.Value == filter.Allocator);
                }
            }
            var student = query
                .TransformUsing (new EntityTransformer<Student> ())
                .SingleOrDefault<Student> ();

            return student;
        }

        /// <summary>
        /// Get all students
        /// </summary>
        public IEnumerable<Student> Get (int pageNumber = 0, int pageSize = 100) {
            return Get (pageNumber, pageSize, null);
        }

        /// <summary>
        /// Get all students
        /// </summary>
        public IEnumerable<Student> Get (int pageNumber = 0, int pageSize = 100, StudentFilter filter = null) {
            var query = BuildBaseQuery ();
            if (filter != null) {
                if (filter.Allocator != null) {
                    StudentAllocator aStudentAllocator = null;
                    query = query.Where (() => aStudentAllocator.Value == filter.Allocator);
                }
            }

            Person aPerson = null;
            var items = query.TransformUsing (new EntityTransformer<Student> ())
                .OrderBy (() => aPerson.Surname).Asc
                .Skip (pageNumber * pageSize).Take (pageSize)
                .List<Student> ();

            return items;
        }

        public Student CreateOne (Student student) {
            using (var transaction = Session.BeginTransaction ()) {
                if (student == null) {
                    return null;
                }

                student.Id = null;
                student.Person.Id = null;
                student.Person.PlaceOfBirth = null;

                Session.Save (student.Person);
                Session.Save (student);

                transaction.Commit ();

                return student;
            }
        }

        public Student UpdateOne (int id, Student student) {
            StudentFilter filter = new StudentFilter () {
                Id = id
            };

            return UpdateOne (filter, student);
        }

        public Student UpdateOne (StudentFilter filters, Student _student) {
            using (var transaction = Session.BeginTransaction ()) {
                Student student = GetOne (filters);
                if (student == null) {
                    return null;
                }

                var changes = AutocopyValues.Copy (_student, student);

                if (student.Person.PlaceOfBirth.Id.GetValueOrDefault () == 0) {
                    student.Person.PlaceOfBirth = null;
                }

                if (changes.Any ()) {
                    Session.Update (student);
                }
                transaction.Commit ();

                return student;
            }
        }

        private TAdditional CreateAdditionalTypeByName<TAdditional, TAdditionalValue> (Student student, string AdditionalTypeName) where TAdditional : StudentAdditionalEntity<TAdditionalValue>, new () {
            var AdditionalTypeId = GetAdditionalTypeId (AdditionalTypeName);
            return new TAdditional () {
                Student = student,
                    TypeId = AdditionalTypeId
            };
        }

        private int GetAdditionalTypeId (string name) {
            return Session
                .QueryOver<AdditionalType> ()
                .Where (at => at.Name == name)
                .Select (at => at.Id)
                .SingleOrDefault<int> ();
        }
    }

}