using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Models;
using PNG.U10.Domain.Repositories.Abstract;

namespace PNG.U10.Domain.Repositories {
    /// <summary>
    /// StatusName Repository
    /// </summary>
    public class EnrollmentStatusDictionaryRepository : BaseRepository, IEnrollmentStatusDictionaryRepository {
        public EnrollmentStatusDictionaryRepository (NHibernate.ISession session, IHttpContextAccessor httpContextAccessor) : base (session, httpContextAccessor) { }

        private IQueryOver<EnrollmentStatusDictionary, EnrollmentStatusDictionary> BuildBaseQuery () {
            EnrollmentStatusDictionary aEnrollmentStatusDictionary = null;
            // EnrollmentStatusDictionaryDTO dStatusName = null;

            return Session
                .QueryOver<EnrollmentStatusDictionary> (() => aEnrollmentStatusDictionary)
                .Where (() => aEnrollmentStatusDictionary.Active == true)
                // .Select (
                //     Projections.ProjectionList ()
                //     .Add (Projections.Property (() => aStatusName.Id).WithAlias (() => dStatusName.Id))
                //     .Add (Projections.Property (() => aStatusName.Name).WithAlias (() => dStatusName.Name))
                // )
                .TransformUsing (Transformers.RootEntity);
        }

        public IEnumerable<EnrollmentStatusDictionary> Get (int pageNumber = 0, int pageSize = 100) {
            return BuildBaseQuery ()
                .OrderBy (p => p.Name).Asc
                .Skip (pageSize * pageNumber).Take (pageSize)
                .List<EnrollmentStatusDictionary> ();
        }

        public EnrollmentStatusDictionary GetOne (int id) {
            EnrollmentStatusDictionary aEnrollmentStatusDictionary = null;

            return BuildBaseQuery ()
                .Where (() => aEnrollmentStatusDictionary.Id == id)
                .SingleOrDefault<EnrollmentStatusDictionary> ();
        }
    }
}