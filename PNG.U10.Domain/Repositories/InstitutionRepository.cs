using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Models;
using PNG.U10.Domain.Repositories.Abstract;

namespace PNG.U10.Domain.Repositories {
    /// <summary>
    /// Institution Repository
    /// </summary>
    public class InstitutionRepository : BaseRepository, IInstitutionRepository {
        private readonly IMapper mapper;
        public InstitutionRepository (
            NHibernate.ISession session,
            IHttpContextAccessor httpContextAccessor,
            IMapper _mapper) : base (session, httpContextAccessor) {
            mapper = _mapper;
        }

        private IQueryOver<Institution, Institution> BuildBaseQuery () {
            Institution aInstitution = null;

            return Session.QueryOver<Institution> (() => aInstitution)
                .TransformUsing (Transformers.RootEntity);
        }

        public IEnumerable<Institution> Get (int[] institutions, int pageNumber = 0, int pageSize = 100) {
            Institution aInstitution = null;

            return BuildBaseQuery ()
                .WhereRestrictionOn (() => aInstitution.Id).IsIn (institutions)
                .OrderBy (() => aInstitution.Name).Asc
                .Skip (pageSize * pageNumber).Take (pageSize).List<Institution> ();
        }

        public IEnumerable<Institution> Get (int pageNumber = 0, int pageSize = 100) {
            Institution aInstitution = null;

            return BuildBaseQuery ()
                .OrderBy (() => aInstitution.Name).Asc
                .Skip (pageSize * pageNumber)
                .Take (pageSize)
                .List<Institution> ();
        }

        public Institution GetOne (int id) {
            Institution aInstitution = null;

            return BuildBaseQuery ()
                .Where (() => aInstitution.Id == id)
                .SingleOrDefault<Institution> ();
        }
    }
}