﻿namespace PNG.U10.Domain.Repositories.Filters {
    public class StudentFilter {
        public int? Id { get; set; }
        public int? PersonId { get; set; }
        public string PersonSubId { get; set; }
        public string Uni10Id { get; set; }
        public string Text { get; set; }
        public string Allocator { get; set; }
        public int? MembershipId { get; set; }
        public int[] Institutions { get; set; }
        public int[] Departments { get; set; }
        public int[] Programmes { get; set; }
        public int[] Semesters { get; set; }
    }
}