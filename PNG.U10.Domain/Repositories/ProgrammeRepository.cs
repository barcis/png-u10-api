using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Models;
using PNG.U10.Domain.Repositories.Abstract;

namespace PNG.U10.Domain.Repositories {
    /// <summary>
    /// Programme Repository
    /// </summary>
    public class ProgrammeRepository : BaseRepository, IProgrammeRepository {
        public ProgrammeRepository (NHibernate.ISession session, IHttpContextAccessor httpContextAccessor) : base (session, httpContextAccessor) { }

        private IQueryOver<Programme, Programme> BuildBaseQuery () {
            Programme aProgramme = null;
            Department aDepartment = null;
            Institution aInstitution = null;
            // ProgrammeDTO dProgramme = null;

            return Session
                .QueryOver<Programme> (() => aProgramme)
                .Left.JoinAlias (() => aProgramme.Department, () => aDepartment)
                .Left.JoinAlias (() => aDepartment.Institution, () => aInstitution)
                // .Select (
                //     Projections.ProjectionList ()
                //     .Add (Projections.Property (() => aProgramme.Id).WithAlias (() => dProgramme.Id))
                //     .Add (Projections.Property (() => aProgramme.Name).WithAlias (() => dProgramme.Name))
                //     .Add (Projections.Property (() => aDepartment.Id).WithAlias (() => dProgramme.DepartmentId))

                // )
                .TransformUsing (Transformers.RootEntity);
        }

        public IEnumerable<Programme> Get (int pageNumber = 0, int pageSize = 100) {
            return BuildBaseQuery ()
                .OrderBy (p => p.Name).Asc
                .Skip (pageSize * pageNumber).Take (pageSize).List<Programme> ();
        }

        public IEnumerable<Programme> Get (int[] institutions, int[] departments, int pageNumber = 0, int pageSize = 100) {
            Department aDepartment = null;
            Institution aInstitution = null;

            var query = BuildBaseQuery ();

            if (institutions.Length > 0) {
                query = query.Where (() => aInstitution.Id.IsIn (institutions));
            }

            if (departments.Length > 0) {
                query = query.Where (() => aDepartment.Id.IsIn (departments));
            }

            return query
                .OrderBy (p => p.Name).Asc
                .Skip (pageSize * pageNumber).Take (pageSize).List<Programme> ();
        }

        public Programme GetOne (int id) {
            Programme aProgramme = null;

            return BuildBaseQuery ()
                .Where (() => aProgramme.Id == id)
                .SingleOrDefault<Programme> ();
        }
    }
}