using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using NHibernate;
using NHibernate.Transform;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Repositories.Abstract;

namespace PNG.U10.Domain.Repositories {
    /// <summary>
    /// User Repository
    /// </summary>
    public class UserRepository : BaseRepository, IUserRepository {
        public UserRepository (NHibernate.ISession session, IHttpContextAccessor httpContextAccessor) : base (session, httpContextAccessor) { }
        private IQueryOver<User, User> BuildBaseUserQuery () {
            User aUser = null;

            return Session.QueryOver (() => aUser)
                .TransformUsing (Transformers.DistinctRootEntity);

        }

        /// <summary>
        /// Get one user by id
        /// </summary>
        /// <param name="id">User id.</param>
        public User GetOne (int id) {
            User aUser = null;
            return BuildBaseUserQuery ()
                .Where (() => aUser.Id == id)
                .SingleOrDefault<User> ();
        }

        /// <summary>
        /// Get one user by person id
        /// </summary>
        /// <param name="personId">Person id.</param>
        public User GetByPersonId (int personId) {
            Person aPerson = null;

            return BuildBaseUserQuery ()
                .Where (() => aPerson.Id == personId)
                .SingleOrDefault<User> ();
        }

        /// <summary>
        /// Get all users
        /// </summary>
        public IEnumerable<User> Get (int pageNumber = 0, int pageSize = 100) {
            return BuildBaseUserQuery ()
                .Skip (pageSize * pageNumber).Take (pageSize)
                .List<User> ();

        }
        /// <summary>
        /// Get all users with role
        /// </summary>
        public IEnumerable<User> GetByRole (string role, int pageNumber = 0, int pageSize = 100) {
            User aUser = null;
            Person aPerson = null;
            Role aRole = null;

            return BuildBaseUserQuery ()
                .JoinAlias (() => aUser.Person, () => aPerson)
                .JoinAlias (() => aPerson.Roles, () => aRole)
                .Where (() => aRole.Name == role)
                .Skip (pageSize * pageNumber).Take (pageSize).List<User> ();
        }

        public User GetBySubId (string subId) {
            User aUser = null;
            Person aPerson = null;

            var user = BuildBaseUserQuery ()
                .JoinAlias (() => aUser.Person, () => aPerson)
                .Where (() => aPerson.SubId == subId)
                .SingleOrDefault<User> ();
            return user;
        }
    }
}