using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Models;
using PNG.U10.Domain.Repositories.Abstract;

namespace PNG.U10.Domain.Repositories {
    /// <summary>
    /// Department Repository
    /// </summary>
    public class DepartmentRepository : BaseRepository, IDepartmentRepository {
        public DepartmentRepository (NHibernate.ISession session, IHttpContextAccessor httpContextAccessor) : base (session, httpContextAccessor) { }

        private IQueryOver<Department, Department> BuildBaseQuery () {
            Department aDepartment = null;
            Institution aInstitution = null;
            // DepartmentDTO dDepartment = null;

            return Session.QueryOver<Department> (() => aDepartment)
                .JoinAlias (() => aDepartment.Institution, () => aInstitution)
                .TransformUsing (Transformers.RootEntity);
        }
        public IEnumerable<Department> Get (int pageNumber = 0, int pageSize = 100) {
            Department aDepartment = null;

            return BuildBaseQuery ()
                .OrderBy (() => aDepartment.Name).Asc
                .Skip (pageSize * pageNumber).Take (pageSize).List<Department> ();
        }

        public IEnumerable<Department> Get (int[] institutions, int pageNumber = 0, int pageSize = 100) {
            Department aDepartment = null;
            Institution aInstitution = null;

            return BuildBaseQuery ()
                .WhereRestrictionOn (() => aInstitution.Id).IsIn (institutions)
                .OrderBy (() => aDepartment.Name).Asc
                .Skip (pageSize * pageNumber).Take (pageSize).List<Department> ();
        }

        public Department GetOne (int id) {
            Department aDepartment = null;

            return BuildBaseQuery ()
                .Where (() => aDepartment.Id == id)
                .SingleOrDefault<Department> ();
        }
    }
}