using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using NHibernate;
using NHibernate.Transform;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Models;
using PNG.U10.Domain.Repositories.Abstract;

namespace PNG.U10.Domain.Repositories {
    /// <summary>
    /// Dormitory Repository
    /// </summary>
    public class DormitoryRepository : BaseRepository, IDormitoryRepository {
        /// <summary>
        /// Constructor
        /// </summary>
        public DormitoryRepository (NHibernate.ISession session, IHttpContextAccessor httpContextAccessor) : base (session, httpContextAccessor) { }

        private IQueryOver<Dormitory, Dormitory> BuildBaseDormitoryQuery () {
            Dormitory aDormitory = null;
            // DormitoryDTO dDormitory = null;

            return Session.QueryOver<Dormitory> (() => aDormitory)
                // .SelectList (l => l
                //     //.Select (() => aDormitory.Id).WithAlias (() => dDormitory.Id)
                //     // .Select (() => aDormitory.Name).WithAlias (() => "Dormitory.Name")
                //     // .Select (() => aDormitory.ShortName) //.WithAlias (() => dDormitory.ShortName)
                //     // .Select (() => aDormitory.Number) //.WithAlias (() => dDormitory.Number)
                //     // .Select (() => aDormitory.Floors) //.WithAlias (() => dDormitory.Floors)
                //     // .Select (() => aDormitory.Active) //.WithAlias (() => dDormitory.Active)
                // )
                .TransformUsing (Transformers.RootEntity);
        }

        private IQueryOver<DormitoryRoom, DormitoryRoom> BuildBaseDormitoryRoomQuery () {
            DormitoryRoom aDormitoryRoom = null;
            // DormitoryRoomDTO dDormitoryRoom = null;

            return Session.QueryOver<DormitoryRoom> (() => aDormitoryRoom)
                .Fetch (x => x.Equipments).Eager
                // .SelectList (l => l
                //     .Select (() => aDormitoryRoom.Id).WithAlias (() => dDormitoryRoom.Id)
                //     .Select (() => aDormitoryRoom.Dormitory.Id).WithAlias (() => dDormitoryRoom.DormitoryId)
                //     .Select (() => aDormitoryRoom.Number).WithAlias (() => dDormitoryRoom.Number)
                //     .Select (() => aDormitoryRoom.Floor).WithAlias (() => dDormitoryRoom.Floor)
                //     .Select (() => aDormitoryRoom.Active).WithAlias (() => dDormitoryRoom.Active)
                // )
                .TransformUsing (Transformers.RootEntity);
        }

        /// <summary>
        /// Get one dormitory by id
        /// </summary>
        /// <param name="id">Dormitory id.</param>
        public Dormitory GetOne (int id) {
            Dormitory aDormitory = null;
            DormitoryRoom aDormitoryRoom = null;

            var item = BuildBaseDormitoryQuery ()
                .Left.JoinAlias (() => aDormitory.Rooms, () => aDormitoryRoom)
                .Where (() => aDormitory.Id == id)
                .SingleOrDefault<Dormitory> ();

            return item;
        }
        /// <summary>
        /// Get all Dormitories
        /// </summary>
        public IEnumerable<Dormitory> Get (int pageNumber = 0, int pageSize = 100) {
            var items = BuildBaseDormitoryQuery ()
                .Skip (pageSize * pageNumber).Take (pageSize)
                .List<Dormitory> ();

            return items;
        }

        /// <summary>
        /// Get all romms
        /// </summary>
        public IEnumerable<DormitoryRoom> GetRooms () => BuildBaseDormitoryRoomQuery ()
            .List<DormitoryRoom> ();

        /// <summary>
        /// Get all rooms of dormitory
        /// </summary>
        /// <param name="id">Dormitory Id</param>
        public IEnumerable<DormitoryRoom> GetRooms (int id) {
            DormitoryRoom aDormitoryRoom = null;

            return BuildBaseDormitoryRoomQuery ()
                .Where (() => aDormitoryRoom.Dormitory.Id == id)
                .List<DormitoryRoom> ();
        }

        public DormitoryRoom GetRoom (int id) {
            DormitoryRoom aDormitoryRoom = null;
            // DormitoryEquipment aDormitoryEquipment = null;
            // DormitoryRoomEquipment aDormitoryRoomEquipment = null;

            var room = BuildBaseDormitoryRoomQuery ()
                .Where (() => aDormitoryRoom.Id == id)
                .SingleOrDefault<DormitoryRoom> ();

            // room.Equipments = Session.QueryOver<DormitoryRoomEquipment> (() => aDormitoryRoomEquipment)
            //     .Left.JoinAlias (() => aDormitoryRoomEquipment.Equipment, () => aDormitoryEquipment)
            //     .Where (() => aDormitoryRoomEquipment.Room.Id == id)
            //     .TransformUsing (Transformers.RootEntity)
            //     .List<DormitoryRoomEquipment> ();

            return room;
        }
    }
}