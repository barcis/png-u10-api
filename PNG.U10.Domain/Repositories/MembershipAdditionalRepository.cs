using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using NHibernate;
using NHibernate.Transform;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Repositories.Abstract;

namespace PNG.U10.Domain.Repositories {
    /// <summary>
    /// MembershipAdditional Repository
    /// </summary>
    public class MembershipAdditionalRepository : BaseRepository, IMembershipAdditionalRepository {
        public MembershipAdditionalRepository (NHibernate.ISession session, IHttpContextAccessor httpContextAccessor) : base (session, httpContextAccessor) { }

        private IQueryOver<MembershipAdditional, MembershipAdditional> BuildBaseQuery () {
            MembershipAdditional aMembershipAdditional = null;
            return Session
                .QueryOver<MembershipAdditional> (() => aMembershipAdditional)
                .TransformUsing (Transformers.RootEntity);
        }

        public IEnumerable<MembershipAdditional> Get (int pageNumber, int pageSize) {
            return BuildBaseQuery ()
                .Skip (pageSize * pageNumber).Take (pageSize).List<MembershipAdditional> ();
        }

        public IEnumerable<MembershipAdditional> Get (int membershipId, int pageNumber, int pageSize) {
            MembershipAdditional aMembershipAdditional = null;
            return BuildBaseQuery ()
                .Where (() => aMembershipAdditional.Membership.Id == membershipId)
                .Skip (pageSize * pageNumber).Take (pageSize).List<MembershipAdditional> ();
        }

        public MembershipAdditional GetOne (int id) {
            MembershipAdditional aMembershipAdditional = null;

            return BuildBaseQuery ()
                .Where (() => aMembershipAdditional.Id == id)
                .SingleOrDefault<MembershipAdditional> ();
        }

        public MembershipAdditional GetOne (int membershipId, int typeId) {
            MembershipAdditional aMembershipAdditional = null;

            return BuildBaseQuery ()
                .Where (() => aMembershipAdditional.Membership.Id == membershipId)
                .Where (() => aMembershipAdditional.Type.Id == typeId)
                .SingleOrDefault<MembershipAdditional> ();
        }
    }
}