using Microsoft.Extensions.DependencyInjection;
using PNG.U10.Domain.Helpers;
using PNG.U10.Domain.Services;
using PNG.U10.Domain.Services.Abstract;

namespace PNG.U10.Domain.Startup {
    public static class ServicesExtension {
        public static IServiceCollection AddServices (this IServiceCollection services) {
            services.AddScoped<IUserService, UserService> ();

            services.AddScoped<IDormitoryService, DormitoryService> ();
            services.AddScoped<IDormitoryRoomService, DormitoryRoomService> ();

            services.AddScoped<IStudentService, StudentService> ();

            services.AddScoped<IPathService, PathService> ();
            services.AddScoped<IInstitutionService, InstitutionService> ();
            services.AddScoped<IDepartmentService, DepartmentService> ();
            services.AddScoped<IProgrammeService, ProgrammeService> ();
            services.AddScoped<ISemesterService, SemesterService> ();

            services.AddScoped<IDictionaryService, DictionaryService> ();
            services.AddScoped<IOtherStatusService, OtherStatusService> ();
            services.AddScoped<IPortOfTravelService, PortOfTravelService> ();
            services.AddScoped<IResidentialStatusService, ResidentialStatusService> ();
            services.AddScoped<IScholarshipStatusService, ScholarshipStatusService> ();
            services.AddScoped<IEnrollmentStatusService, EnrollmentStatusService> ();
            services.AddScoped<ICitizenshipsService, CitizenshipsService> ();
            services.AddScoped<IAdditionalService, AdditionalService> ();

            services.AddScoped<IAutocopyValues, AutocopyValues> ();

            return services;
        }
    }
}