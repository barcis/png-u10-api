using System.Linq;
using AutoMapper;
using PNG.U10.Domain.Entities;
using PNG.U10.Domain.Models;

namespace PNG.U10.Domain {
    public class DomainMapperProfile : Profile {
        /// <summary>
        /// Constructor
        /// </summary>
        public DomainMapperProfile () {
            AllowNullCollections = true;

            // CreateMap<Address, AddressDTO> ().ReverseMap ();
            CreateMap<AdditionalType, AdditionalTypeDTO> ().ReverseMap ();
            CreateMap<AdditionalDictionary, AdditionalDictionaryDTO> ().ReverseMap ();
            CreateMap<Department, DepartmentDTO> ()
                .ForMember (
                    dest => dest.Programmes,
                    opts => opts.Ignore ()).ReverseMap ();
            CreateMap<Department, DepartmentFullDTO> ().ReverseMap ();
            CreateMap<Dormitory, DormitoryDTO> ().ReverseMap ();
            // CreateMap<DormitoryEquipment, DormitoryEquipmentDTO> ().ReverseMap ();
            CreateMap<DormitoryRoom, DormitoryRoomDTO> ().ReverseMap ();
            CreateMap<DormitoryRoomEquipment, DormitoryRoomEquipmentDTO> ().ReverseMap ();
            CreateMap<EnrollmentStatusDictionary, EnrollmentStatusDictionaryDTO> ().ReverseMap ();
            CreateMap<Institution, InstitutionDTO> ()
                .ForMember (
                    dest => dest.Departments,
                    opts => opts.Ignore ())
                .ReverseMap ();
            CreateMap<Institution, InstitutionFullDTO> ().ReverseMap ();
            CreateMap<Module, ModuleDTO> ().ReverseMap ();
            CreateMap<Membership, MembershipDTO> ()
                .ForMember (d => d.GPA, opt => opt.MapFrom (s => s.GPA.Value))
                .ReverseMap ()
                .ForPath (s => s.GPA.Value, opt => opt.MapFrom (src => src.GPA));
            CreateMap<MembershipAdditional, MembershipAdditionalDTO> ().ReverseMap ();
            CreateMap<MembershipSemester, MembershipSemesterDTO> ().ReverseMap ();
            CreateMap<Kind, KindDTO> ().ReverseMap ();
            CreateMap<Specialty, SpecialtyDTO> ().ReverseMap ();
            CreateMap<Specialization, SpecializationDTO> ().ReverseMap ();
            CreateMap<Mode, ModeDTO> ().ReverseMap ();
            // CreateMap<MembershipGPA, MembershipGPADTO> ().ReverseMap ();
            CreateMap<OtherStatusDictionary, OtherStatusDictionaryDTO> ().ReverseMap ();
            CreateMap<Person, PersonDTO> ()
                .ForMember (d => d.PlaceOfBirth, opt => opt.MapFrom (s => s.PlaceOfBirth.Name))
                .ReverseMap ()
                .ForPath (s => s.PlaceOfBirth.Name, opt => opt.MapFrom (src => src.PlaceOfBirth));
            CreateMap<PersonCitizenship, PersonCitizenshipDTO> ().ReverseMap ();
            CreateMap<CitizenshipDictionary, CitizenshipDictionaryDTO> ().ReverseMap ();
            CreateMap<PlaceDictionary, PlaceDictionaryDTO> ().ReverseMap ();
            CreateMap<PortOfTravelDictionary, PortOfTravelDictionaryDTO> ().ReverseMap ();
            CreateMap<Programme, ProgrammeDTO> ().ReverseMap ();
            CreateMap<ResidentialStatusDictionary, ResidentialStatusDictionaryDTO> ().ReverseMap ();
            CreateMap<Role, RoleDTO> ().ReverseMap ();
            CreateMap<ScholarshipStatusDictionary, ScholarshipStatusDictionaryDTO> ().ReverseMap ();
            CreateMap<Semester, SemesterDTO> ().ReverseMap ();
            CreateMap<Student, StudentDTO> ()
                .ForMember (d => d.Allocator, opt => opt.MapFrom (s => s.Allocator.Value))
                .ForMember (d => d.PortOfTravel, opt => opt.MapFrom (s => s.Port.Value))
                .ForMember (d => d.ResidentialStatus, opt => opt.MapFrom (s => s.ResidentialStatus.Value))
                .ForMember (d => d.ScholarshipStatus, opt => opt.MapFrom (s => s.ScholarshipStatus.Value))
                .ForMember (d => d.OtherStatus, opt => opt.MapFrom (s => s.OtherStatus.Value))
                .ForMember (d => d.Comment, opt => opt.MapFrom (s => s.Comment.Value))
                .ReverseMap ()
                .ForPath (s => s.Allocator.Value, opt => opt.MapFrom (src => src.Allocator))
                .ForPath (s => s.Port.Value, opt => opt.MapFrom (src => src.PortOfTravel))
                .ForPath (s => s.ResidentialStatus.Value, opt => opt.MapFrom (src => src.ResidentialStatus))
                .ForPath (s => s.ScholarshipStatus.Value, opt => opt.MapFrom (src => src.ScholarshipStatus))
                .ForPath (s => s.OtherStatus.Value, opt => opt.MapFrom (src => src.OtherStatus))
                .ForPath (s => s.Comment.Value, opt => opt.MapFrom (src => src.Comment));
            CreateMap<StudentAdditional, StudentAdditionalDTO> ().ReverseMap ();
            CreateMap<StudentEnrollmentStatus, StudentEnrollmentStatusDTO> ().ReverseMap ();
            CreateMap<User, UserDTO> ().ReverseMap ();
        }
    }
}