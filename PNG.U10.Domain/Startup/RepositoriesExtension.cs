using Microsoft.Extensions.DependencyInjection;
using PNG.U10.Domain.Repositories;
using PNG.U10.Domain.Repositories.Abstract;

namespace PNG.U10.Domain.Startup {
    public static class RepositoriesExtension {

        public static IServiceCollection AddRepositories (this IServiceCollection services) {
            services.AddScoped<IUserRepository, UserRepository> ();
            services.AddScoped<IDormitoryRepository, DormitoryRepository> ();
            services.AddScoped<IStudentRepository, StudentRepository> ();
            services.AddScoped<IInstitutionRepository, InstitutionRepository> ();
            services.AddScoped<IDepartmentRepository, DepartmentRepository> ();
            services.AddScoped<IProgrammeRepository, ProgrammeRepository> ();
            services.AddScoped<ISemesterRepository, SemesterRepository> ();
            services.AddScoped<IEnrollmentStatusDictionaryRepository, EnrollmentStatusDictionaryRepository> ();

            services.AddScoped<IOtherStatusDictionaryRepository, OtherStatusDictionaryRepository> ();
            services.AddScoped<IPortOfTravelDictionaryRepository, PortOfTravelDictionaryRepository> ();
            services.AddScoped<IResidentialStatusDictionaryRepository, ResidentialStatusDictionaryRepository> ();
            services.AddScoped<IScholarshipStatusDictionaryRepository, ScholarshipStatusDictionaryRepository> ();
            services.AddScoped<ICitizenshipDictionaryRepository, CitizenshipDictionaryRepository> ();
            services.AddScoped<IStudentAdditionalRepository, StudentAdditionalRepository> ();
            services.AddScoped<IMembershipAdditionalRepository, MembershipAdditionalRepository> ();

            return services;

        }

    }
}