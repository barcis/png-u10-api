using System;
using System.Collections;
using System.Linq;
using System.Reflection;
using System.Security.Principal;
using FluentNHibernate.Cfg;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using NHibernate;
using NHibernate.Type;
using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Startup {
    /// <summary>
    /// NHibernate startup extenson class
    /// </summary>
    public static class NHibernateExtension {
        /// <summary>
        /// NHibernate startup configuration
        /// </summary>
        public static IServiceCollection AddNHibernate (this IServiceCollection services, string connectionString) {
            services.AddScoped<IAuditInterceptor, AuditInterceptor> ();
            services.AddSingleton<ISessionFactory> (factory => Fluently
                .Configure ()
                .Database (() => {
                    return FluentNHibernate.Cfg.Db.MsSqlConfiguration
                        .MsSql2012
                        .ShowSql ()
                        .ConnectionString (connectionString);
                })
                .Cache (
                    c => c.UseQueryCache ()
                    .UseSecondLevelCache ()
                    .ProviderClass<NHibernate.Cache.HashtableCacheProvider> ())
                .Mappings (m => m.FluentMappings.AddFromAssembly (Assembly.GetExecutingAssembly ()))
                .BuildSessionFactory ());

            services.AddScoped<NHibernate.ISession> (factory => {
                IAuditInterceptor auditInterceptor = factory.GetServices<IAuditInterceptor> ().First ();
                ISessionFactory sessionFactory = factory.GetServices<ISessionFactory> ().First ();

                return sessionFactory
                    .WithOptions ().Interceptor (auditInterceptor)
                    .OpenSession ();
            });

            return services;
        }
    }

    public interface IAuditInterceptor : IInterceptor { }
    public class AuditInterceptor : EmptyInterceptor, IAuditInterceptor {
        private IHttpContextAccessor HttpContextAccessor { get; }

        private IIdentity Identity {
            get {
                return HttpContextAccessor.HttpContext.User.Identity;
            }
        }

        public AuditInterceptor (IHttpContextAccessor httpContextAccessor) => HttpContextAccessor = httpContextAccessor;

        public override bool OnFlushDirty (object entity,
            object id,
            object[] currentState,
            object[] previousState,
            string[] propertyNames,
            IType[] types) {

            if (entity is IBaseEntity) {
                IBaseEntity baseEntity = entity as IBaseEntity;
                baseEntity.UpdateUser = Identity.Name;
                baseEntity.UpdateTime = DateTime.Now;
            }
            return false;
        }

        public override bool OnSave (object entity,
            object id,
            object[] state,
            string[] propertyNames,
            IType[] types) {

            if (entity is IBaseEntity) {
                IBaseEntity baseEntity = entity as IBaseEntity;
                baseEntity.CreateFirm = 0;
                baseEntity.CreateUser = Identity.Name;
                baseEntity.CreateTime = DateTime.Now;
            }
            return false;
        }
    }
}