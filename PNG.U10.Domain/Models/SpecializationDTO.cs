using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Models {
    public class SpecializationDTO : BaseDTO {
        public virtual string Name { get; set; }
        public virtual string Short { get; set; }
        public virtual string Code { get; set; }
        public virtual SpecialtyDTO Specialty { get; set; }
    }
}