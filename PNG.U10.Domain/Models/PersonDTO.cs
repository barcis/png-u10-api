using System;
using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Models {
    public class PersonDTO : BaseDTO {
        /// <summary>
        /// Person surname
        /// </summary>
        public virtual string Surname { get; set; }

        /// <summary>
        /// Person name
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Person gender
        /// </summary>
        public virtual int Gender { get; set; }

        /// <summary>
        /// Person subject id (IdP id)
        /// </summary>
        public virtual string SubId { get; set; }

        /// <summary>
        /// Person email
        /// </summary>
        public virtual string Email { get; set; }

        /// <summary>
        /// Person email2
        /// </summary>
        public virtual string Email2 { get; set; }

        /// <summary>
        /// Person email to work
        /// </summary>
        public virtual string EmailWork { get; set; }

        /// <summary>
        /// Person cell phone number
        /// </summary>
        public virtual string PhoneNumber { get; set; }

        /// <summary>
        /// Person cell phone number
        /// </summary>
        public virtual string PhoneNumberDormitory { get; set; }

        /// <summary>
        /// Person cell phone number
        /// </summary>
        public virtual string PhoneNumberHome { get; set; }

        /// <summary>
        /// Person cell phone number
        /// </summary>
        public virtual string PhoneNumberHome2 { get; set; }

        /// <summary>
        /// Person cell phone number
        /// </summary>
        public virtual string PhoneNumberWork { get; set; }

        /// <summary>
        /// Person cell phone number
        /// </summary>
        public virtual string PhoneNumberWork2 { get; set; }

        /// <summary>
        /// Person cell phone number
        /// </summary>
        public virtual string PhoneNumberOther { get; set; }

        /// <summary>
        /// Person Date Of Birth
        /// </summary>
        public virtual DateTime? DateOfBirth { get; set; }

        /// <summary>
        /// Person Place Of Birth
        /// </summary>
        public virtual string PlaceOfBirth { get; set; }

        /// <summary>
        /// Person citizenship
        /// </summary>
        public virtual IList<CitizenshipDictionaryDTO> Citizenships { get; set; }

        /// <summary>
        /// Person nationality
        /// </summary>
        public virtual string Nationality { get; set; }

        /// <summary>
        /// Person nrigin
        /// </summary>
        public virtual string Origin { get; set; }

        /// <summary>
        /// Person company
        /// </summary>
        public virtual string Company { get; set; }

        /// <summary>
        /// Student Marital Status
        /// </summary>
        public int MaritalStatus { get; set; }

        /// <summary>
        /// User role
        /// </summary>
        public virtual IList<RoleDTO> Roles { get; set; }
    }
}