using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Models {
    public class RoleDTO : BaseDTO {
        /// <summary>
        /// Role name
        /// </summary>
        public virtual string Name { get; set; }
    }
}