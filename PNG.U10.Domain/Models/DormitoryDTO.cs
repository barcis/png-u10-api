using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Models {

    public class DormitoryDTO : BaseDTO {
        /// <summary>
        /// Dormitory Name
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Dormitory ShortName
        /// </summary>
        public virtual string ShortName { get; set; }

        /// <summary>
        /// Dormitory Number
        /// </summary>
        public virtual int Number { get; set; }

        /// <summary>
        /// Dormitory Floors
        /// </summary>
        public virtual int Floors { get; set; }

        /// <summary>
        /// Dormitory Rooms
        /// </summary>
        public virtual IEnumerable<DormitoryRoomDTO> Rooms { get; set; }

        /// <summary>
        /// Dormitory is Active
        /// </summary>
        public virtual bool Active { get; set; } = true;
    }
}