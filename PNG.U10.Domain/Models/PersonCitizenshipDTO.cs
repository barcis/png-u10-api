using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Models {
    public class PersonCitizenshipDTO : BaseDTO {
        public virtual CitizenshipDictionaryDTO Citizenship { get; set; }
    }
}