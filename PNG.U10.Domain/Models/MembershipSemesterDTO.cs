using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Models {

    public class MembershipSemesterDTO : BaseDTO {
        public virtual int Year { get; set; }
        public virtual double ManualAverage { get; set; }

        public virtual SemesterDTO Semester { get; set; }

        public virtual IList<StudentEnrollmentStatusDTO> Statuses { get; set; }
    }
}