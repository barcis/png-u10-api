using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Models {
    public class DormitoryRoomDTO : BaseDTO {
        /// <summary>
        /// Dormitory Id
        /// </summary>
        public virtual int DormitoryId { get; set; }

        /// <summary>
        /// Dormitory Room Number
        /// </summary>
        public virtual string Number { get; set; }

        /// <summary>
        /// Dormitory Room Gender
        /// </summary>
        public virtual int Gender { get; set; }

        /// <summary>
        /// Dormitory Room Floor
        /// </summary>
        public virtual int Floor { get; set; }

        /// <summary>
        /// Dormitory Room Floor
        /// </summary>
        public virtual int Places { get; set; }

        /// <summary>
        /// Dormitory Room Occupied Places
        /// </summary>
        public virtual int OccupiedPlaces { get; set; }

        /// <summary>
        /// Dormitory Room Free Places
        /// </summary>
        public virtual int FreePlaces { get; set; }

        /// <summary>
        /// Dormitory Room Reserved Places
        /// </summary>
        public virtual int ReservedPlaces { get; set; }

        /// <summary>
        /// Dormitory Room Phone
        /// </summary>
        public virtual string Phone { get; set; }

        /// <summary>
        /// Dormitory Room Network Socket Number
        /// </summary>
        public virtual string NetworkSocket { get; set; }

        /// <summary>
        /// Dormitory Room Type Id
        /// </summary>
        public virtual int RoomTypeId { get; set; }

        /// <summary>
        /// Dormitory Room Type Name
        /// </summary>
        public virtual string RoomTypeName { get; set; }

        /// <summary>
        /// Dormitory Room is Active
        /// </summary>
        public virtual bool Active { get; set; } = true;

        /// <summary>
        /// Dormitory room equipment list
        /// </summary>
        public virtual IEnumerable<DormitoryRoomEquipmentDTO> Equipments { get; set; }
    }
}