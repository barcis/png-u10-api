using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Models {
    public class AdditionalTypeDTO : BaseDTO {
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual int Type { get; set; }
        public virtual int Edit { get; set; }
        public virtual IList<AdditionalDictionaryDTO> Options { get; set; }

    }
}