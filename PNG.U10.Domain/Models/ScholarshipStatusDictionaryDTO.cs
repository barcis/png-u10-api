using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Models {
    public class ScholarshipStatusDictionaryDTO : BaseDTO {
        /// <summary>
        /// Student Scholarship Status Dictionary Name
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Student Scholarship Status Dictionary is Active
        /// </summary>
        public virtual bool Active { get; set; } = true;
    }
}