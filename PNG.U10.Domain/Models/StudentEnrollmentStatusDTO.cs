using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Models {
    public class StudentEnrollmentStatusDTO : BaseDTO {
        /// <summary>
        /// Status related to StudentEnrollmentStatus
        /// </summary>
        public virtual EnrollmentStatusDictionaryDTO Value { get; set; }

        /// <summary>
        /// StudentEnrollmentStatus is Active
        /// </summary>
        public virtual bool Active { get; set; } = true;
    }
}