using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Models {
    public class StudentAdditionalDTO : BaseDTO {
        public virtual object Value { get; set; }
        public virtual int StudentId { get; set; }
        public virtual AdditionalTypeDTO Type { get; set; }

    }
}