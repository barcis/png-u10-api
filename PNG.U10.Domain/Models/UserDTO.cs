using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Models {
    public class UserDTO : BaseDTO {

        /// <summary>
        /// User login
        /// </summary>
        public virtual string Login { get; set; }

        /// <summary>
        /// Person related to user
        /// </summary>
        public virtual PersonDTO Person { get; set; }
    }
}