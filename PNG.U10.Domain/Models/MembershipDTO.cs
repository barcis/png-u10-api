using System;
using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Models {
    public class MembershipDTO : BaseDTO {
        /// <summary>
        /// Membership Album Number
        /// </summary>
        public virtual string AlbumNumber { get; set; }

        /// <summary>
        /// Membership Start Date
        /// </summary>
        public virtual DateTime? StartDate { get; set; }

        /// <summary>
        /// Membership Start Date on University
        /// </summary>
        public virtual DateTime? UniversityStartDate { get; set; }

        /// <summary>
        /// Membership is GPA
        /// </summary>
        public virtual double GPA { get; set; }

        /// <summary>
        /// Institution related to Membership
        /// </summary>
        public virtual InstitutionDTO Institution { get; set; }

        /// <summary>
        /// Department related to Membership
        /// </summary>
        public virtual DepartmentDTO Department { get; set; }

        /// <summary>
        /// Programme related to Membership
        /// </summary>
        public virtual ProgrammeDTO Programme { get; set; }

        /// <summary>
        /// Kind related to Membership
        /// </summary>
        public virtual KindDTO Kind { get; set; }

        /// <summary>
        /// List of Modules related to Membership
        /// </summary>
        public virtual IList<ModuleDTO> Modules { get; set; }
        /// <summary>
        /// List of Semesters related to Membership
        /// </summary>
        public virtual IList<MembershipSemesterDTO> MembershipSemesters { get; set; }
    }
}