using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Models {
    public class ModuleDTO : BaseDTO {
        public virtual MembershipDTO Membership { get; set; }
        public virtual MembershipSemesterDTO MembershipSemester { get; set; }
        public virtual SpecializationDTO Specialization { get; set; }
        public virtual SpecialtyDTO Specialty { get; set; }
        public virtual SemesterDTO Semester { get; set; }
    }
}