using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Models {
    public class DepartmentDTO : BaseDTO {
        public virtual string Name { get; set; }
        public virtual int GradingScaleId { get; set; }

        public virtual IList<ProgrammeDTO> Programmes { get; set; }
    }

    public class DepartmentFullDTO : DepartmentDTO { }
}