using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Models {
    public class PlaceDictionaryDTO : BaseDTO {
        public virtual string Name { get; set; }
        public virtual bool Active { get; set; } = true;
    }
}