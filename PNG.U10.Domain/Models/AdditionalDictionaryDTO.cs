using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Models {
    public class AdditionalDictionaryDTO : BaseDTO {
        public virtual int OrdinalNumber { get; set; }
        public virtual string Value { get; set; }
        public virtual string Short { get; set; }
    }
}