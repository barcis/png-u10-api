using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Models {
    public class CitizenshipDictionaryDTO : BaseDTO {
        public virtual int OrdinalNumber { get; set; }
        public virtual string Value { get; set; }
        public virtual string Short { get; set; }
        public virtual int TypeId { get; set; }
        public virtual bool Active { get; set; } = true;
    }
}