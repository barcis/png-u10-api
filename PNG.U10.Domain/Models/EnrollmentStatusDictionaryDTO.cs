using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Models {
    public class EnrollmentStatusDictionaryDTO : BaseDTO {
        public virtual string Name { get; set; }
        public virtual bool Active { get; set; } = true;
    }
}