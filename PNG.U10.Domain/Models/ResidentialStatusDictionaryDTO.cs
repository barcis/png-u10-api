using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Models {
    public class ResidentialStatusDictionaryDTO : BaseDTO {
        /// <summary>
        /// Student Residential Status Dictionary Name
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Student Residential Status Dictionary is Active
        /// </summary>
        public virtual bool Active { get; set; } = true;
    }
}