using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Models {
    public class SemesterDTO : BaseDTO {
        public virtual string Name { get; set; }

        public virtual int Number { get; set; }

    }
}