using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Models {
    public class DormitoryRoomEquipmentDTO : BaseDTO {
        /// <summary>
        /// Dormitory Room Id
        /// </summary>
        public virtual int DormitoryRoomId { get; set; }

        /// <summary>
        /// Dormitory Room Equipment Name
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Dormitory Room Equipment Description
        /// </summary>
        public virtual string Description { get; set; }

        /// <summary>
        /// Dormitory room equipment Type Name
        /// </summary>
        public virtual string TypeName { get; set; }

        /// <summary>
        /// Dormitory room equipment number of items
        /// </summary>
        public virtual int NumberOfItems { get; set; }
    }
}