using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Models {
    public class PortOfTravelDictionaryDTO : BaseDTO {
        /// <summary>
        /// Student Port Of Travel Dictionary Name
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Student Port Of Travel Dictionary is Active
        /// </summary>
        public virtual bool Active { get; set; } = true;
    }
}