using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Models {
    public class SpecialtyDTO : BaseDTO {
        public virtual string Name { get; set; }
        public virtual string Short { get; set; }
        public virtual string Code { get; set; }
        public virtual KindDTO Kind { get; set; }
    }
}