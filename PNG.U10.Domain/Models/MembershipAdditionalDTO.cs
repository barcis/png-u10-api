using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Models {
    public class MembershipAdditionalDTO : BaseDTO {
        public virtual object Value { get; set; }
        public virtual int MembershipId { get; set; }
        public virtual AdditionalTypeDTO Type { get; set; }
    }
}