using System.Collections.Generic;
using PNG.U10.Domain.Abstracts;
using PNG.U10.Domain.Entities;

namespace PNG.U10.Domain.Models {
    public class InstitutionDTO : BaseDTO {
        public virtual string Name { get; set; }

        /// <summary>
        /// Institution id in Selection system
        /// </summary>
        public virtual int InstitutionId { get; set; }

        public virtual List<DepartmentDTO> Departments { get; set; }
    }

    public class InstitutionFullDTO : InstitutionDTO {

    }
}