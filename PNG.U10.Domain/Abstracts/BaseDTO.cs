using System;
using System.Collections.Generic;

namespace PNG.U10.Domain.Abstracts {
    public class BaseDTO {
        /// <summary>
        /// entity unique id
        /// </summary>
        public virtual int Id { get; set; }
        // /// <summary>
        // /// entity create firm
        // /// </summary>
        // public virtual int CreateFirm { get; set; }

        // /// <summary>
        // /// entity create user
        // /// </summary>
        // public virtual string CreateUser { get; set; }

        // /// <summary>
        // /// entity update user
        // /// </summary>
        // public virtual string UpdateUser { get; set; }

        // /// <summary>
        // /// entity create time
        // /// </summary>
        // public virtual DateTime? CreateTime { get; set; }

        // /// <summary>
        // /// entity update time
        // /// </summary>
        // public virtual DateTime? UpdateTime { get; set; }
    }

}