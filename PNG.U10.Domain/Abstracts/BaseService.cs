using System.Collections.Generic;
using AutoMapper;
using PNG.U10.Domain.Abstracts;

namespace PNG.U10.Domain.Abstract {
    public abstract class BaseService<TDto, TEntity> : IBaseService<TDto, TEntity> where TDto : BaseDTO where TEntity : BaseEntity {

        protected IMapper Mapper;

        protected BaseService (IMapper mapper) => Mapper = mapper;

        public TDto Map (TEntity item) => Mapper.Map<TDto> (item);
        public TEntity Map (TDto item) => Mapper.Map<TEntity> (item);

        public IEnumerable<TDto> Map (IEnumerable<TEntity> items) => Mapper.Map<IEnumerable<TDto>> (items);
        public IEnumerable<TEntity> Map (IEnumerable<TDto> items) => Mapper.Map<IEnumerable<TEntity>> (items);
    }

    public abstract class BaseService<TDto, TDtoFull, TEntity> : BaseService<TDto, TEntity> where TDto : BaseDTO where TDtoFull : BaseDTO where TEntity : BaseEntity {
        protected BaseService (IMapper mapper) : base (mapper) { }

        public TDtoFull MapFull (TEntity item) => Mapper.Map<TDtoFull> (item);
    }
}