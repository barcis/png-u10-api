﻿namespace PNG.U10.Domain.Abstracts {
    public interface IUpdateRepository<TEntity> where TEntity : BaseEntity {
        /// <summary>
        /// Get one item by id
        /// </summary>
        /// <param name="id">Item id.</param>
        /// <param name="item">Item content</param>
        TEntity UpdateOne (int id, TEntity item);
    }
}