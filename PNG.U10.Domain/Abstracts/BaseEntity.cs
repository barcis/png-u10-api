using System;
using PNG.U10.Domain.Helpers;

namespace PNG.U10.Domain.Abstracts {
    public interface IBaseEntity {
        int? Id { get; set; }
        int CreateFirm { get; set; }
        string CreateUser { get; set; }
        string UpdateUser { get; set; }
        DateTime? CreateTime { get; set; }
        DateTime? UpdateTime { get; set; }
    }

    public abstract class BaseEntity : IBaseEntity {
        /// <summary>
        /// entity unique id
        /// </summary>
        [AutocopyDisabled]
        public virtual int? Id { get; set; }
        /// <summary>
        /// entity create firm
        /// </summary>
        [AutocopyDisabled]
        public virtual int CreateFirm { get; set; }

        /// <summary>
        /// entity create user
        /// </summary>
        [AutocopyDisabled]
        public virtual string CreateUser { get; set; }

        /// <summary>
        /// entity update user
        /// </summary>
        [AutocopyDisabled]
        public virtual string UpdateUser { get; set; }

        /// <summary>
        /// entity create time
        /// </summary>
        [AutocopyDisabled]
        public virtual DateTime? CreateTime { get; set; }

        /// <summary>
        /// entity update time
        /// </summary>
        [AutocopyDisabled]
        public virtual DateTime? UpdateTime { get; set; }
    }
}