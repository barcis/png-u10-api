using System.Collections.Generic;

namespace PNG.U10.Domain.Abstracts {
    public interface IBaseService<TDto, TEntity> where TDto : BaseDTO where TEntity : BaseEntity {
        TDto Map (TEntity item);
        TEntity Map (TDto item);

        IEnumerable<TDto> Map (IEnumerable<TEntity> item);
        IEnumerable<TEntity> Map (IEnumerable<TDto> item);
    }
}