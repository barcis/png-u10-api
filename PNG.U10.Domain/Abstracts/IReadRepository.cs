using System.Collections.Generic;

namespace PNG.U10.Domain.Abstracts {
    public interface IReadRepository<TEntity> where TEntity : BaseEntity {
        /// <summary>
        /// Get one item by id
        /// </summary>
        /// <param name="id">Item id.</param>
        TEntity GetOne (int id);
        /// <summary>
        /// Get all items
        /// </summary>
        IEnumerable<TEntity> Get (int pageNumber = 0, int pageSize = 100);

    }
}