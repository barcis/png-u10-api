using System;
using System.Linq.Expressions;
using System.Reflection;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Impl;
using NHibernate.Type;
using PNG.U10.Domain.Entities;

namespace PNG.U10.Domain.Abstracts {
    /// <summary>
    /// Base Repository
    /// </summary>
    public class BaseRepository {
        /// <summary>
        /// session
        /// </summary>
        protected NHibernate.ISession Session { get; }

        /// <summary>
        /// HttpContextAccessor 
        /// </summary>
        protected IHttpContextAccessor HttpContextAccessor { get; }
        /// <summary>
        /// HttpContext
        /// </summary>
        protected HttpContext HttpContext { get; }
        /// <summary>
        /// User
        /// </summary>
        protected ClaimsPrincipal User { get; }

        /// <summary>
        /// Constrictor
        /// </summary>
        public BaseRepository (NHibernate.ISession session, IHttpContextAccessor httpContextAccessor) {
            Session = session;
            HttpContextAccessor = httpContextAccessor;
            HttpContext = httpContextAccessor.HttpContext;
            User = httpContextAccessor.HttpContext.User;
        }

        protected bool IsSimple (Type type) {
            if (type.IsGenericType && type.GetGenericTypeDefinition () == typeof (Nullable<>)) {
                // nullable type, check if the nested type is simple.
                return IsSimple (type.GetGenericArguments () [0]);
            }
            return type.IsPrimitive ||
                type.IsEnum ||
                type.Equals (typeof (string)) ||
                type.Equals (typeof (decimal)) ||
                type.Equals (typeof (DateTime));
        }

        protected void addProjections<TEntity> (ProjectionList list, string prefix, Expression<Func<object>> aliasExpression) where TEntity : BaseEntity {
            Type entityType = typeof (TEntity);
            var metaData = Session.SessionFactory.GetClassMetadata (entityType);
            var key = metaData.IdentifierPropertyName;
            if (IsSimple (entityType.GetProperty (key).PropertyType)) {
                string alias = ExpressionProcessor.FindMemberExpression (aliasExpression.Body);
                list.Add (Projections.Property (string.Format ("{0}.{1}", alias, key)).As (prefix + key));
            }

            foreach (var name in metaData.PropertyNames) {
                if (IsSimple (entityType.GetProperty (name).PropertyType)) {
                    string alias = ExpressionProcessor.FindMemberExpression (aliasExpression.Body);
                    list.Add (Projections.Property (string.Format ("{0}.{1}", alias, name)).As (prefix + name));
                }
            }
        }
    }
}