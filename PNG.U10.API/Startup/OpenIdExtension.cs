using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Protocols;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Tokens;
using PNG.U10.Domain.Services.Abstract;

namespace PNG.U10.API.Startup {
    /// <summary>
    /// OpenId (IdentityProvider) startup extenson class
    /// </summary>
    public static class OpenIdExtension {
        /// <summary>
        /// OpenId (IdentityProvider) startup configuration
        /// </summary>
        public static IServiceCollection AddOpenId (this IServiceCollection services, string Authority, string Audience) {
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear ();

            var stsDiscoveryEndpoint = $"{Authority}/.well-known/openid-configuration";

            var configurationManager =
                new ConfigurationManager<OpenIdConnectConfiguration> (stsDiscoveryEndpoint, new OpenIdConnectConfigurationRetriever ());

            OpenIdConnectConfiguration _configuration = configurationManager.GetConfigurationAsync (CancellationToken.None).Result;

            services
                .AddAuthentication (x => {
                    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer (options => {
                    options.RequireHttpsMetadata = false;
                    options.SaveToken = true;
                    options.Authority = Authority;
                    options.Audience = Audience;
                    options.TokenValidationParameters = new TokenValidationParameters {
                        ValidateIssuerSigningKey = true,
                        ValidateIssuer = false,
                        ValidateAudience = true,
                        NameClaimType = ClaimTypes.Name,
                        RoleClaimType = ClaimTypes.Role,
                        IssuerSigningKeys = GetSecurityKeys (_configuration.JsonWebKeySet.Keys),
                    };
                    options.Events = new JwtBearerEvents {
                        OnTokenValidated = context => {

                            // var claimsIdentity = context.Principal.Identity as System.Security.Claims.ClaimsIdentity;

                            // var sub = claimsIdentity.Claims.FirstOrDefault (x => x.Type == JwtRegisteredClaimNames.Sub);
                            // if (sub != null) {
                            //     var users = context.HttpContext.RequestServices.GetRequiredService<IUserService> ();
                            //     var meDTO = users.GetOneBySubId (sub.Value);

                            //     if (meDTO != null) {
                            //         var claims = new List<Claim> {
                            //         new Claim (ClaimTypes.Name, meDTO.Login)
                            //         };

                            //         if (meDTO.Person != null && meDTO.Person.Roles != null && meDTO.Person.Roles.Any ()) {

                            //             foreach (var role in meDTO.Person.Roles) {
                            //                 claims.Add (new Claim (ClaimTypes.Role, role.Name));
                            //             }
                            //         }
                            //         claimsIdentity.AddClaims (claims);
                            //     }
                            // }

                            return Task.CompletedTask;
                        }
                    };
                });

            return services;
        }

        /// <summary>
        /// Get keys using to signing tokens
        /// </summary>
        private static List<SecurityKey> GetSecurityKeys (IList<JsonWebKey> jsonkeys) {
            var keys = new List<SecurityKey> ();
            foreach (var key in jsonkeys) {
                if (key.Kty == "RSA") {
                    if (key.X5c != null && key.X5c.Count > 0) {
                        string certificateString = key.X5c[0];
                        var certificate = new X509Certificate2 (Convert.FromBase64String (certificateString));

                        var x509SecurityKey = new X509SecurityKey (certificate);

                        keys.Add (x509SecurityKey);
                    }
                } else {
                    throw new NotImplementedException ("Only RSA key type is implemented for token validation");
                }
            }

            return keys;
        }
    }
}