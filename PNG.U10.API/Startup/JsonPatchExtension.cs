using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace PNG.U10.API.Startup {
    /// <summary>
    /// JSON Patch Extenson for partial model updates
    /// </summary>
    public static class JsonPatchExtension {
        /// <summary>
        /// JSON Patch Extenson startup configuration
        /// </summary>
        public static IServiceCollection AddJsonPatch (this IServiceCollection services) {
            services
                .AddControllersWithViews ()
                .AddNewtonsoftJson ();

            /*services.AddControllersWithViews (options => {
                options.InputFormatters.Insert (0, GetJsonPatchInputFormatter ());
            });*/
            return services;
        }

        private static NewtonsoftJsonPatchInputFormatter GetJsonPatchInputFormatter () {
            var builder = new ServiceCollection ()
                .AddLogging ()
                .AddMvc ()
                .AddNewtonsoftJson ()
                .Services.BuildServiceProvider ();

            return builder
                .GetRequiredService<IOptions<MvcOptions>> ()
                .Value
                .InputFormatters
                .OfType<NewtonsoftJsonPatchInputFormatter> ()
                .First ();
        }
    }
}