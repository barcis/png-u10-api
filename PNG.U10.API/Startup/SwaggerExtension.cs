using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;

namespace PNG.U10.API.Startup {
    /// <summary>
    /// Swagger startup extenson class
    /// </summary>
    public static class SwaggerExtension {
        /// <summary>
        /// NHibernate startup configuration
        /// </summary>
        public static IServiceCollection AddSwagger (this IServiceCollection services, OpenApiInfo description) {
            services.AddSwaggerGen (sw => {
                sw.SwaggerDoc ("v1", description);

                // Swagger token from UI
                sw.AddSecurityDefinition (JwtBearerDefaults.AuthenticationScheme, new OpenApiSecurityScheme {
                    Description = "JWT Authorization header using the Bearer scheme.",
                        Name = "Authorization",
                        In = ParameterLocation.Header,
                        Type = SecuritySchemeType.ApiKey,
                        Scheme = "Bearer"
                });

                sw.AddSecurityRequirement (new OpenApiSecurityRequirement () {
                    {
                        new OpenApiSecurityScheme {
                            Reference = new OpenApiReference {
                                    Type = ReferenceType.SecurityScheme,
                                        Id = "Bearer"
                                },
                                Scheme = "oauth2",
                                Name = "Bearer",
                                In = ParameterLocation.Header,

                        },
                        new List<string> ()
                    }
                });

                //For generate Swagger from comments
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine (AppContext.BaseDirectory, xmlFile);
                sw.IncludeXmlComments (xmlPath);
            });
            return services;
        }
    }
}