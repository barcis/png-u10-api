﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Serialization;
using PNG.U10.API.Mappings;
using PNG.U10.API.Startup;
using PNG.U10.Domain;
using PNG.U10.Domain.Startup;
using Swashbuckle.AspNetCore.SwaggerUI;

namespace PNGU10API.Core {
    /// <summary>
    /// Startup
    /// </summary>
    public class Startup {
        /// <summary>
        /// Startup constructor
        /// </summary>
        public Startup (IConfiguration configuration) {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        /// <summary>
        /// Startup configuration
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        public void ConfigureServices (IServiceCollection services) {

            services.AddHttpContextAccessor ();
            // Config OpenId connect
            services.AddOpenId (Configuration["OpenId_Authority"], Configuration["OpenId_Audience"]);

            // Swagger
            OpenApiInfo description = Configuration.GetSection ("APIdescription").Get<OpenApiInfo> ();
            services.AddSwagger (description);

            // NHibernate
            string connStr = Configuration.GetConnectionString ("DefaultConnection");
            services.AddNHibernate (connStr);

            services.AddRepositories ();
            services.AddServices ();

            List<Type> profiles = new List<Type> ();

            profiles.Add (typeof (ApiMapperProfile));
            profiles.Add (typeof (DomainMapperProfile));

            services.AddAutoMapper (profiles.ToArray ());

            services.AddCors (options => {
                options.AddDefaultPolicy (
                    builder => {
                        builder.WithOrigins ("http://localhost:4200", "http://localhost:4200/")
                            .AllowAnyHeader ()
                            .AllowAnyMethod ()
                            .AllowAnyOrigin ();
                    });

            });

            services.AddJsonPatch ();

            services.AddControllers ();
        }

        /// <summary>
        /// Startup configure
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        public void Configure (IApplicationBuilder app, IWebHostEnvironment env) {
            if (env.IsDevelopment ()) {
                app.UseDeveloperExceptionPage ();
            }

            app.UseHttpsRedirection ();

            app.UseSwagger ();
            app.UseSwaggerUI (ui => {
                //                ui.RoutePrefix
                ui.DocExpansion (DocExpansion.List);
                ui.SwaggerEndpoint ("v1/swagger.json", "1.0");
            });

            app.UseRouting ();

            app.UseCors ();

            app.UseAuthentication ();
            app.UseAuthorization ();

            app.UseEndpoints (endpoints => {
                endpoints.MapControllers ();
            });
        }
    }
}