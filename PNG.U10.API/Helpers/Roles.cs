using System;

namespace PNG.U10.API.Helpers {
    /// <summary>
    /// User roles
    /// </summary>
    public class Roles {
        /// <summary>
        /// SuperAdmin
        /// </summary>
        public const string SuperAdmin = "SuperAdmin";
        /// <summary>
        /// AdminHEI
        /// </summary>
        public const string AdminHEI = "AdminHEI";
        /// <summary>
        /// SiteAdministrator
        /// </summary>
        public const string SiteAdministrator = "SiteAdministrator";
        /// <summary>
        /// Selector
        /// </summary>
        public const string Selector = "Selector";
        /// <summary>
        /// Moderator
        /// </summary>
        public const string Moderator = "Moderator";
        /// <summary>
        /// Principal
        /// </summary>
        public const string Principal = "Principal";
        /// <summary>
        /// SchoolPatron
        /// </summary>
        public const string SchoolPatron = "SchoolPatron";
        /// <summary>
        /// Candidate
        /// </summary>
        public const string Candidate = "Candidate";
        /// <summary>
        /// Student
        /// </summary>
        public const string Student = "Student";
        /// <summary>
        /// StudentDataOfficer
        /// </summary>
        public const string StudentDataOfficer = "StudentDataOfficer";
        /// <summary>
        /// Allocator
        /// </summary>
        public const string Allocator = "Allocator";

    }
}