namespace PNG.U10.API.Models {
    /// <summary>
    /// PlaceDictionary data
    /// </summary>
    public class PlaceDictionary {
        /// <summary>
        /// Place Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Place name
        /// </summary>
        public virtual string Name { get; set; }
    }
}