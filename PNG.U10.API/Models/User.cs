using System.Collections.Generic;

namespace PNG.U10.API.Models {
    /// <summary>
    /// User data
    /// </summary>
    public class User {
        /// <summary>
        /// User Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Surname of Person related to User
        /// </summary>
        public virtual string Surname { get; set; }

        /// <summary>
        /// Name of Person related to User
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Email of Person related to User
        /// </summary>
        public virtual string Email { get; set; }

        /// <summary>
        /// User login
        /// </summary>
        public virtual string Login { get; set; }

        /// <summary>
        /// Roles of Person related to User
        /// </summary>
        public virtual IEnumerable<Role> Roles { get; set; }

        /// <summary>
        /// SubId of Person related to User
        /// </summary>
        public virtual string SubId { get; set; }

        /// <summary>
        /// Id of Person related to User
        /// </summary>
        public virtual int PersonId { get; set; }
    }
}