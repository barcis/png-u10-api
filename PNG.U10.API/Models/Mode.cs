namespace PNG.U10.API.Models {
    /// <summary>
    /// Mode data
    /// </summary>
    public class Mode {
        /// <summary>
        /// Mode Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Mode name
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Mode short
        /// </summary>
        public virtual string Short { get; set; }

        /// <summary>
        /// Mode is Part-time or full-time studies
        /// </summary>
        public virtual bool Fulltime { get; set; }
    }

}