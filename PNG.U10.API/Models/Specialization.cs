namespace PNG.U10.API.Models {
    /// <summary>
    /// Specialization data
    /// </summary>
    public class Specialization {
        /// <summary>
        /// Specialization Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Specialization name
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Specialization short
        /// </summary>
        public virtual string Short { get; set; }

        /// <summary>
        /// Specialization code
        /// </summary>
        public virtual string Code { get; set; }
    }
}