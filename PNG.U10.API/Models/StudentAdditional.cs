namespace PNG.U10.API.Models {
    /// <summary>
    /// StudentAdditional data
    /// </summary>
    public class StudentAdditional {
        /// <summary>
        /// Additional id
        /// </summary>
        public virtual int Id { get; set; }

        /// <summary>
        /// Additional value
        /// </summary>
        public virtual object Value { get; set; }

        /// <summary>
        /// Student Id
        /// </summary>
        public virtual int StudentId { get; set; }

        /// <summary>
        /// Type Id
        /// </summary>
        public virtual AdditionalType Type { get; set; }
    }
}