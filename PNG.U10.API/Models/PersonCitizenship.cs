namespace PNG.U10.API.Models {
    /// <summary>
    /// PersonCitizenship data
    /// </summary>
    public class PersonCitizenship {
        /// <summary>
        /// PersonCitizenship Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Related CitizenshipDictionary
        /// </summary>
        public virtual CitizenshipDictionary Citizenship { get; set; }
    }
}