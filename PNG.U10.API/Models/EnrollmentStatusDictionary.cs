namespace PNG.U10.API.Models {
    /// <summary>
    /// StatusName data
    /// </summary>
    public class EnrollmentStatusDictionary {
        /// <summary>
        /// StatusName Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// StatusName Name
        /// </summary>
        public virtual string Name { get; set; }
    }
}