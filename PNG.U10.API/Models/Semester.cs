namespace PNG.U10.API.Models {
    /// <summary>
    /// Semester data
    /// </summary>
    public class Semester {
        // /// <summary>
        // /// Semester Id
        // /// </summary>
        // public int Id { get; set; }
        /// <summary>
        /// Semester name
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Number name
        /// </summary>
        public virtual int Number { get; set; }
    }
}