namespace PNG.U10.API.Models {
    /// <summary>
    /// Scholarship Status Dictionary data
    /// </summary>
    public class ScholarshipStatusDictionary {
        /// <summary>
        /// Scholarship Status Dictionary Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Scholarship Status Dictionary Name
        /// </summary>
        public virtual string Name { get; set; }
    }
}