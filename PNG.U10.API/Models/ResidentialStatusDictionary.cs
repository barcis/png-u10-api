namespace PNG.U10.API.Models {
    /// <summary>
    /// Residential Status Dictionary data
    /// </summary>
    public class ResidentialStatusDictionary {
        /// <summary>
        /// Residential Status Dictionary Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Residential Status Dictionary Name
        /// </summary>
        public virtual string Name { get; set; }
    }
}