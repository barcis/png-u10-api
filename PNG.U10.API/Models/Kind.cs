namespace PNG.U10.API.Models {
    /// <summary>
    /// Kind data
    /// </summary>
    public class Kind {
        /// <summary>
        /// Kind Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Kind code
        /// </summary>
        public virtual string Code { get; set; }

        /// <summary>
        /// Kind name
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Kind short
        /// </summary>
        public virtual string Short { get; set; }

        /// <summary>
        /// Mode related to Kind
        /// </summary>
        public virtual Mode Mode { get; set; }
    }

}