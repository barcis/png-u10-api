using System.Collections.Generic;

namespace PNG.U10.API.Models {
    /// <summary>
    /// Specialty data
    /// </summary>
    public class Specialty {
        /// <summary>
        /// Specialty Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Specialty name
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Specialty short
        /// </summary>
        public virtual string Short { get; set; }

        /// <summary>
        /// Specialty code
        /// </summary>
        public virtual string Code { get; set; }
    }
}