using System.Collections.Generic;

namespace PNG.U10.API.Models {
    /// <summary>
    /// MembershipAdditional data
    /// </summary>
    public class AdditionalType {
        /// <summary>
        /// Additional Type id
        /// </summary>
        public virtual int Id { get; set; }

        /// <summary>
        /// Additional Type name
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Additional Type description
        /// </summary>

        public virtual string Description { get; set; }

        /// <summary>
        /// Additional Type Type id
        /// :D
        /// </summary>

        public virtual int Type { get; set; }

        /// <summary>
        /// Additional Type Edit method
        /// :D
        /// </summary>

        public virtual int Edit { get; set; }

        /// <summary>
        /// Additional value options list
        /// </summary>
        public virtual IList<AdditionalDictionary> Options { get; set; }
    }
}