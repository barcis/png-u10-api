using System.Collections.Generic;

namespace PNG.U10.API.Models {
    /// <summary>
    /// Department data
    /// </summary>
    public class Department {
        /// <summary>
        /// Department Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Department name
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Programmes related to Department
        /// </summary>
        public virtual IList<Programme> Programmes { get; set; }

    }
}