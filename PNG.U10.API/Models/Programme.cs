namespace PNG.U10.API.Models {
    /// <summary>
    /// Programme data
    /// </summary>
    public class Programme {
        /// <summary>
        /// Programme Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Programme name
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Programme short
        /// </summary>
        public virtual string Short { get; set; }

        /// <summary>
        /// Programme code
        /// </summary>
        public virtual string Code { get; set; }
    }

}