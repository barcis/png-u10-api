namespace PNG.U10.API.Models {
    /// <summary>
    /// Port Of Travel Dictionary data
    /// </summary>
    public class PortOfTravelDictionary {
        /// <summary>
        /// Port Of Travel Dictionary Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Port Of Travel Dictionary Name
        /// </summary>
        public virtual string Name { get; set; }
    }
}