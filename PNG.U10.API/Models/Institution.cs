using System.Collections.Generic;

namespace PNG.U10.API.Models {
    /// <summary>
    /// Institution data
    /// </summary>
    public class Institution {
        /// <summary>
        /// Institution Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Institution name
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Institution id in Selection system
        /// </summary>
        public virtual int InstitutionId { get; set; }

        /// <summary>
        /// Departments related to Institution
        /// </summary>
        public virtual IList<Department> Departments { get; set; }
    }
}