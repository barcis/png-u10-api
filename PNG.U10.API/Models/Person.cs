using System;
using System.Collections.Generic;

namespace PNG.U10.API.Models {
    /// <summary>
    /// Person data
    /// </summary>
    public class Person {
        /// <summary>
        /// Person Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Person surname
        /// </summary>
        public virtual string Surname { get; set; }

        /// <summary>
        /// Person name
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Person gender
        /// </summary>
        public virtual int Gender { get; set; }

        /// <summary>
        /// Person SubId
        /// </summary>
        public virtual string SubId { get; set; }

        /// <summary>
        /// Person email
        /// </summary>
        public virtual string Email { get; set; }

        /// <summary>
        /// Person email2
        /// </summary>
        public virtual string Email2 { get; set; }

        /// <summary>
        /// Person email to work
        /// </summary>
        public virtual string EmailWork { get; set; }

        /// <summary>
        /// Person cell phone number
        /// </summary>
        public virtual string PhoneNumber { get; set; }

        /// <summary>
        /// Person cell phone number
        /// </summary>
        public virtual string PhoneNumberDormitory { get; set; }

        /// <summary>
        /// Person cell phone number
        /// </summary>
        public virtual string PhoneNumberHome { get; set; }

        /// <summary>
        /// Person cell phone number
        /// </summary>
        public virtual string PhoneNumberHome2 { get; set; }

        /// <summary>
        /// Person cell phone number
        /// </summary>
        public virtual string PhoneNumberWork { get; set; }

        /// <summary>
        /// Person cell phone number
        /// </summary>
        public virtual string PhoneNumberWork2 { get; set; }

        /// <summary>
        /// Person cell phone number
        /// </summary>
        public virtual string PhoneNumberOther { get; set; }

        /// <summary>
        /// Person Date Of Birth
        /// </summary>
        public virtual DateTime? DateOfBirth { get; set; }

        /// <summary>
        /// Person Place Of Birth
        /// </summary>
        public virtual string PlaceOfBirth { get; set; }

        /// <summary>
        /// Person citizenship
        /// </summary>
        public virtual IList<CitizenshipDictionary> Citizenships { get; set; }

        /// <summary>
        /// Person nationality
        /// </summary>
        public virtual string Nationality { get; set; }

        /// <summary>
        /// Person origin
        /// </summary>
        public virtual string Origin { get; set; }

        /// <summary>
        /// Person company
        /// </summary>
        public virtual string Company { get; set; }

        /// <summary>
        /// Student Marital Status
        /// 0 - unknown,
        /// 1 - unmarried woman
        /// 2 - unmarried man
        /// 3 - married woman
        /// 4 - married man
        /// 5 - widow
        /// 6 - widower
        /// </summary>
        public int MaritalStatus { get; set; }

        /// <summary>
        /// User role
        /// </summary>
        public virtual IList<Role> Roles { get; set; }
    }
}