namespace PNG.U10.API.Models {
    /// <summary>
    /// MembershipAdditional data
    /// </summary>
    public class MembershipAdditional {
        /// <summary>
        /// Additional id
        /// </summary>
        public virtual int Id { get; set; }

        /// <summary>
        /// Additional value
        /// </summary>
        public virtual object Value { get; set; }

        /// <summary>
        /// Membership Id
        /// </summary>
        public virtual int MembershipId { get; set; }

        /// <summary>
        /// Type Id
        /// </summary>
        public virtual AdditionalType Type { get; set; }
    }
}