using System.Collections.Generic;

namespace PNG.U10.API.Models {
    /// <summary>
    /// Dormitory Room data
    /// </summary>
    public class DormitoryRoom {
        /// <summary>
        /// Dormitory room id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Dormitory id
        /// </summary>
        public int DormitoryId { get; set; }

        /// <summary>
        /// Dormitory room number
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// Dormitory room gender <br/>
        /// 0 - Undefined <br/>
        /// 1 - Female <br/>
        /// 2 - Male
        /// </summary>
        public int Gender { get; set; }

        /// <summary>
        /// Floor on which the room is located
        /// </summary>
        public int Floor { get; set; }

        /// <summary>
        /// Number of places in the room
        /// </summary>
        public int Places { get; set; }

        /// <summary>
        /// Number of occupied places in the room
        /// </summary>
        public int OccupiedPlaces { get; set; }

        /// <summary>
        /// Number of free places in the room
        /// </summary>
        public int FreePlaces { get; set; }

        /// <summary>
        /// Number of reserved places in the room
        /// </summary>
        public int ReservedPlaces { get; set; }

        /// <summary>
        /// Phone number to room
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Network socket number
        /// </summary>
        public string NetworkSocket { get; set; }

        /// <summary>
        /// Room type id
        /// </summary>
        public int RoomTypeId { get; set; }

        /// <summary>
        /// Room type name
        /// </summary>
        public string RoomTypeName { get; set; }

        /// <summary>
        /// Room is active
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// List of room equipment
        /// </summary>
        public IList<DormitoryRoomEquipment> Equipments { get; set; }
    }
}