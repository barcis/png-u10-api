namespace PNG.U10.API.Models {
    /// <summary>
    /// Module data
    /// </summary>
    public class Module {

        /// <summary>
        /// Module Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Specialization related to Module
        /// </summary>
        public virtual Specialization Specialization { get; set; }

        /// <summary>
        /// Specialty related to Module
        /// </summary>
        public virtual Specialty Specialty { get; set; }
    }
}