using System;
using System.Collections.Generic;

namespace PNG.U10.API.Models {
    /// <summary>
    /// Membership data
    /// </summary>
    public class Membership {
        /// <summary>
        /// Membership Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Membership Album Number
        /// </summary>
        public virtual string AlbumNumber { get; set; }

        /// <summary>
        /// Membership Start Date
        /// </summary>
        public virtual DateTime? StartDate { get; set; }

        /// <summary>
        /// Membership Start Date on University
        /// </summary>
        public virtual DateTime? UniversityStartDate { get; set; }

        /// <summary>
        /// Membership is GPA
        /// </summary>
        public virtual double GPA { get; set; }

        /// <summary>
        /// Institution related to Membership
        /// </summary>
        public virtual Institution Institution { get; set; }

        /// <summary>
        /// Department related to Membership
        /// </summary>
        public virtual Department Department { get; set; }

        /// <summary>
        /// Programme related to Membership
        /// </summary>
        public virtual Programme Programme { get; set; }

        /// <summary>
        /// Kind related to Membership
        /// </summary>
        public virtual Kind Kind { get; set; }

        /// <summary>
        /// Module related to Membership
        /// </summary>
        public virtual Module Module { get; set; }

        /// <summary>
        /// Semester related to Membership
        /// </summary>
        public virtual MembershipSemester MembershipSemester { get; set; }
    }
}