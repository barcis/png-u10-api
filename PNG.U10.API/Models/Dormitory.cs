using System.Collections.Generic;

namespace PNG.U10.API.Models {
    /// <summary>
    /// Dormitory data
    /// </summary>
    public class Dormitory {
        /// <summary>
        /// Dormitory Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Dormitory name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Dormitory short name
        /// </summary>
        public string ShortName { get; set; }

        /// <summary>
        /// Dormitory number
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// Dormitory floors number
        /// </summary>
        public int Floors { get; set; }

        /// <summary>
        /// Dormitory rooms number
        /// </summary>
        public int RoomsNumber { get; set; }

        /// <summary>
        /// Dormitory rooms list
        /// </summary>
        public IList<DormitoryRoom> Rooms { get; set; }

        /// <summary>
        /// Dormitory is currently active
        /// </summary>
        public bool Active { get; set; }
    }
}