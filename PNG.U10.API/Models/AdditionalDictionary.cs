namespace PNG.U10.API.Models {
    /// <summary>
    /// AdditionalDictionary data
    /// </summary>
    public class AdditionalDictionary {
        /// <summary>
        /// AdditionalDictionary id
        /// </summary>
        public virtual int Id { get; set; }

        /// <summary>
        /// AdditionalDictionary Ordinal Number (LP)
        /// </summary>
        public virtual int OrdinalNumber { get; set; }

        /// <summary>
        /// AdditionalDictionary Value
        /// </summary>
        public virtual string Value { get; set; }

        /// <summary>
        /// AdditionalDictionary Short name
        /// </summary>
        public virtual string Short { get; set; }
    }
}