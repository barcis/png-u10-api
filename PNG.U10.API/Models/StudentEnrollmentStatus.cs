namespace PNG.U10.API.Models {
    /// <summary>
    /// StudentEnrollmentStatus data
    /// </summary>
    public class StudentEnrollmentStatus {
        /// <summary>
        /// StudentEnrollmentStatus Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// EnrollmentStatusDictionary related to Status
        /// </summary>
        public virtual EnrollmentStatusDictionary Value { get; set; }
    }
}