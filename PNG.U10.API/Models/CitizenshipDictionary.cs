namespace PNG.U10.API.Models {
    /// <summary>
    /// CitizenshipDictionary data
    /// </summary>
    public class CitizenshipDictionary {
        /// <summary>
        /// Citizenship Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Citizenship name
        /// </summary>
        public virtual string Value { get; set; }
        /// <summary>
        /// Citizenship Short
        /// </summary>

        public virtual string Short { get; set; }

        /// <summary>
        /// Citizenship dictionary TypeId
        /// </summary>
        public int TypeId { get; set; }
    }
}