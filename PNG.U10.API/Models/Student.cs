using System.Collections.Generic;

namespace PNG.U10.API.Models {
    /// <summary>
    /// Student data
    /// </summary>
    public class Student {
        /// <summary>
        /// Student Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Person related with Student
        /// </summary>
        public virtual Person Person { get; set; }

        /// <summary>
        /// Student Id from Uni10 system
        /// </summary>
        public virtual string Uni10Id { get; set; }

        /// <summary>
        /// Memberships related with Student
        /// </summary>
        public virtual IList<Membership> Memberships { get; set; }

        /// <summary>
        /// Student allocator name
        /// </summary>
        public virtual string Allocator { get; set; }

        /// <summary>
        /// Student Comment value
        /// </summary>
        public virtual string Comment { get; set; }

        /// <summary>
        /// Student PortOfTravel value
        /// </summary>
        public virtual string PortOfTravel { get; set; }

        /// <summary>
        /// Student ResidentialStatus value
        /// </summary>
        public virtual string ResidentialStatus { get; set; }

        /// <summary>
        /// Student ScholarshipStatus value
        /// </summary>
        public virtual string ScholarshipStatus { get; set; }

        /// <summary>
        /// Student OtherStatus value
        /// </summary>
        public virtual string OtherStatus { get; set; }
    }
}