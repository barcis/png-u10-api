namespace PNG.U10.API.Models {
    /// <summary>
    /// Other Status Dictionary data
    /// </summary>
    public class OtherStatusDictionary {
        /// <summary>
        /// Other Status Dictionary Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Other Status Dictionary Name
        /// </summary>
        public virtual string Name { get; set; }
    }
}