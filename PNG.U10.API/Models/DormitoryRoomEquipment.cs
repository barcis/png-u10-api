namespace PNG.U10.API.Models {
    /// <summary>
    /// Dormitory Room Equipment data
    /// </summary>
    public class DormitoryRoomEquipment {
        /// <summary>
        /// Dormitory room equipment id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Dormitory room id
        /// </summary>
        public int DormitoryRoomId { get; set; }

        /// <summary>
        /// Equipment full name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Equipment description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Equipment type name
        /// </summary>
        public string TypeName { get; set; }
    }
}