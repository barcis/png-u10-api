using System.Collections.Generic;

namespace PNG.U10.API.Models {
    /// <summary>
    /// MembershipSemester data
    /// </summary>
    public class MembershipSemester {
        /// <summary>
        /// MembershipSemester Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// MembershipSemester Year
        /// </summary>
        public virtual int Year { get; set; }

        /// <summary>
        /// ManualAverage Year
        /// </summary>
        public virtual double ManualAverage { get; set; }

        /// <summary>
        /// Semester realted to MembershipSemester
        /// </summary>
        public virtual Semester Semester { get; set; }

        /// <summary>
        /// StudentEnrollmentStatus related to MembershipSemester
        /// </summary>
        public virtual StudentEnrollmentStatus EnrollmentStatus { get; set; }
        // public virtual EnrollmentStatusDictionary EnrollmentStatus { get; set; }
    }
}