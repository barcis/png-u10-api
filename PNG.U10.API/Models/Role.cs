namespace PNG.U10.API.Models {
    /// <summary>
    /// Role data
    /// </summary>
    public class Role {
        /// <summary>
        /// Role Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Role name
        /// </summary>
        public virtual string Name { get; set; }
    }
}