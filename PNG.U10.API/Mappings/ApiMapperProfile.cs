using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using PNG.U10.API.Models;
using PNG.U10.Domain.Models;

namespace PNG.U10.API.Mappings {
    /// <summary>
    /// Creates maps for AutoMapper
    /// </summary>
    public class ApiMapperProfile : Profile {

        /// <summary>
        /// Constructor
        /// </summary>
        public ApiMapperProfile () {

            AllowNullCollections = true;

            CreateMap<AdditionalTypeDTO, AdditionalType> ().ReverseMap ();
            CreateMap<AdditionalDictionaryDTO, AdditionalDictionary> ().ReverseMap ();
            CreateMap<DormitoryDTO, Dormitory> ().ReverseMap ();
            CreateMap<DormitoryRoomDTO, DormitoryRoom> ().ReverseMap ();
            CreateMap<DormitoryRoomEquipmentDTO, DormitoryRoomEquipment> ().ReverseMap ();
            CreateMap<StudentDTO, Student> ().ReverseMap ();
            CreateMap<StudentAdditionalDTO, StudentAdditional> ().ReverseMap ();
            CreateMap<PersonDTO, Person> ().ReverseMap ();
            CreateMap<PersonCitizenshipDTO, PersonCitizenship> ().ReverseMap ();
            CreateMap<PlaceDictionaryDTO, PlaceDictionary> ().ReverseMap ();
            CreateMap<CitizenshipDictionaryDTO, CitizenshipDictionary> ().ReverseMap ();
            CreateMap<UserDTO, User> ()
                .ForMember (
                    dest => dest.SubId,
                    opts => opts.MapFrom (src => src.Person.SubId))
                .ForMember (
                    dest => dest.Surname,
                    opts => opts.MapFrom (src => src.Person.Surname))
                .ForMember (
                    dest => dest.Name,
                    opts => opts.MapFrom (src => src.Person.Name))
                .ForMember (
                    dest => dest.Email,
                    opts => opts.MapFrom (src => src.Person.Email))
                .ForMember (
                    dest => dest.Roles,
                    opts => opts.MapFrom (src => src.Person.Roles))
                .ReverseMap ();
            CreateMap<RoleDTO, Role> ().ReverseMap ();
            CreateMap<MembershipAdditionalDTO, MembershipAdditional> ().ReverseMap ();
            CreateMap<MembershipDTO, Membership> ()
                .ForMember (
                    dest => dest.MembershipSemester,
                    opts => opts.MapFrom (src => src.MembershipSemesters.FirstOrDefault ()))
                .ForMember (
                    dest => dest.Module,
                    opts => opts.MapFrom (src => src.Modules.FirstOrDefault ()))
                .ReverseMap ()
                .ForPath (d => d.MembershipSemesters, opt => opt.MapFrom (s => new List<MembershipSemester> () { s.MembershipSemester }))
                .ForPath (d => d.Modules, opt => opt.MapFrom (s => new List<Module> () { s.Module }));

            CreateMap<InstitutionDTO, Institution> ().ReverseMap ();
            CreateMap<DepartmentDTO, Department> ().ReverseMap ();
            CreateMap<ProgrammeDTO, Programme> ().ReverseMap ();
            CreateMap<KindDTO, Kind> ().ReverseMap ();
            CreateMap<ModeDTO, Mode> ().ReverseMap ();
            CreateMap<SemesterDTO, Semester> ().ReverseMap ();
            CreateMap<SpecializationDTO, Specialization> ().ReverseMap ();
            CreateMap<SpecialtyDTO, Specialty> ().ReverseMap ();
            CreateMap<ModuleDTO, Module> ().ReverseMap ();
            CreateMap<MembershipSemesterDTO, MembershipSemester> ()
                .ForMember (
                    d => d.EnrollmentStatus,
                    opt => opt.MapFrom (s => s.Statuses.FirstOrDefault ()))

                .ReverseMap ()
                .ForPath (d => d.Statuses, opt => opt.MapFrom (s => new List<StudentEnrollmentStatus> () { s.EnrollmentStatus }));

            CreateMap<StudentEnrollmentStatusDTO, StudentEnrollmentStatus> ().ReverseMap ();

            CreateMap<EnrollmentStatusDictionaryDTO, EnrollmentStatusDictionary> ().ReverseMap ();
            CreateMap<OtherStatusDictionaryDTO, OtherStatusDictionary> ().ReverseMap ();
            CreateMap<PortOfTravelDictionaryDTO, PortOfTravelDictionary> ().ReverseMap ();
            CreateMap<ResidentialStatusDictionaryDTO, ResidentialStatusDictionary> ().ReverseMap ();
            CreateMap<ScholarshipStatusDictionaryDTO, ScholarshipStatusDictionary> ().ReverseMap ();
        }
    }
}