using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PNG.U10.API.SeedWork {
    /// <summary>
    /// Page pagination parameter
    /// </summary>
    public class Pagination {
        /// <summary>
        /// Page number start from 0
        /// </summary>
        [Required]
        [DefaultValue (0)]
        public int PageNumber { get; set; } = 0;

        /// <summary>
        /// Number of elements on page default 100
        /// </summary>
        [Required]
        [DefaultValue (100)]
        public int PageSize { get; set; } = 100;

    }
}