﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace PNGU10API.Core {
    /// <summary>
    /// API Main class
    /// </summary>
    public class Program {
        /// <summary>
        /// API Main Method
        /// </summary>
        public static void Main (string[] args) {
            CreateHostBuilder (args).Build ().Run ();
        }

        private static IHostBuilder CreateHostBuilder (string[] args) =>
            Host.CreateDefaultBuilder (args)
            .ConfigureWebHostDefaults (webBuilder => {
                webBuilder.UseStartup<Startup> ();
            });
    }
}