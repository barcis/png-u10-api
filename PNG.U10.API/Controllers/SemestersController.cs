using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PNG.U10.API.Models;
using PNG.U10.API.SeedWork;
using PNG.U10.Domain.Models;
using PNG.U10.Domain.Services.Abstract;

namespace PNGU10API.Controllers {
    /// <summary>
    /// Semesters Controller
    /// </summary>
    [Route ("api/[controller]")]
    [Produces ("application/json")]
    [ApiController]
    [Authorize]
    public class SemestersController : ControllerBase {
        private readonly IMapper mapper;
        private readonly IPathService service;

        /// <summary>
        /// Constructor
        /// </summary>
        public SemestersController (IPathService _service, IMapper _mapper) {
            mapper = _mapper;
            service = _service;
        }

        /// <summary>
        /// Return semesters list
        /// </summary>
        /// <returns>semesters</returns>
        /// <response code="200">Return values</response>
        [HttpGet ()]
        [ProducesResponseType (typeof (IEnumerable<MembershipSemester>), 200)]
        public ActionResult<IEnumerable<MembershipSemester>> GetAll ([FromQuery] int[] institutions, [FromQuery] int[] departments, [FromRoute] int[] programmes, [FromQuery] Pagination pagination) {
            try {
                if (pagination == null) {
                    pagination = new Pagination ();
                }

                var itemsDTO = service.GetSemesters (institutions, departments, programmes, pagination.PageNumber, pagination.PageSize);
                if (itemsDTO == null) {
                    return NotFound ();
                }
                var items = mapper.Map<IList<MembershipSemester>> (itemsDTO);
                return Ok (items);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }

        /// <summary>
        /// Return semesters list
        /// </summary>
        /// <returns>semesters</returns>
        /// <response code="200">Return values</response>
        [HttpGet ("Years")]
        [ProducesResponseType (typeof (IEnumerable<MembershipSemester>), 200)]
        public ActionResult<IEnumerable<MembershipSemester>> GetYears ([FromQuery] int[] institutions, [FromQuery] int[] departments, [FromRoute] int[] programmes, [FromQuery] Pagination pagination) {
            try {
                if (pagination == null) {
                    pagination = new Pagination ();
                }

                var itemsDTO = service.GetSemesters (institutions, departments, programmes, pagination.PageNumber, pagination.PageSize);
                if (itemsDTO == null) {
                    return NotFound ();
                }

                IEnumerable<MembershipSemesterDTO> filteredList = itemsDTO
                    .GroupBy (membershipSemester => membershipSemester.Semester.Number)
                    .Select (group => group.First ());

                var items = mapper.Map<IList<MembershipSemester>> (filteredList);
                return Ok (items);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }

        /// <summary>
        /// Return semester details
        /// </summary>
        /// <returns>semester</returns>
        /// <response code="200">Return value</response>
        [HttpGet ("{id:int}")]
        [ProducesResponseType (typeof (MembershipSemester), 200)]
        public ActionResult<IEnumerable<MembershipSemester>> GetOne ([FromRoute] int id) {
            try {
                var itemDTO = service.GetSemester (id);
                if (itemDTO == null) {
                    return NotFound ();
                }
                var item = mapper.Map<MembershipSemester> (itemDTO);
                return Ok (item);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }
    }
}