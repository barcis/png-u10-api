using System;
using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PNG.U10.API.Models;
using PNG.U10.API.SeedWork;
using PNG.U10.Domain.Services.Abstract;

namespace PNGU10API.Controllers {
    /// <summary>
    /// Additional Controller
    /// </summary>
    [Route ("api/[controller]")]
    [Produces ("application/json")]
    [ApiController]
    [Authorize]
    public class AdditionalController : ControllerBase {
        private readonly IMapper mapper;
        private readonly IAdditionalService service;

        /// <summary>
        /// Constructor
        /// </summary>
        public AdditionalController (IAdditionalService _service, IMapper _mapper) {
            mapper = _mapper;
            service = _service;
        }

        /// <summary>
        /// Return student additional details
        /// </summary>
        /// <returns>student additional</returns>
        /// <response code="200">Return value</response>
        [HttpGet ("{typeId:int}/Student/{studentId:int}")]
        [ProducesResponseType (typeof (StudentAdditional), 200)]
        public ActionResult<StudentAdditional> GetStudentAddtional ([FromRoute] int studentId, [FromRoute] int typeId) {
            try {
                var itemDTO = service.GetStudentAdditional (studentId, typeId);
                if (itemDTO == null) {
                    return NotFound ();
                }
                var item = mapper.Map<StudentAdditional> (itemDTO);
                return Ok (item);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }
        /// <summary>
        /// Return student additionala list
        /// </summary>
        /// <returns>student additionala</returns>
        /// <response code="200">Return value</response>
        [HttpGet ("Student/{studentId:int}")]
        [ProducesResponseType (typeof (IEnumerable<StudentAdditional>), 200)]
        public ActionResult<IEnumerable<StudentAdditional>> GetStudentAddtionals ([FromRoute] int studentId, [FromQuery] Pagination pagination) {
            try {
                if (pagination == null) {
                    pagination = new Pagination ();
                }

                var itemsDTO = service.GetStudentAdditionals (studentId, pagination.PageNumber, pagination.PageSize);
                if (itemsDTO == null) {
                    return NotFound ();
                }
                var items = mapper.Map<IList<StudentAdditional>> (itemsDTO);
                return Ok (items);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }

        /// <summary>
        /// Return membership additional details
        /// </summary>
        /// <returns>membership additional</returns>
        /// <response code="200">Return value</response>
        [HttpGet ("{typeId:int}/Membership/{membershipId:int}")]
        [ProducesResponseType (typeof (MembershipAdditional), 200)]
        public ActionResult<MembershipAdditional> GetMembershipAddtional ([FromRoute] int membershipId, [FromRoute] int typeId) {
            try {
                var itemDTO = service.GetMembershipAdditional (membershipId, typeId);
                if (itemDTO == null) {
                    return NotFound ();
                }
                var item = mapper.Map<MembershipAdditional> (itemDTO);
                return Ok (item);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }

        /// <summary>
        /// Return membership additional details
        /// </summary>
        /// <returns>membership additional</returns>
        /// <response code="200">Return value</response>
        [HttpGet ("Membership/{membershipId:int}")]
        [ProducesResponseType (typeof (IEnumerable<MembershipAdditional>), 200)]
        public ActionResult<IEnumerable<MembershipAdditional>> GetMembershipAddtional ([FromRoute] int membershipId, [FromQuery] Pagination pagination) {
            try {
                if (pagination == null) {
                    pagination = new Pagination ();
                }

                var itemsDTO = service.GetMembershipAdditionals (membershipId, pagination.PageNumber, pagination.PageSize);
                if (itemsDTO == null) {
                    return NotFound ();
                }
                var items = mapper.Map<IList<StudentAdditional>> (itemsDTO);
                return Ok (items);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }
    }
}