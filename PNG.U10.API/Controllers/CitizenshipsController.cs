using System;
using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PNG.U10.API.Models;
using PNG.U10.API.SeedWork;
using PNG.U10.Domain.Services.Abstract;

namespace PNGU10API.Controllers {
    /// <summary>
    /// Citizenships Controller
    /// </summary>
    [Route ("api/[controller]")]
    [Produces ("application/json")]
    [ApiController]
    [Authorize]
    public class CitizenshipsController : ControllerBase {
        private readonly IMapper mapper;
        private readonly IDictionaryService service;

        /// <summary>
        /// Constructor
        /// </summary>
        public CitizenshipsController (IDictionaryService _service, IMapper _mapper) {
            mapper = _mapper;
            service = _service;
        }

        /// <summary>
        /// Return citizenships list
        /// </summary>
        /// <returns>citizenships</returns>
        /// <response code="200">Return values</response>
        [HttpGet ()]
        [ProducesResponseType (typeof (IEnumerable<CitizenshipDictionary>), 200)]
        public ActionResult<IEnumerable<CitizenshipDictionary>> GetAll ([FromQuery] Pagination pagination) {
            try {
                if (pagination == null) {
                    pagination = new Pagination ();
                }

                var itemsDTO = service.GetCitizenships (pagination.PageNumber, pagination.PageSize);
                if (itemsDTO == null) {
                    return NotFound ();
                }
                var items = mapper.Map<IList<CitizenshipDictionary>> (itemsDTO);
                return Ok (items);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }

        /// <summary>
        /// Return citizenship details
        /// </summary>
        /// <returns>citizenship</returns>
        /// <response code="200">Return value</response>
        [HttpGet ("{id:int}")]
        [ProducesResponseType (typeof (CitizenshipDictionary), 200)]
        public ActionResult<CitizenshipDictionary> GetOne ([FromRoute] int id) {
            try {
                var itemDTO = service.GetCitizenships (id);
                if (itemDTO == null) {
                    return NotFound ();
                }
                var item = mapper.Map<CitizenshipDictionary> (itemDTO);
                return Ok (item);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }
    }
}