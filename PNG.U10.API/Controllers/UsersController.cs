using System;
using System.Collections.Generic;
using System.Security.Claims;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PNG.U10.API.Models;
using PNG.U10.API.SeedWork;
using PNG.U10.Domain.Models;
using PNG.U10.Domain.Services.Abstract;

namespace PNGU10API.Controllers {
    /// <summary>
    /// Users Controller
    /// </summary>
    [Route ("api/[controller]")]
    [Produces ("application/json")]
    [ApiController]
    [Authorize]
    public class UsersController : ControllerBase {

        private readonly IUserService service;

        private readonly IMapper mapper;

        /// <summary>
        /// Constructor
        /// </summary>
        public UsersController (IUserService _service, IMapper _mapper) {
            mapper = _mapper;
            service = _service;
        }

        /// <summary>
        /// Return values
        /// </summary>
        /// <returns>List of values</returns>
        /// <response code="200">Return values</response>
        /// <response code="500">Exception</response>
        /// <param name="pagination">Pagination</param>
        /// <param name="role">Role name</param>
        [HttpGet ("")]
        [ProducesResponseType (typeof (IEnumerable<User>), 200)]
        [ProducesResponseType (500)]
        public ActionResult<IEnumerable<User>> GetAll ([FromQuery] Pagination pagination, [FromQuery (Name = "role")] string role = null) {
            try {
                if (pagination == null) {
                    pagination = new Pagination ();
                }

                IEnumerable<UserDTO> itemsDTO;
                if (role == null) {
                    itemsDTO = service.GetAll (pagination.PageNumber, pagination.PageSize);
                } else {
                    itemsDTO = service.GetAllWithRole (role, pagination.PageNumber, pagination.PageSize);
                }

                var items = mapper.Map<IList<User>> (itemsDTO);
                return Ok (items);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }

        /// <summary>
        /// Return user details
        /// </summary>
        /// <returns>user details</returns>
        /// <response code="200">Return value</response>
        /// <response code="404">Not found value</response>
        /// <response code="500">Exception</response>
        [HttpGet ("{id}")]
        [ProducesResponseType (typeof (IEnumerable<User>), 200)]
        [ProducesResponseType (404)]
        [ProducesResponseType (500)]
        public ActionResult<IEnumerable<User>> GetOne (int id) {
            try {
                var itemDTO = service.GetOne (id);
                if (itemDTO == null) {
                    return NotFound ();
                }
                var item = mapper.Map<User> (itemDTO);
                return Ok (item);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }

        /// <summary>
        /// Return user details
        /// </summary>
        /// <returns>user details</returns>
        /// <response code="200">Return value</response>
        /// <response code="404">Not found value</response>
        /// <response code="500">Exception</response>
        /// <param name="personId">Person Id</param>
        [HttpGet ("personId/{personId}")]
        [ProducesResponseType (typeof (IEnumerable<User>), 200)]
        [ProducesResponseType (404)]
        [ProducesResponseType (500)]
        public ActionResult<IEnumerable<User>> GetOneByPersonId (int personId) {
            try {
                var itemDTO = service.GetOneByPersonId (personId);
                if (itemDTO == null) {
                    return NotFound ();
                }
                var item = mapper.Map<User> (itemDTO);
                return Ok (item);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }

        /// <summary>
        /// Return user details
        /// </summary>
        /// <returns>user details</returns>
        /// <response code="200">Return value</response>
        /// <response code="404">Not found value</response>
        /// <response code="500">Exception</response>
        /// <param name="subId">Person subId</param>
        [HttpGet ("subId/{subId}")]
        [ProducesResponseType (typeof (IEnumerable<User>), 200)]
        [ProducesResponseType (404)]
        [ProducesResponseType (500)]
        public ActionResult<IEnumerable<User>> GetOneBySubId (string subId) {
            try {
                var itemDTO = service.GetOneBySubId (subId);
                if (itemDTO == null) {
                    return NotFound ();
                }
                var item = mapper.Map<User> (itemDTO);
                return Ok (item);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }

        /// <summary>
        /// Return current user details
        /// </summary>
        /// <returns>user details</returns>
        /// <response code="200">Return value</response>
        [HttpGet ("me")]
        [ProducesResponseType (typeof (IEnumerable<User>), 200)]
        public ActionResult<IEnumerable<User>> GetMe () {
            try {
                var subId = string.Empty;
                if (User.Identity is ClaimsIdentity identity) {
                    subId = identity.FindFirst ("sub").Value;
                }

                var itemDTO = service.GetOneBySubId (subId);
                if (itemDTO == null) {
                    return NotFound ();
                }
                var item = mapper.Map<User> (itemDTO);
                return Ok (item);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }
    }
}