using System;
using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PNG.U10.API.Models;
using PNG.U10.API.SeedWork;
using PNG.U10.Domain.Services.Abstract;

namespace PNGU10API.Controllers {
    /// <summary>
    /// Departments Controller
    /// </summary>
    [Route ("api/[controller]")]
    [Produces ("application/json")]
    [ApiController]
    [Authorize]
    public class DepartmentsController : ControllerBase {
        private readonly IMapper mapper;
        private readonly IDepartmentService service;

        /// <summary>
        /// Constructor
        /// </summary>
        public DepartmentsController (IDepartmentService _service, IMapper _mapper) {
            mapper = _mapper;
            service = _service;
        }

        /// <summary>
        /// Return departments list
        /// </summary>
        /// <returns>departments</returns>
        /// <response code="200">Return values</response>
        [HttpGet ()]
        [ProducesResponseType (typeof (IEnumerable<Department>), 200)]
        public ActionResult<IEnumerable<Department>> GetAll ([FromQuery] int[] institutions, [FromQuery] Pagination pagination) {
            try {
                if (pagination == null) {
                    pagination = new Pagination ();
                }

                var itemsDTO = service.GetDepartments (institutions, pagination.PageNumber, pagination.PageSize);

                if (itemsDTO == null) {
                    return NotFound ();
                }
                var items = mapper.Map<IList<Department>> (itemsDTO);
                return Ok (items);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }

        /// <summary>
        /// Return department details
        /// </summary>
        /// <returns>department</returns>
        /// <response code="200">Return value</response>
        [HttpGet ("{id:int}")]
        [ProducesResponseType (typeof (Department), 200)]
        public ActionResult<IEnumerable<Department>> GetOne ([FromRoute] int id) {
            try {
                var itemDTO = service.GetDepartment (id);
                if (itemDTO == null) {
                    return NotFound ();
                }
                var item = mapper.Map<Department> (itemDTO);
                return Ok (item);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }
    }
}