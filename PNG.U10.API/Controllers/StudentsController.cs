using System;
using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using PNG.U10.API.Helpers;
using PNG.U10.API.Models;
using PNG.U10.API.SeedWork;
using PNG.U10.Domain.Models;
using PNG.U10.Domain.Repositories.Filters;
using PNG.U10.Domain.Services.Abstract;

namespace PNGU10API.Controllers {
    /// <summary>
    /// Students Controller
    /// </summary>
    [Route ("api/[controller]")]
    [Produces ("application/json")]
    [ApiController]
    [Authorize]
    public class StudentsController : ControllerBase {

        private readonly IStudentService service;
        private readonly IMapper mapper;

        /// <summary>
        /// Constructor
        /// </summary>
        public StudentsController (IStudentService _service, IMapper _mapper) {
            mapper = _mapper;
            service = _service;
        }

        /// <summary>
        /// Return students list
        /// </summary>
        /// <returns>List of students</returns>
        /// <response code="200">Return values</response>
        /// <response code="500">Exception</response>
        /// <param name="pagination">Pagination page number and page size</param>
        [HttpGet]
        [ProducesResponseType (typeof (IEnumerable<Student>), StatusCodes.Status200OK)]
        [ProducesResponseType (StatusCodes.Status500InternalServerError)]
        public ActionResult<IEnumerable<Student>> GetStudents ([FromQuery] Pagination pagination) {
            try {
                if (pagination == null) {
                    pagination = new Pagination ();
                }
                StudentFilter filter = new StudentFilter ();

                if (User.IsInRole (Roles.Allocator)) {
                    filter.Allocator = User.Identity.Name;
                }

                var itemsDTO = service.GetAll (pagination.PageNumber, pagination.PageSize, filter);
                var items = mapper.Map<IList<Student>> (itemsDTO);

                Response.Headers.Add ("x-results-count", "200");

                return Ok (items);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }

        /// <summary>
        /// Return student details
        /// </summary>
        /// <returns>Student details</returns>
        /// <response code="200">Return value</response>
        /// <response code="404">Not found value</response>
        /// <response code="500">Exception</response>
        /// <param name="id">Student id</param>
        [HttpGet ("{id:int}")]
        [ProducesResponseType (typeof (Student), StatusCodes.Status200OK)]
        [ProducesResponseType (StatusCodes.Status404NotFound)]
        [ProducesResponseType (StatusCodes.Status500InternalServerError)]
        public ActionResult<Student> GetStudent (int id) {
            try {
                StudentFilter filter = new StudentFilter () {
                    Id = id
                };

                if (User.IsInRole (Roles.Allocator)) {
                    filter.Allocator = User.Identity.Name;
                }

                var itemDTO = service.GetOne (filter);
                if (itemDTO == null) {
                    return NotFound ();
                }
                var item = mapper.Map<Student> (itemDTO);
                return Ok (item);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }

        /// <summary>
        /// Patch student details
        /// </summary>
        /// <param name="id">student id</param>
        /// <param name="patchStudent">Student patch document</param>
        /// <response code="204">Return value</response>
        /// <response code="404">Not found value</response>
        /// <response code="500">Exception</response>
        [HttpPatch ("{id:int}")]
        [ProducesResponseType (StatusCodes.Status204NoContent)]
        [ProducesResponseType (StatusCodes.Status404NotFound)]
        [ProducesResponseType (StatusCodes.Status400BadRequest)]
        [ProducesResponseType (StatusCodes.Status500InternalServerError)]
        public IActionResult PatchOne (int id, [FromBody] JsonPatchDocument<Student> patchStudent) {
            try {
                if (patchStudent == null) {
                    return BadRequest ();
                }

                StudentFilter filter = new StudentFilter () {
                    Id = id
                };

                if (User.IsInRole (Roles.Allocator)) {
                    filter.Allocator = User.Identity.Name;
                }

                StudentDTO itemDTO = service.GetOne (filter);

                if (itemDTO == null) {
                    return NotFound ();
                }

                Student item = mapper.Map<Student> (itemDTO);

                patchStudent.ApplyTo (item);

                var isValid = TryValidateModel (item);

                if (!isValid) {
                    return BadRequest (ModelState);
                }

                StudentDTO patchedDTO = mapper.Map<StudentDTO> (item);
                service.UpdateOne (filter, patchedDTO);

                return NoContent ();
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }

        /// <summary>
        /// Patch student details
        /// </summary>
        /// <param name="newStudent">Student patch document</param>
        /// <response code="201">Return value</response>
        /// <response code="404">Not found value</response>
        /// <response code="500">Exception</response>
        [HttpPost ()]
        [ProducesResponseType (StatusCodes.Status201Created)]
        [ProducesResponseType (StatusCodes.Status404NotFound)]
        [ProducesResponseType (StatusCodes.Status400BadRequest)]
        [ProducesResponseType (StatusCodes.Status500InternalServerError)]
        public IActionResult CreateOne ([FromBody] Student newStudent) {
            try {
                if (newStudent == null) {
                    return BadRequest ();
                }

                StudentDTO newDTO = mapper.Map<StudentDTO> (newStudent);
                StudentDTO retunedDTO = service.CreateOne (newDTO);

                return CreatedAtAction (nameof (GetStudent), new { id = retunedDTO.Id }, retunedDTO);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }

        /// <summary>
        /// Return student details
        /// </summary>
        /// <returns>Student details</returns>
        /// <response code="200">Return value</response>
        /// <response code="404">Not found value</response>
        /// <response code="500">Exception</response>
        /// <param name="personId">Person Id</param>
        [HttpGet ("personId/{personId}")]
        [ProducesResponseType (typeof (Student), StatusCodes.Status200OK)]
        [ProducesResponseType (StatusCodes.Status404NotFound)]
        [ProducesResponseType (StatusCodes.Status500InternalServerError)]
        public ActionResult<Student> GetOneByPersonId (int personId) {
            try {
                StudentFilter filter = new StudentFilter () {
                    PersonId = personId
                };

                if (User.IsInRole (Roles.Allocator)) {
                    filter.Allocator = User.Identity.Name;
                }

                var itemDTO = service.GetOne (filter);
                if (itemDTO == null) {
                    return NotFound ();
                }
                var item = mapper.Map<Student> (itemDTO);
                return Ok (item);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }

        /// <summary>
        /// Return student details
        /// </summary>
        /// <returns>Student details</returns>
        /// <response code="200">Return value</response>
        /// <response code="404">Not found value</response>
        /// <response code="500">Exception</response>
        /// <param name="subId">Person subId</param>
        [HttpGet ("subId/{subId}")]
        [ProducesResponseType (typeof (Student), StatusCodes.Status200OK)]
        [ProducesResponseType (StatusCodes.Status404NotFound)]
        [ProducesResponseType (StatusCodes.Status500InternalServerError)]
        public ActionResult<Student> GetOneBySubId (string subId) {
            try {
                StudentFilter filter = new StudentFilter () {
                    PersonSubId = subId
                };

                if (User.IsInRole (Roles.Allocator)) {
                    filter.Allocator = User.Identity.Name;
                }

                var itemDTO = service.GetOne (filter);
                if (itemDTO == null) {
                    return NotFound ();
                }
                var item = mapper.Map<Student> (itemDTO);
                return Ok (item);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }

        /// <summary>
        /// Return student details
        /// </summary>
        /// <returns>Student details</returns>
        /// <response code="200">Return value</response>
        /// <response code="404">Not found value</response>
        /// <response code="500">Exception</response>
        /// <param name="slf">Student SLF</param>
        [HttpGet ("uni10id/{slf}")]
        [ProducesResponseType (typeof (Student), StatusCodes.Status200OK)]
        [ProducesResponseType (StatusCodes.Status404NotFound)]
        [ProducesResponseType (StatusCodes.Status500InternalServerError)]
        public ActionResult<Student> GetOneByU10Id (string slf) {
            try {
                StudentFilter filter = new StudentFilter () {
                    Uni10Id = slf
                };

                if (User.IsInRole (Roles.Allocator)) {
                    filter.Allocator = User.Identity.Name;
                }

                var itemDTO = service.GetOne (filter);
                if (itemDTO == null) {
                    return NotFound ();
                }
                var item = mapper.Map<Student> (itemDTO);
                return Ok (item);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }
    }
}