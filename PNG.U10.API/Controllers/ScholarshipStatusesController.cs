using System;
using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PNG.U10.API.Models;
using PNG.U10.API.SeedWork;
using PNG.U10.Domain.Services.Abstract;

namespace PNGU10API.Controllers {
    /// <summary>
    /// ScholarshipStatuses Controller
    /// </summary>
    [Route ("api/[controller]")]
    [Produces ("application/json")]
    [ApiController]
    [Authorize]
    public class ScholarshipStatusesController : ControllerBase {
        private readonly IMapper mapper;
        private readonly IScholarshipStatusService service;

        /// <summary>
        /// Constructor
        /// </summary>
        public ScholarshipStatusesController (IScholarshipStatusService _service, IMapper _mapper) {
            mapper = _mapper;
            service = _service;
        }

        /// <summary>
        /// Return scholarship statuses list
        /// </summary>
        /// <returns>scholarship statuses</returns>
        /// <response code="200">Return values</response>
        [HttpGet ()]
        [ProducesResponseType (typeof (IEnumerable<ScholarshipStatusDictionary>), 200)]
        public ActionResult<IEnumerable<ScholarshipStatusDictionary>> GetAll ([FromQuery] Pagination pagination) {
            try {
                if (pagination == null) {
                    pagination = new Pagination ();
                }

                var itemsDTO = service.GetScholarshipStatuses (pagination.PageNumber, pagination.PageSize);
                if (itemsDTO == null) {
                    return NotFound ();
                }
                var items = mapper.Map<IList<ScholarshipStatusDictionary>> (itemsDTO);
                return Ok (items);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }

        /// <summary>
        /// Return scholarship status details
        /// </summary>
        /// <returns>scholarship status</returns>
        /// <response code="200">Return value</response>
        [HttpGet ("{id:int}")]
        [ProducesResponseType (typeof (ScholarshipStatusDictionary), 200)]
        public ActionResult<ScholarshipStatusDictionary> GetOne ([FromRoute] int id) {
            try {
                var itemDTO = service.GetScholarshipStatus (id);
                if (itemDTO == null) {
                    return NotFound ();
                }
                var item = mapper.Map<ScholarshipStatusDictionary> (itemDTO);
                return Ok (item);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }
    }
}