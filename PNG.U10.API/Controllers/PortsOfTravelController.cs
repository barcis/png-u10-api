using System;
using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PNG.U10.API.Models;
using PNG.U10.API.SeedWork;
using PNG.U10.Domain.Services.Abstract;

namespace PNGU10API.Controllers {
    /// <summary>
    /// PortsOfTravel Controller
    /// </summary>
    [Route ("api/[controller]")]
    [Produces ("application/json")]
    [ApiController]
    [Authorize]
    public partial class PortsOfTravelController : ControllerBase {
        private readonly IMapper mapper;
        private readonly IPortOfTravelService service;

        /// <summary>
        /// Constructor
        /// </summary>
        public PortsOfTravelController (IPortOfTravelService _service, IMapper _mapper) {
            mapper = _mapper;
            service = _service;
        }

        /// <summary>
        /// Return ports of travel list
        /// </summary>
        /// <returns>ports of travel</returns>
        /// <response code="200">Return values</response>
        [HttpGet ()]
        [ProducesResponseType (typeof (IEnumerable<PortOfTravelDictionary>), 200)]
        public ActionResult<IEnumerable<PortOfTravelDictionary>> GetAll ([FromQuery] Pagination pagination) {
            try {
                if (pagination == null) {
                    pagination = new Pagination ();
                }

                var itemsDTO = service.GetPortsOfTravel (pagination.PageNumber, pagination.PageSize);
                if (itemsDTO == null) {
                    return NotFound ();
                }
                var items = mapper.Map<IList<PortOfTravelDictionary>> (itemsDTO);
                return Ok (items);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }

        /// <summary>
        /// Return port of travel details
        /// </summary>
        /// <returns>port of travel</returns>
        /// <response code="200">Return value</response>
        [HttpGet ("{id:int}")]
        [ProducesResponseType (typeof (PortOfTravelDictionary), 200)]
        public ActionResult<PortOfTravelDictionary> GetOne ([FromRoute] int id) {
            try {
                var itemDTO = service.GetPortOfTravel (id);
                if (itemDTO == null) {
                    return NotFound ();
                }
                var item = mapper.Map<PortOfTravelDictionary> (itemDTO);
                return Ok (item);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }
    }
}