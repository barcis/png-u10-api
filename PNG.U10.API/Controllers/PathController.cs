using System;
using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PNG.U10.API.Models;
using PNG.U10.API.SeedWork;
using PNG.U10.Domain.Services.Abstract;

namespace PNGU10API.Controllers {
    /// <summary>
    /// Path Controller
    /// </summary>
    [Route ("api/[controller]")]
    [Produces ("application/json")]
    [ApiController]
    [Authorize]
    public partial class PathController : ControllerBase {
        private readonly IMapper mapper;
        private readonly IPathService service;

        /// <summary>
        /// Constructor
        /// </summary>
        public PathController (IPathService _service, IMapper _mapper) {
            mapper = _mapper;
            service = _service;
        }

        /// <summary>
        /// Return departments list
        /// </summary>
        /// <returns>departments</returns>
        /// <response code="200">Return values</response>
        [HttpGet ("Institutions/{institution:int}/Departments")]
        [ProducesResponseType (typeof (IEnumerable<Department>), 200)]
        public ActionResult<IEnumerable<Department>> GetDepartments ([FromRoute] int institution, [FromQuery] Pagination pagination) {
            try {
                if (pagination == null) {
                    pagination = new Pagination ();
                }

                int[] institutions = { institution };

                var itemsDTO = service.GetDepartments (institutions, pagination.PageNumber, pagination.PageSize);
                if (itemsDTO == null) {
                    return NotFound ();
                }
                var items = mapper.Map<IList<Department>> (itemsDTO);
                return Ok (items);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }

        /// <summary>
        /// Return programmes list
        /// </summary>
        /// <returns>programmes</returns>
        /// <response code="200">Return values</response>
        [HttpGet ("Institutions/{institution:int}/Departments/{department:int}/Programmes")]
        [ProducesResponseType (typeof (IEnumerable<Programme>), 200)]
        public ActionResult<IEnumerable<Programme>> GetProgrammes ([FromRoute] int institution, [FromRoute] int department, [FromQuery] Pagination pagination) {
            try {
                if (pagination == null) {
                    pagination = new Pagination ();
                }

                int[] institutions = { institution };
                int[] departments = { department };

                var itemsDTO = service.GetProgrammes (institutions, departments, pagination.PageNumber, pagination.PageSize);
                if (itemsDTO == null) {
                    return NotFound ();
                }
                var items = mapper.Map<IList<Programme>> (itemsDTO);
                return Ok (items);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }

        /// <summary>
        /// Return semesters list
        /// </summary>
        /// <returns>semesters</returns>
        /// <response code="200">Return values</response>
        [HttpGet ("Institutions/{institution:int}/Departments/{department:int}/Programmes/{programme:int}/Semesters")]
        [ProducesResponseType (typeof (IEnumerable<MembershipSemester>), 200)]
        public ActionResult<IEnumerable<MembershipSemester>> GetSemesters ([FromRoute] int institution, [FromRoute] int department, [FromRoute] int programme, [FromQuery] Pagination pagination) {
            try {
                if (pagination == null) {
                    pagination = new Pagination ();
                }

                int[] institutions = { institution };
                int[] departments = { department };
                int[] programmes = { programme };

                var itemsDTO = service.GetSemesters (institutions, departments, programmes, pagination.PageNumber, pagination.PageSize);
                if (itemsDTO == null) {
                    return NotFound ();
                }
                var items = mapper.Map<IList<MembershipSemester>> (itemsDTO);
                return Ok (items);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }
    }
}