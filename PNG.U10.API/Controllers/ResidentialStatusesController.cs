using System;
using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PNG.U10.API.Models;
using PNG.U10.API.SeedWork;
using PNG.U10.Domain.Services.Abstract;

namespace PNGU10API.Controllers {
    /// <summary>
    /// ResidentialStatuses Controller
    /// </summary>
    [Route ("api/[controller]")]
    [Produces ("application/json")]
    [ApiController]
    [Authorize]
    public class ResidentialStatusesController : ControllerBase {
        private readonly IMapper mapper;
        private readonly IResidentialStatusService service;

        /// <summary>
        /// Constructor
        /// </summary>
        public ResidentialStatusesController (IResidentialStatusService _service, IMapper _mapper) {
            mapper = _mapper;
            service = _service;
        }
        /// <summary>
        /// Return residential statuses list
        /// </summary>
        /// <returns>residential statuses</returns>
        /// <response code="200">Return values</response>
        [HttpGet ()]
        [ProducesResponseType (typeof (IEnumerable<ResidentialStatusDictionary>), 200)]
        public ActionResult<IEnumerable<ResidentialStatusDictionary>> GetAll ([FromQuery] Pagination pagination) {
            try {
                if (pagination == null) {
                    pagination = new Pagination ();
                }

                var itemsDTO = service.GetResidentialStatuses (pagination.PageNumber, pagination.PageSize);
                if (itemsDTO == null) {
                    return NotFound ();
                }
                var items = mapper.Map<IList<ResidentialStatusDictionary>> (itemsDTO);
                return Ok (items);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }

        /// <summary>
        /// Return residential status details
        /// </summary>
        /// <returns>residential status</returns>
        /// <response code="200">Return value</response>
        [HttpGet ("{id:int}")]
        [ProducesResponseType (typeof (ResidentialStatusDictionary), 200)]
        public ActionResult<ResidentialStatusDictionary> GetOne ([FromRoute] int id) {
            try {
                var itemDTO = service.GetResidentialStatus (id);
                if (itemDTO == null) {
                    return NotFound ();
                }
                var item = mapper.Map<ResidentialStatusDictionary> (itemDTO);
                return Ok (item);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }
    }
}