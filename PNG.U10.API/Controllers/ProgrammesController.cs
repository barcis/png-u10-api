using System;
using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PNG.U10.API.Models;
using PNG.U10.API.SeedWork;
using PNG.U10.Domain.Services.Abstract;

namespace PNGU10API.Controllers {
    /// <summary>
    /// Structure Controller
    /// </summary>
    [Route ("api/[controller]")]
    [Produces ("application/json")]
    [ApiController]
    [Authorize]
    public class ProgrammesController : ControllerBase {
        private readonly IMapper mapper;
        private readonly IPathService service;

        /// <summary>
        /// Constructor
        /// </summary>
        public ProgrammesController (IPathService _service, IMapper _mapper) {
            mapper = _mapper;
            service = _service;
        }

        /// <summary>
        /// Return programmes list
        /// </summary>
        /// <returns>programmes</returns>
        /// <response code="200">Return values</response>
        [HttpGet ("")]
        [ProducesResponseType (typeof (IEnumerable<Programme>), 200)]
        public ActionResult<IEnumerable<Programme>> GetAll ([FromQuery] int[] institutions, [FromQuery] int[] departments, [FromQuery] Pagination pagination) {
            try {
                if (pagination == null) {
                    pagination = new Pagination ();
                }
                var itemsDTO = service.GetProgrammes (institutions, departments, pagination.PageNumber, pagination.PageSize);
                if (itemsDTO == null) {
                    return NotFound ();
                }
                var items = mapper.Map<IList<Programme>> (itemsDTO);
                return Ok (items);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }

        /// <summary>
        /// Return programme details
        /// </summary>
        /// <returns>programme</returns>
        /// <response code="200">Return value</response>
        [HttpGet ("{id:int}")]
        [ProducesResponseType (typeof (Programme), 200)]
        public ActionResult<IEnumerable<Programme>> GetOne ([FromRoute] int id) {
            try {
                var itemDTO = service.GetProgramme (id);
                if (itemDTO == null) {
                    return NotFound ();
                }
                var item = mapper.Map<Programme> (itemDTO);
                return Ok (item);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }
    }
}