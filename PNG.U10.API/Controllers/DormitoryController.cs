using System;
using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PNG.U10.API.Models;
using PNG.U10.API.SeedWork;
using PNG.U10.Domain.Services.Abstract;

namespace PNGU10API.Controllers {
    /// <summary>
    /// Dormitory Controller
    /// </summary>
    [Route ("api/[controller]")]
    [Produces ("application/json")]
    [ApiController]
    [Authorize]
    public class DormitoriesController : ControllerBase {
        private readonly IDormitoryService service;
        private readonly IMapper mapper;

        /// <summary>
        /// Constructor
        /// </summary>
        public DormitoriesController (IDormitoryService _service, IMapper _mapper) {
            mapper = _mapper;
            service = _service;
        }

        /// <summary>
        /// Return dormitories list
        /// </summary>
        /// <returns>List of dormitories</returns>
        /// <response code="200">Return values</response>
        /// <response code="500">Exception</response>
        /// <param name="pagination">Pagination</param>
        [HttpGet ("")]
        [ProducesResponseType (typeof (IEnumerable<Dormitory>), 200)]
        [ProducesResponseType (500)]
        public ActionResult<IEnumerable<Dormitory>> GetAll ([FromQuery] Pagination pagination) {
            try {
                if (pagination == null) {
                    pagination = new Pagination ();
                }

                var itemsDTO = service.GetAll (pagination.PageNumber, pagination.PageSize);
                var items = mapper.Map<IList<Dormitory>> (itemsDTO);
                return Ok (items);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }

        /// <summary>
        /// Return dormitory details
        /// </summary>
        /// <returns>Dormitory</returns>
        /// <response code="200">Return value</response>
        /// <response code="404">Not found value</response>
        /// <response code="500">Exception</response>
        [HttpGet ("{id}")]
        [ProducesResponseType (typeof (IEnumerable<Dormitory>), 200)]
        [ProducesResponseType (500)]
        public ActionResult<Dormitory> GetOne (int id) {
            try {
                var itemDTO = service.GetOne (id);
                if (itemDTO == null) {
                    return NotFound ();
                }
                var item = mapper.Map<Dormitory> (itemDTO);
                return Ok (item);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }

        /// <summary>
        /// Return dormitory rooms list
        /// </summary>
        /// <returns>List of dormitory rooms</returns>
        /// <response code="200">Return values</response>
        /// <response code="500">Exception</response>
        /// <param name="id">Dormitory id</param>
        [HttpGet ("{id}/Rooms")]
        [ProducesResponseType (typeof (IEnumerable<DormitoryRoom>), 200)]
        [ProducesResponseType (404)]
        [ProducesResponseType (500)]
        public ActionResult<DormitoryRoom> GetOneDormitoryRooms (int id) {
            try {
                var itemsDTO = service.GetOneDormitoryRooms (id);
                var items = mapper.Map<IList<DormitoryRoom>> (itemsDTO);
                return Ok (items);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }

        /// <summary>
        /// Return dormitories rooms list
        /// </summary>
        /// <returns>List of dormitories rooms</returns>
        /// <response code="200">Return values</response>
        /// <response code="500">Exception</response>
        [HttpGet ("Rooms")]
        [ProducesResponseType (typeof (IEnumerable<DormitoryRoom>), 200)]
        [ProducesResponseType (500)]
        public ActionResult<DormitoryRoom> GetAllDormitoriesRooms () {
            try {
                var itemsDTO = service.GetAllDormitoriesRooms ();
                var items = mapper.Map<IList<DormitoryRoom>> (itemsDTO);
                return Ok (items);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }

        /// <summary>
        /// Return dormitory room detail
        /// </summary>
        /// <returns>Dormitory room details</returns>
        /// <response code="200">Return values</response>
        /// <response code="404">Not found value</response>
        /// <response code="500">Exception</response>
        /// <param name="id">Dormitory room id</param>
        [HttpGet ("Rooms/{id}")]
        [ProducesResponseType (typeof (IEnumerable<DormitoryRoom>), 200)]
        [ProducesResponseType (500)]
        public ActionResult<DormitoryRoom> GetDormitoryRoom (int id) {
            try {
                var itemDTO = service.GetDormitoryRoom (id);
                if (itemDTO == null) {
                    return NotFound ();
                }
                var item = mapper.Map<DormitoryRoom> (itemDTO);
                return Ok (item);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }
    }
}