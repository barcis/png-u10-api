using System;
using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PNG.U10.API.Models;
using PNG.U10.API.SeedWork;
using PNG.U10.Domain.Services.Abstract;

namespace PNGU10API.Controllers {
    /// <summary>
    /// Institutions Controller
    /// </summary>
    [Route ("api/[controller]")]
    [Produces ("application/json")]
    [ApiController]
    [Authorize]
    public class InstitutionsController : ControllerBase {
        private readonly IMapper mapper;
        private readonly IInstitutionService service;

        /// <summary>
        /// Constructor
        /// </summary>
        public InstitutionsController (IInstitutionService _service, IMapper _mapper) {
            mapper = _mapper;
            service = _service;
        }

        /// <summary>
        /// Return institutions list
        /// </summary>
        /// <returns>institutions</returns>
        /// <response code="200">Return values</response>
        [HttpGet ()]
        [ProducesResponseType (typeof (IEnumerable<Institution>), 200)]
        public ActionResult<IEnumerable<Institution>> GetAll ([FromQuery] Pagination pagination) {
            try {
                if (pagination == null) {
                    pagination = new Pagination ();
                }

                //TODO: uzupełnić sprawdzanie uprawnień dla StudentDataOfficer
                var itemsDTO = service.GetInstitutions (pagination.PageNumber, pagination.PageSize);
                if (itemsDTO == null) {
                    return NotFound ();
                }
                var items = mapper.Map<IList<Institution>> (itemsDTO);

                // IList<Institution> itemsQuery;

                // if (User.IsInRole (Roles.StudentDataOfficer)) {
                //     var userId = GetUserId ();
                //     var instIds = _userService.GetU10InstitutionIdList (userId);
                //     itemsQuery = _structureService.GetInstitutions (instIds);
                // } else {
                //     itemsQuery = _structureService.GetInstitutions ();
                // }

                return Ok (items);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }

        /// <summary>
        /// Return institution details
        /// </summary>
        /// <returns>institution</returns>
        /// <response code="200">Return values</response>
        [HttpGet ("{id:int}")]
        [ProducesResponseType (typeof (Institution), 200)]
        public ActionResult<Institution> GetOne ([FromRoute] int id) {
            try {
                //TODO: uzupełnić sprawdzanie uprawnień dla StudentDataOfficer
                var itemDTO = service.GetInstitution (id);
                if (itemDTO == null) {
                    return NotFound ();
                }
                var item = mapper.Map<Institution> (itemDTO);
                return Ok (item);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }
    }
}