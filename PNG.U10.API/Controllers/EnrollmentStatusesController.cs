using System;
using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PNG.U10.API.Models;
using PNG.U10.API.SeedWork;
using PNG.U10.Domain.Services.Abstract;

namespace PNGU10API.Controllers {
    /// <summary>
    /// StatusNames Controller
    /// </summary>
    [Route ("api/[controller]")]
    [Produces ("application/json")]
    [ApiController]
    [Authorize]
    public class EnrollmentStatusesController : ControllerBase {
        private readonly IMapper mapper;
        private readonly IEnrollmentStatusService service;

        /// <summary>
        /// Constructor
        /// </summary>
        public EnrollmentStatusesController (IEnrollmentStatusService _service, IMapper _mapper) {
            mapper = _mapper;
            service = _service;
        }

        /// <summary>
        /// Return status names list
        /// </summary>
        /// <returns>status names</returns>
        /// <response code="200">Return values</response>
        [HttpGet ()]
        [ProducesResponseType (typeof (IEnumerable<EnrollmentStatusDictionary>), 200)]
        public ActionResult<IEnumerable<EnrollmentStatusDictionary>> GetAll ([FromQuery] Pagination pagination) {
            try {
                if (pagination == null) {
                    pagination = new Pagination ();
                }

                var itemsDTO = service.GetEnrollmentStatuses (pagination.PageNumber, pagination.PageSize);
                if (itemsDTO == null) {
                    return NotFound ();
                }
                var items = mapper.Map<IList<EnrollmentStatusDictionary>> (itemsDTO);
                return Ok (items);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }

        /// <summary>
        /// Return status name details
        /// </summary>
        /// <returns>status name</returns>
        /// <response code="200">Return value</response>
        [HttpGet ("{id:int}")]
        [ProducesResponseType (typeof (EnrollmentStatusDictionary), 200)]
        public ActionResult<EnrollmentStatusDictionary> GetOne ([FromRoute] int id) {
            try {
                var itemDTO = service.GetEnrollmentStatus (id);
                if (itemDTO == null) {
                    return NotFound ();
                }
                var item = mapper.Map<EnrollmentStatusDictionary> (itemDTO);
                return Ok (item);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }
    }
}