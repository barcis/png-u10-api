using System;
using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PNG.U10.API.Models;
using PNG.U10.API.SeedWork;
using PNG.U10.Domain.Services.Abstract;

namespace PNGU10API.Controllers {
    /// <summary>
    /// OtherStatuses Controller
    /// </summary>
    [Route ("api/[controller]")]
    [Produces ("application/json")]
    [ApiController]
    [Authorize]
    public class OtherStatusesController : ControllerBase {
        private readonly IMapper mapper;
        private readonly IOtherStatusService service;

        /// <summary>
        /// Constructor
        /// </summary>
        public OtherStatusesController (IOtherStatusService _service, IMapper _mapper) {
            mapper = _mapper;
            service = _service;
        }

        /// <summary>
        /// Return other statuses list
        /// </summary>
        /// <returns>other statuses</returns>
        /// <response code="200">Return values</response>
        [HttpGet ()]
        [ProducesResponseType (typeof (IEnumerable<OtherStatusDictionary>), 200)]
        public ActionResult<IEnumerable<OtherStatusDictionary>> GetAll ([FromQuery] Pagination pagination) {
            try {
                if (pagination == null) {
                    pagination = new Pagination ();
                }

                var itemsDTO = service.GetOtherStatuses (pagination.PageNumber, pagination.PageSize);
                if (itemsDTO == null) {
                    return NotFound ();
                }
                var items = mapper.Map<IList<OtherStatusDictionary>> (itemsDTO);
                return Ok (items);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }

        /// <summary>
        /// Return other status details
        /// </summary>
        /// <returns>other status</returns>
        /// <response code="200">Return value</response>
        [HttpGet ("{id:int}")]
        [ProducesResponseType (typeof (OtherStatusDictionary), 200)]
        public ActionResult<OtherStatusDictionary> GetOne ([FromRoute] int id) {
            try {
                var itemDTO = service.GetOtherStatus (id);
                if (itemDTO == null) {
                    return NotFound ();
                }
                var item = mapper.Map<OtherStatusDictionary> (itemDTO);
                return Ok (item);
            } catch (Exception ex) {
                return Problem (
                    detail: ex.StackTrace,
                    title: ex.Message);
            }
        }
    }
}